#ifndef __NUMERIC_H
#define __NUMERIC_H


#include "iec61131_types.h"


int numeric_ABS( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_ACOS( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_ASIN( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_ATAN( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_COS( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_EXP( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_LOG( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_LN( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_SIN( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_SQRT( IEC61131_DATA * Parameters, unsigned int Count );

int numeric_TAN( IEC61131_DATA * Parameters, unsigned int Count );


#endif

