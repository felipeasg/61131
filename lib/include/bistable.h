#ifndef __BISTABLE_H
#define __BISTABLE_H


#include "iec61131_external.h"
#include "iec61131_types.h"


typedef struct _BISTABLE
{
    EXTERNAL_BOOL( S );
    EXTERNAL_BOOL( R );
    EXTERNAL_BOOL( Q );
}
BISTABLE;


int bistable_executeRS( char * Memory );

int bistable_executeSR( char * Memory );

int bistable_parameterBISTABLE( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type );


#endif
