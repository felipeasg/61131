#ifndef __BITSTRING_H
#define __BITSTRING_H


#include "iec61131_types.h"


int bitstring_ROL( IEC61131_DATA * Parameters, unsigned int Count );

int bitstring_ROR( IEC61131_DATA * Parameters, unsigned int Count );

int bitstring_SHL( IEC61131_DATA * Parameters, unsigned int Count );

int bitstring_SHR( IEC61131_DATA * Parameters, unsigned int Count );


#endif

