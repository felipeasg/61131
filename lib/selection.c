#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include "config.h"
#include "selection.h"


#define _TYPE_COUNT( c )        if( Count != (c) ) return -EINVAL;

#define _TYPE_COUNT_MIN( c )    if( Count < (c) ) return -EINVAL;


int 
selection_LIMIT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pIn, *pMaximum, *pMinimum, *pParameter, stValue;


    _TYPE_COUNT( 3 );

    pIn = &Parameters[1];
    pMinimum = &Parameters[0];
    pMaximum = &Parameters[2];

    if( ( pIn->Type != pMinimum->Type ) ||
            ( pIn->Type != pMaximum->Type ) )
    {
        return -EINVAL;
    }

    ( void ) memcpy( &stValue, pIn, sizeof( IEC61131_DATA ) );

    switch( pIn->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            if( pIn->Value.B8 < pMinimum->Value.B8 )
            {
                stValue.Value.B8 = pMinimum->Value.B8;
            }
            if( pIn->Value.B8 > pMaximum->Value.B8 )
            {
                stValue.Value.B8 = pMaximum->Value.B8;
            }
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            if( pIn->Value.B16 < pMinimum->Value.B16 )
            {
                stValue.Value.B16 = pMinimum->Value.B16;
            }
            if( pIn->Value.B16 > pMaximum->Value.B16 )
            {
                stValue.Value.B16 = pMaximum->Value.B16;
            }
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            if( pIn->Value.B32 < pMinimum->Value.B32 )
            {
                stValue.Value.B32 = pMinimum->Value.B32;
            }
            if( pIn->Value.B32 > pMaximum->Value.B32 )
            {
                stValue.Value.B32 = pMaximum->Value.B32;
            }
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            if( pIn->Value.B64 < pMinimum->Value.B64 )
            {
                stValue.Value.B64 = pMinimum->Value.B64;
            }
            if( pIn->Value.B64 > pMaximum->Value.B64 )
            {
                stValue.Value.B64 = pMaximum->Value.B64;
            }
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            if( pIn->Value.Float < pMinimum->Value.Float )
            {
                stValue.Value.Float = pMinimum->Value.Float;
            }
            if( pIn->Value.Float > pMinimum->Value.Float )
            {
                stValue.Value.Float = pMaximum->Value.Float;
            }
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            if( pIn->Value.Double < pMinimum->Value.Double )
            {
                stValue.Value.Double = pMinimum->Value.Double;
            }
            if( pIn->Value.Double > pMaximum->Value.Double )
            {
                stValue.Value.Double = pMaximum->Value.Double;
            }
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            return -EINVAL;
    }

    pParameter = &Parameters[0];
    ( void ) memcpy( pParameter, &stValue, sizeof( IEC61131_DATA ) );

    return 1;
}


int 
selection_MAX( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter, stValue;
    unsigned int uIndex;


    _TYPE_COUNT_MIN( 1 );

    stValue.Value.B64 = 0;
    stValue.Type = TYPE_NONE;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        pParameter = &Parameters[ uIndex ];
        if( ( stValue.Type != TYPE_NONE ) &&
                ( stValue.Type != pParameter->Type ) )
        {
            return -EINVAL;
        }
        stValue.Type = pParameter->Type;

        switch( pParameter->Type )
        {
            case TYPE_BOOL:
            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                stValue.Value.B8 = ( stValue.Value.B8 > pParameter->Value.B8 ) ? stValue.Value.B8 : pParameter->Value.B8;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                stValue.Value.B16 = ( stValue.Value.B16 > pParameter->Value.B16 ) ? stValue.Value.B16 : pParameter->Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                stValue.Value.B32 = ( stValue.Value.B32 > pParameter->Value.B32 ) ? stValue.Value.B32 : pParameter->Value.B32;
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:

                stValue.Value.B64 = ( stValue.Value.B64 > pParameter->Value.B64 ) ? stValue.Value.B64 : pParameter->Value.B64;
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case TYPE_REAL:

                stValue.Value.Float = ( stValue.Value.Float > pParameter->Value.Float ) ? stValue.Value.Float : pParameter->Value.Float;
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case TYPE_LREAL:

                stValue.Value.Double = ( stValue.Value.Double > pParameter->Value.Double ) ? stValue.Value.Double : pParameter->Value.Double;
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
            default:

                break;
        }
    }

    pParameter = &Parameters[0];
    ( void ) memcpy( pParameter, &stValue, sizeof( IEC61131_DATA ) );

    return 1;
}


int 
selection_MIN( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter, stValue;
    unsigned int uIndex;


    _TYPE_COUNT_MIN( 1 );

    stValue.Value.B64 = 0;
    stValue.Type = TYPE_NONE;

    for( uIndex = 0; uIndex < Count; ++uIndex )
    {
        pParameter = &Parameters[ uIndex ];
        if( ( stValue.Type != TYPE_NONE ) &&
                ( stValue.Type != pParameter->Type ) )
        {
            return -EINVAL;
        }
        stValue.Type = pParameter->Type;

        switch( pParameter->Type )
        {
            case TYPE_BOOL:
            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                stValue.Value.B8 = ( stValue.Value.B8 < pParameter->Value.B8 ) ? stValue.Value.B8 : pParameter->Value.B8;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                stValue.Value.B16 = ( stValue.Value.B16 < pParameter->Value.B16 ) ? stValue.Value.B16 : pParameter->Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                stValue.Value.B32 = ( stValue.Value.B32 < pParameter->Value.B32 ) ? stValue.Value.B32 : pParameter->Value.B32;
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:

                stValue.Value.B64 = ( stValue.Value.B64 < pParameter->Value.B64 ) ? stValue.Value.B64 : pParameter->Value.B64;
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case TYPE_REAL:

                stValue.Value.Float = ( stValue.Value.Float < pParameter->Value.Float ) ? stValue.Value.Float : pParameter->Value.Float;
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case TYPE_LREAL:

                stValue.Value.Double = ( stValue.Value.Double < pParameter->Value.Double ) ? stValue.Value.Double : pParameter->Value.Double;
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
            default:

                break;
        }
    }

    pParameter = &Parameters[0];
    ( void ) memcpy( pParameter, &stValue, sizeof( IEC61131_DATA ) );

    return 1;
}


int 
selection_MUX( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    int8_t nIndex, nValue;


    _TYPE_COUNT_MIN( 2 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_SINT:
        case TYPE_USINT:

            nValue = ( int8_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:

            nValue = ( int8_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:

            nValue = ( int8_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:

            nValue = ( int8_t ) pParameter->Value.B64;
            break;

#endif
        default:

            return -EINVAL;
    }
    if( ( nValue < 0 ) ||
            ( nValue > ( int8_t ) ( Count - 2 ) ) )
    {
        return -EINVAL;
    }

    nIndex = ( 1 + ( nValue % ( Count - 1 ) ) );
    ( void ) memcpy( pParameter, &Parameters[ nIndex ], sizeof( IEC61131_DATA ) );

    return 0;
}


int 
selection_SEL( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pValue1, *pValue2, *pParameter, *pResult;


    _TYPE_COUNT( 3 );

    pParameter = &Parameters[0];
    pValue1 = &Parameters[1];
    pValue2 = &Parameters[2];

    if( ( pParameter->Type != TYPE_BOOL ) ||
            ( pValue1->Type != pValue2->Type ) )
    {
        return -EINVAL;
    }

    pResult = ( pParameter->Value.B8 != 0 ) ? pValue2 : pValue1;
    ( void ) memcpy( pParameter, pResult, sizeof( IEC61131_DATA ) );    
        
    return 1;
}


