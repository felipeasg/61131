#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include "bitstring.h"
#include "config.h"


#define _TYPE_COUNT( c )        if( Count != (c) ) return -EINVAL;


int 
bitstring_ROL( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pBit, *pCount;
    int64_t sCount;
    uint8_t bRollover;


    _TYPE_COUNT( 2 );

    pBit = &Parameters[0];
    pCount = &Parameters[1];

    switch( pCount->Type )
    {
        case TYPE_SINT:
        case TYPE_USINT:

            sCount = ( int64_t ) pCount->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:

            sCount = ( int64_t ) pCount->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:

            sCount = ( int64_t ) pCount->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:

            sCount = ( int64_t ) pCount->Value.B64;
            break;

#endif
        default:

            return -EINVAL;
    }
    if( sCount < 0 )
    {
        return -EINVAL;
    }

    switch( pBit->Type )
    {
        case TYPE_BYTE:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B8 & 0x80 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B8 <<= 1;
                pBit->Value.B8 |= ( bRollover == BOOL_TRUE ) ? 1 : 0;
            }
            /* pBit->Type = TYPE_BYTE; */
            break;

        case TYPE_WORD:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B16 & 0x8000 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B16 <<= 1;
                pBit->Value.B16 |= ( bRollover == BOOL_TRUE ) ? 1 : 0;
            }
            /* pBit->Type = TYPE_WORD; */
            break;

        case TYPE_DWORD:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B32 & 0x80000000 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B32 <<= 1;
                pBit->Value.B32 |= ( bRollover == BOOL_TRUE ) ? 1 : 0;
            }
            /* pBit->Type = TYPE_DWORD; */
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LWORD:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B64 & 0x8000000000000000 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B64 <<= 1;
                pBit->Value.B64 |= ( bRollover == BOOL_TRUE ) ? 1 : 0;
            }
            /* pBit->Type = TYPE_LWORD; */
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
bitstring_ROR( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pBit, *pCount;
    int64_t sCount;
    uint8_t bRollover;


    _TYPE_COUNT( 2 );

    pBit = &Parameters[0];
    pCount = &Parameters[1];

    switch( pCount->Type )
    {
        case TYPE_SINT:
        case TYPE_USINT:

            sCount = ( int64_t ) pCount->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:

            sCount = ( int64_t ) pCount->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:

            sCount = ( int64_t ) pCount->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:

            sCount = ( int64_t ) pCount->Value.B64;
            break;

#endif
        default:

            return -EINVAL;
    }
    if( sCount < 0 )
    {
        return -EINVAL;
    }

    switch( pBit->Type )
    {
        case TYPE_BYTE:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B8 & 0x1 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B8 >>= 1;
                pBit->Value.B8 |= ( bRollover == BOOL_TRUE ) ? 0x80 : 0;
            }
            /* pBit->Type = TYPE_BYTE; */
            break;

        case TYPE_WORD:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B16 & 0x1 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B16 >>= 1;
                pBit->Value.B16 |= ( bRollover == BOOL_TRUE ) ? 0x8000 : 0;
            }
            /* pBit->Type = TYPE_WORD; */
            break;

        case TYPE_DWORD:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B32 & 0x1 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B32 >>= 1;
                pBit->Value.B32 |= ( bRollover == BOOL_TRUE ) ? 0x80000000 : 0;
            }
            /* pBit->Type = TYPE_DWORD; */
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LWORD:

            for( ; sCount > 0; --sCount )
            {
                bRollover = ( ( pBit->Value.B64 & 0x1 ) != 0 ) ? BOOL_TRUE : BOOL_FALSE;
                pBit->Value.B64 >>= 1;
                pBit->Value.B64 |= ( bRollover == BOOL_TRUE ) ? 0x8000000000000000 : 0;
            }
            /* pBit->Type = TYPE_LWORD; */
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
bitstring_SHL( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pBit, *pCount;
    int64_t sCount;


    _TYPE_COUNT( 2 );

    pBit = &Parameters[0];
    pCount = &Parameters[1];

    switch( pCount->Type )
    {
        case TYPE_SINT:
        case TYPE_USINT:

            sCount = ( int64_t ) pCount->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:

            sCount = ( int64_t ) pCount->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:

            sCount = ( int64_t ) pCount->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:

            sCount = ( int64_t ) pCount->Value.B64;
            break;

#endif
        default:

            return -EINVAL;
    }
    if( sCount < 0 )
    {
        return -EINVAL;
    }

    switch( pBit->Type )
    {
        case TYPE_BYTE:

            pBit->Value.B8 <<= sCount;
            /* pBit->Type = TYPE_BYTE; */
            break;

        case TYPE_WORD:

            pBit->Value.B16 <<= sCount;
            /* pBit->Type = TYPE_WORD; */
            break;

        case TYPE_DWORD:

            pBit->Value.B32 <<= sCount;
            /* pBit->Type = TYPE_DWORD; */
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LWORD:

            pBit->Value.B64 <<= sCount;
            /* pBit->Type = TYPE_LWORD; */
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}


int
bitstring_SHR( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pBit, *pCount;
    int64_t sCount;


    _TYPE_COUNT( 2 );

    pBit = &Parameters[0];
    pCount = &Parameters[1];

    switch( pCount->Type )
    {
        case TYPE_SINT:
        case TYPE_USINT:

            sCount = ( int64_t ) pCount->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:

            sCount = ( int64_t ) pCount->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:

            sCount = ( int64_t ) pCount->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:

            sCount = ( int64_t ) pCount->Value.B64;
            break;

#endif
        default:

            return -EINVAL;
    }
    if( sCount < 0 )
    {
        return -EINVAL;
    }

    switch( pBit->Type )
    {
        case TYPE_BYTE:

            pBit->Value.B8 >>= sCount;
            /* pBit->Type = TYPE_BYTE; */
            break;

        case TYPE_WORD:

            pBit->Value.B16 >>= sCount;
            /* pBit->Type = TYPE_WORD; */
            break;

        case TYPE_DWORD:

            pBit->Value.B32 >>= sCount;
            /* pBit->Type = TYPE_DWORD; */
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LWORD:

            pBit->Value.B64 >>= sCount;
            /* pBit->Type = TYPE_LWORD; */
            break;

#endif
        default:

            return -EINVAL;
    }

    return 1;
}

