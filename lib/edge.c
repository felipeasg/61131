#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>

#include "edge.h"


int 
edge_executeFTRIG( char * Memory )
{
    TRIG *pInstance;


    pInstance = ( TRIG * ) Memory;
    pInstance->Q = ~pInstance->Clk;
    pInstance->Q &= pInstance->Edge;
    pInstance->Edge = pInstance->Clk;

    return 0;
}


int
edge_executeRTRIG( char * Memory )
{
    TRIG *pInstance;


    pInstance = ( TRIG * ) Memory;
    pInstance->Q = pInstance->Clk;
    pInstance->Q &= ~pInstance->Edge;
    pInstance->Edge = pInstance->Clk;

    return 0;
}

int 
edge_parameterTRIG( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "Clk" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( TRIG, Clk );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "Q" ) == 0 ) 
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( TRIG, Q );
        *Type = TYPE_BOOL;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}


