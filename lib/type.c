#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include "config.h"
#include "type.h"


#define _TYPE_COUNT( c )        if( Count != (c) ) return -EINVAL;


int
type_TOBOOL( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint8_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( pParameter->Value.B8 != 0 ) ? BOOL_TRUE : BOOL_FALSE;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( pParameter->Value.B16 != 0 ) ? BOOL_TRUE : BOOL_FALSE;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( pParameter->Value.B32 != 0 ) ? BOOL_TRUE : BOOL_FALSE;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( pParameter->Value.B64 != 0 ) ? BOOL_TRUE : BOOL_FALSE;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( pParameter->Value.Float != 0.0 ) ? BOOL_TRUE : BOOL_FALSE;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( pParameter->Value.Double != 0.0 ) ? BOOL_TRUE : BOOL_FALSE;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = BOOL_FALSE;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.S8 = uValue;
    pParameter->Type = TYPE_BOOL;

    return 1;
}


int 
type_TOBYTE( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint8_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint8_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint8_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint8_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint8_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint8_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint8_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint8_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U8 = uValue;
    pParameter->Type = TYPE_BYTE;

    return 1;
}

#ifdef CONFIG_SUPPORT_DATETIME

int
type_TODATE( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        default:

            break;
    }

    return 0;
}

#endif

int
type_TODINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    int32_t sValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            sValue = ( int32_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            sValue = ( int32_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            sValue = ( int32_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            sValue = ( int32_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            sValue = ( int32_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            sValue = ( int32_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            sValue = ( int32_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.S32 = sValue;
    pParameter->Type = TYPE_DINT;

    return 1;
}

#ifdef CONFIG_SUPPORT_DATETIME

int
type_TODT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        default:

            break;
    }

    return 0;
}

#endif

int 
type_TODWORD( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint32_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint32_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint32_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint32_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint32_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint32_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint32_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint32_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U32 = uValue;
    pParameter->Type = TYPE_DWORD;

    return 1;
}


int
type_TOINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    int16_t sValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            sValue = ( int16_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            sValue = ( int16_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            sValue = ( int16_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            sValue = ( int16_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            sValue = ( int16_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            sValue = ( int16_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            sValue = ( int16_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.S16 = sValue;
    pParameter->Type = TYPE_INT;

    return 1;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int
type_TOLINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    int64_t sValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            sValue = ( int64_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            sValue = ( int64_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            sValue = ( int64_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            sValue = ( int64_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            sValue = ( int64_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            sValue = ( int64_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            sValue = ( int64_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.S64 = sValue;
    pParameter->Type = TYPE_LINT;

    return 1;
}

#endif
#ifdef CONFIG_SUPPORT_LREAL

int
type_TOLREAL( IEC61131_DATA * Parameters, unsigned int Count )
{
    return 0;
}

#endif
#ifdef CONFIG_SUPPORT_LONGLONG

int 
type_TOLWORD( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint64_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint64_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint64_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint64_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint64_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint64_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint64_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint64_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U64 = uValue;
    pParameter->Type = TYPE_LWORD;

    return 1;
}

#endif
#ifdef CONFIG_SUPPORT_REAL

int
type_TOREAL( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    float fValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:

            fValue = ( pParameter->Value.B8 > 0 ) ? 1.0 : 0.0;
            break;

        case TYPE_USINT:
        case TYPE_BYTE:

            fValue = ( float ) pParameter->Value.U8;
            break;

        case TYPE_SINT:

            fValue = ( float ) pParameter->Value.S8;
            break;

        case TYPE_UINT:
        case TYPE_WORD:

            fValue = ( float ) pParameter->Value.U16;
            break;

        case TYPE_INT:

            fValue = ( float ) pParameter->Value.S16;
            break;

        case TYPE_UDINT:
        case TYPE_DWORD:

            fValue = ( float ) pParameter->Value.U32;
            break;

        case TYPE_DINT:
        
            fValue = ( float ) pParameter->Value.S32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_ULINT:
        case TYPE_LWORD:

            fValue = ( float ) pParameter->Value.U64;
            break;

        case TYPE_LINT:

            fValue = ( float ) pParameter->Value.S64;
            break;

#endif
        case TYPE_REAL:

            fValue = ( float ) pParameter->Value.Float;
            break;

#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            fValue = ( float ) pParameter->Value.Double;
            break;

#endif
        default:

            fValue = 0.0f;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.Float = fValue;
    pParameter->Type = TYPE_REAL;

    return 1;
}

#endif

int 
type_TOSINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    int8_t sValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            sValue = ( int8_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            sValue = ( int8_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            sValue = ( int8_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            sValue = ( int8_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            sValue = ( int8_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            sValue = ( int8_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            sValue = ( uint8_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.S8 = sValue;
    pParameter->Type = TYPE_SINT;

    return 1;
}

#ifdef CONFIG_SUPPORT_STRING

int
type_TOSTRING( IEC61131_DATA * Parameters, unsigned int Count )
{
    return 0;
}

#endif
#ifdef CONFIG_SUPPORT_DATETIME

int
type_TOTIME( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    int32_t nValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            nValue = ( int32_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            nValue = ( int32_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            nValue = ( int32_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            nValue = ( int32_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        case TYPE_BOOL:
        default:

            nValue = 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.Time = nValue;
    pParameter->Type = TYPE_TIME;

    return 1;
}

#endif
#ifdef CONFIG_SUPPORT_DATETIME

int
type_TOTOD( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        default:

            break;
    }

    return 0;
}

#endif

int
type_TOUDINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint32_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint32_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint32_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint32_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint32_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint32_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint32_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint32_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U32 = uValue;
    pParameter->Type = TYPE_UDINT;

    return 1;
}


int
type_TOUINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint16_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint16_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint16_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint16_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint16_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint16_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint16_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint16_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U16 = uValue;
    pParameter->Type = TYPE_UINT;

    return 1;
}

#ifdef CONFIG_SUPPORT_LONGLONG

int
type_TOULINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint64_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint64_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint64_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint64_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint64_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint64_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint64_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint64_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U64 = uValue;
    pParameter->Type = TYPE_ULINT;

    return 1;
}

#endif

int
type_TOUSINT( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint8_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint8_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint8_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint8_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint8_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint8_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint8_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint8_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U8 = uValue;
    pParameter->Type = TYPE_USINT;

    return 1;
}


int 
type_TOWORD( IEC61131_DATA * Parameters, unsigned int Count )
{
    IEC61131_DATA *pParameter;
    uint16_t uValue;


    _TYPE_COUNT( 1 );

    pParameter = &Parameters[0];
    switch( pParameter->Type )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            uValue = ( uint16_t ) pParameter->Value.B8;
            break;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            uValue = ( uint16_t ) pParameter->Value.B16;
            break;

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            uValue = ( uint16_t ) pParameter->Value.B32;
            break;

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            uValue = ( uint16_t ) pParameter->Value.B64;
            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            uValue = ( uint16_t ) pParameter->Value.Float;
            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            uValue = ( uint16_t ) pParameter->Value.Double;
            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
#endif
#ifdef CONFIG_SUPPORT_STRING
#endif
#ifdef CONFIG_SUPPORT_WSTRING
#endif
        default:

            uValue = ( uint16_t ) 0;
            break;
    }

    ( void ) memset( pParameter, 0, sizeof( *pParameter ) );
    pParameter->Value.U16 = uValue;
    pParameter->Type = TYPE_WORD;

    return 1;
}

#ifdef CONFIG_SUPPORT_WSTRING

int
type_TOWSTRING( IEC61131_DATA * Parameters, unsigned int Count )
{
    return 0;
}

#endif


