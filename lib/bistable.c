#include <string.h>
#include <errno.h>

#include "bistable.h"


int 
bistable_executeRS( char * Memory )
{
    BISTABLE *pInstance;


    pInstance = ( BISTABLE * ) Memory;
    pInstance->Q &= ~pInstance->R;
    pInstance->Q |= pInstance->S;

    return 0;
}


int 
bistable_executeSR( char * Memory )
{
    BISTABLE *pInstance;


    pInstance = ( BISTABLE * ) Memory;
    pInstance->Q |= pInstance->S;
    pInstance->Q &= ~pInstance->R;

    return 0;
}


int 
bistable_parameterBISTABLE( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "S" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( BISTABLE, S );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "R" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( BISTABLE, R );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( BISTABLE, Q );
        *Type = TYPE_BOOL;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}

