#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "iec61131_external.h"

#include "md5.h"


/*!
    \fn void IEC61131_DIGEST( char * Digest )
    \brief Returns message digest for IEC 61131-3 function and function block library
    \param Digest Pointer to character string array for message digest
    \returns No return value

    This function is intended to be employed for matching of C-based library 
    functions between the IEC 61131-3 compiler and virtual machine run-time, 
    ensuring that C-based function and function block offsets defined and 
    specified within a byte code file by the compiler match those in place 
    within the virtual machine run-time.
*/

void
IEC61131_DIGEST( char * Digest )
{
    MD5 stDigest;
    char szLine[LINE_MAX];
    unsigned int uIndex;


    md5_initialise( &stDigest );

    for( uIndex = 0; uIndex < IEC61131_FUNCTION_COUNT; ++uIndex )
    {
        ( void ) snprintf( szLine, sizeof( szLine ), "%s;", IEC61131_FUNCTION[ uIndex ].Name );
        md5_update( &stDigest, ( unsigned char * ) szLine, strlen( szLine ) );
    }
    for( uIndex = 0; uIndex < IEC61131_FUNCTION_BLOCK_COUNT; ++uIndex )
    {
        ( void ) snprintf( szLine, sizeof( szLine ), "%s;", IEC61131_FUNCTION_BLOCK[ uIndex ].Name );
        md5_update( &stDigest, ( unsigned char * ) szLine, strlen( szLine ) );
    }

    md5_finalise( &stDigest, ( unsigned char * ) Digest );
}

