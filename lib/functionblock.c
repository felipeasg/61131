#include "iec61131_external.h"

#include "bistable.h"
#include "config.h"
#include "counters.h"
#include "edge.h"
#include "timers.h"


EXTERNAL_FUNCTION_BLOCK_DECLARATION IEC61131_FUNCTION_BLOCK[] =
{
    /* Standard Function Blocks */

    /* 2.5.2.3.1 Bistable Elements */

    { "RS",             sizeof( BISTABLE ),     bistable_parameterBISTABLE,     bistable_executeRS          },
    { "SR",             sizeof( BISTABLE ),     bistable_parameterBISTABLE,     bistable_executeSR          },

    /* 2.5.2.3.2 Edge Detection */

    { "F_TRIG",         sizeof( TRIG ),         edge_parameterTRIG,             edge_executeFTRIG           },
    { "R_TRIG",         sizeof( TRIG ),         edge_parameterTRIG,             edge_executeRTRIG           },

    /* 2.5.2.3.3 Counters */

    { "CTD",            sizeof( CTD ),          counters_parameterCTD,          counters_executeCTD         },
    { "CTD_DINT",       sizeof( CTD_DINT ),     counters_parameterCTD_DINT,     counters_executeCTD_DINT    },
#ifdef CONFIG_SUPPORT_LONGLONG
    { "CTD_LINT",       sizeof( CTD_LINT ),     counters_parameterCTD_LINT,     counters_executeCTD_LINT    },
#endif
    { "CTD_UDINT",      sizeof( CTD_UDINT ),    counters_parameterCTD_UDINT,    counters_executeCTD_UDINT   },
#ifdef CONFIG_SUPPORT_LONGLONG
    { "CTD_ULINT",      sizeof( CTD_ULINT ),    counters_parameterCTD_ULINT,    counters_executeCTD_ULINT   },
#endif
    { "CTU",            sizeof( CTU ),          counters_parameterCTU,          counters_executeCTU         },
    { "CTU_DINT",       sizeof( CTU_DINT ),     counters_parameterCTU_DINT,     counters_executeCTU_DINT    },
#ifdef CONFIG_SUPPORT_LONGLONG
    { "CTU_LINT",       sizeof( CTU_LINT ),     counters_parameterCTU_LINT,     counters_executeCTU_LINT    },
#endif
    { "CTU_UDINT",      sizeof( CTU_UDINT ),    counters_parameterCTU_UDINT,    counters_executeCTU_UDINT   },
#ifdef CONFIG_SUPPORT_LONGLONG
    { "CTU_ULINT",      sizeof( CTU_ULINT ),    counters_parameterCTU_ULINT,    counters_executeCTU_ULINT   },
#endif
    { "CTUD",           sizeof( CTUD ),         counters_parameterCTUD,         counters_executeCTUD        },
    { "CTUD_DINT",      sizeof( CTUD_DINT ),    counters_parameterCTUD_DINT,    counters_executeCTUD_DINT   },
#ifdef CONFIG_SUPPORT_LONGLONG
    { "CTUD_LINT",      sizeof( CTUD_LINT ),    counters_parameterCTUD_LINT,    counters_executeCTUD_LINT   },
#endif
    { "CTUD_UDINT",     sizeof( CTUD_UDINT ),   counters_parameterCTUD_UDINT,   counters_executeCTUD_UDINT  },
#ifdef CONFIG_SUPPORT_LONGLONG
    { "CTUD_ULINT",     sizeof( CTUD_ULINT ),   counters_parameterCTUD_ULINT,   counters_executeCTUD_ULINT  },
#endif

    /* 2.5.2.3.4 Timers */
#ifdef CONFIG_SUPPORT_DATETIME
    { "TOF",            sizeof( TIMER ),        timers_parameterTIMER,          timers_executeTOF           },
    { "TON",            sizeof( TIMER ),        timers_parameterTIMER,          timers_executeTON           },
    { "TP",             sizeof( TIMER ),        timers_parameterTIMER,          timers_executeTP            },
#endif
};


size_t IEC61131_FUNCTION_BLOCK_COUNT = ( sizeof( IEC61131_FUNCTION_BLOCK ) / sizeof( IEC61131_FUNCTION_BLOCK[0] ) );


