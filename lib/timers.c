#include <stdbool.h>
#include <string.h>
#include <errno.h>

#include "timers.h"


int
timers_executeTOF( char * Memory )
{
    TIMER *pInstance;
    bool bEnable;


    pInstance = ( TIMER * ) Memory;
    bEnable = ( pInstance->EN == BOOL_FALSE );

    if( ( bEnable ) &&
            ( pInstance->_EDGE == BOOL_TRUE ) )
    {
        pInstance->_TIME = _CYCLETIME;
        pInstance->_EDGE = BOOL_FALSE;
    }

    if( bEnable )
    {
        if( pInstance->Q )
        {
            pInstance->ET = ( _CYCLETIME - pInstance->_TIME );

            if( pInstance->ET >= pInstance->PT )
            {
                pInstance->ET = pInstance->PT;
                pInstance->Q = BOOL_FALSE;
            }
        }
    }
    else
    {
        pInstance->_EDGE = BOOL_TRUE;
        pInstance->Q = BOOL_TRUE;
        pInstance->ET = 0;
    }

    return 0;
}


int
timers_executeTON( char * Memory )
{
    TIMER *pInstance;
    bool bEnable;


    pInstance = ( TIMER * ) Memory;
    bEnable = ( pInstance->EN != BOOL_FALSE );

    if( ( bEnable ) &&
            ( pInstance->_EDGE == BOOL_FALSE ) )
    {
        pInstance->_TIME = _CYCLETIME;
        pInstance->_EDGE = BOOL_TRUE;
    }

    if( bEnable )
    {
        if( ! pInstance->Q )
        {
            pInstance->ET = ( _CYCLETIME - pInstance->_TIME );

            if( pInstance->ET >= pInstance->PT )
            {
                pInstance->ET = pInstance->PT;
                pInstance->Q = BOOL_TRUE;
            }
        }
    }
    else
    {
        pInstance->_EDGE = BOOL_FALSE;
        pInstance->Q = BOOL_FALSE;
        pInstance->ET = 0;
    }

    return 0;
}


int
timers_executeTP( char * Memory )
{
    TIMER *pInstance;
    bool bEnable, bQ;


    pInstance = ( TIMER * ) Memory;
    bEnable = ( pInstance->EN != BOOL_FALSE );

    if( ( bEnable ) &&
            ( pInstance->_EDGE == BOOL_FALSE ) )
    {
        pInstance->_TIME = _CYCLETIME;
        pInstance->_EDGE = BOOL_TRUE;
        bQ = true;
    }

    if( bQ )
    {
        pInstance->ET = ( _CYCLETIME - pInstance->_TIME );

        if( pInstance->ET >= pInstance->PT )
        {
            pInstance->ET = pInstance->PT;
            bQ = false;
        }
    }

    if( ( ! bQ ) &&
            ( ! bEnable ) )
    {
        pInstance->_TIME = 0;
        pInstance->_EDGE = BOOL_FALSE;
    }

    pInstance->Q = ( bQ ) ? BOOL_TRUE : BOOL_FALSE;

    return 0;
}


int
timers_parameterTIMER( char * Name, EXTERNAL_PARAMETER * Parameter, unsigned int * Offset, IEC61131_TYPE * Type )
{
    if( strcmp( Name, "EN" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( TIMER, EN );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "PT" ) == 0 )
    {
        *Parameter = PARAMETER_INPUT;
        *Offset = offsetof( TIMER, PT );
        *Type = TYPE_TIME;
    }
    else if( strcmp( Name, "Q" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( TIMER, Q );
        *Type = TYPE_BOOL;
    }
    else if( strcmp( Name, "ET" ) == 0 )
    {
        *Parameter = PARAMETER_OUTPUT;
        *Offset = offsetof( TIMER, ET );
        *Type = TYPE_TIME;
    }
    else
    {
        *Parameter = PARAMETER_NONE;
        *Offset = 0;
        *Type = TYPE_NONE;

        return -EFAULT;
    }

    return 0;
}
