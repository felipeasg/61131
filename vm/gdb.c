#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "gdb.h"
#include "log.h"


#define __UNUSED( __Object )    ( ( void ) ( ( __Object ) == ( __Object ) ) )


static void _gdb_acceptCallback( int nHandle, void *pParameter );

static void _gdb_closeSocket( GDB *pGDB, int nSocket );

static int _gdb_convertHexChar( unsigned char uChar );

static char * _gdb_convertMem2Hex( unsigned char *pMem, char *pBuffer, unsigned int uCount );

static int _gdb_createServer( GDB *pGDB, int nPort );

static void _gdb_discardPacket( GDB *pGDB );

static char * _gdb_parsePacket( GDB *pGDB );

static void _gdb_readCallback( int nHandle, void *pParameter );

static int _gdb_readQueryPacket( GDB *pGDB, char *pPacket );

static int _gdb_transmitByte( GDB *pGDB, unsigned char uByte );

static void _gdb_transmitPacket( GDB *pGDB );


static GDB stContext;

static const char _gdb_hexValues[] = "0123456789abcdef";


static void
_gdb_acceptCallback( int nHandle, void *pParameter )
{
    GDB *pGDB;
    VM *pVM;
    struct sockaddr_in stAddress;
    socklen_t uSize;
    int nSocket;


    pGDB = ( GDB * ) pParameter;
    /* assert( pGDB != NULL ); */
    /* assert( pGDB->Server == nHandle ); */
    pVM = ( VM * ) pGDB->Parent;

    uSize = sizeof( stAddress );
    if( ( nSocket = accept( nHandle, ( struct sockaddr * ) &stAddress, &uSize ) ) < 0 )
    {
        return /* -errno */;
    }

    LOG_INFO( "Accepted connection from %s",
            inet_ntoa( stAddress.sin_addr ) );
    pVM->State = VM_STATE_DEBUG;

    if( pGDB->Client >= 0 )
    {
        close( nSocket );
        return;
    }

    if( handle_addReadCallback( &pVM->Handles, nSocket, _gdb_readCallback, pGDB ) != 0 )
    {
        close( nSocket );
        return;
    }
    pGDB->Client = nSocket;
}


static void
_gdb_closeSocket( GDB *pGDB, int nSocket )
{
    VM *pVM;
    void *pData;


    if( nSocket >= 0 )
    {
        pVM = ( VM * ) pGDB->Parent;

        if( handle_deleteReadCallback( &pVM->Handles, nSocket, &pData ) == 0 )
        {
            /* assert( pData == pGDB ); */
            close( nSocket );
        }
    }
}


static int 
_gdb_convertHexChar( unsigned char uChar )
{
    if( ( uChar >= 'a' ) &&
            ( uChar <= 'f' ) )
    {
        return uChar - 'a' + 10;
    }
    if( ( uChar >= 'A' ) &&
            ( uChar <= 'F' ) )
    {
        return uChar - 'A' + 10;
    }
    if( ( uChar >= '0' ) &&
            ( uChar <= '9' ) )
    {
        return uChar - '0';
    }
    return -1;
}


static char * 
_gdb_convertMem2Hex( unsigned char *pMem, char *pBuffer, unsigned int uCount )
{
    char uChar;


    while( uCount-- > 0 )
    {
        uChar = *pMem++;
        *pBuffer++ = _gdb_hexValues[ ( uChar >> 4 ) & 0xf ];
        *pBuffer++ = _gdb_hexValues[ uChar & 0xf ];
    }
    *pBuffer = 0;

    return pBuffer;
}


static int
_gdb_createServer( GDB *pGDB, int nPort )
{
    VM *pVM;
    struct sockaddr_in stAddress;
    int nResult;


    pVM = ( VM * ) pGDB->Parent;

    ( void ) memset( &stAddress, 0, sizeof( stAddress ) );
    stAddress.sin_family = AF_INET;
    stAddress.sin_addr.s_addr = 0;
    stAddress.sin_port = htons( nPort );

    if( ( pGDB->Server = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 )
    {
        LOG_ERR( "Failed to create socket for gdb debugging (%d, %s)",
                errno,
                strerror( errno ) );

        return -errno;
    }

    if( bind( pGDB->Server, ( struct sockaddr * ) &stAddress, sizeof( stAddress ) ) < 0 )
    {
        LOG_ERR( "Failed to bind socket for gdb debugging (%d, %s)",
                errno,
                strerror( errno ) );

        close( pGDB->Server );
        return -errno;
    }

    if( listen( pGDB->Server, 2 ) < 0 )
    {
        close( pGDB->Server );
        return -errno;
    }

    if( ( nResult = handle_addReadCallback( &pVM->Handles, pGDB->Server, _gdb_acceptCallback, pGDB ) ) != 0 )
    {
        close( pGDB->Server );
        return -errno;
    }

    LOG_INFO( "Listening for debugger connections on port %d", nPort );

    return 0;
}


static void
_gdb_discardPacket( GDB *pGDB )
{
    unsigned int uIndex;


    for( uIndex = 0; uIndex < pGDB->ReceiveLength; ++uIndex )
    {
        if( pGDB->ReceiveBuffer[ uIndex ] == '#' )
        {
            break;
        }
    }
    if( uIndex >= ( pGDB->ReceiveLength - 2 ) )
    {
        pGDB->ReceiveLength = 0;
    }
    else
    {
        uIndex += 2;

        ( void ) memmove( &pGDB->ReceiveBuffer[0], &pGDB->ReceiveBuffer[ uIndex ], pGDB->ReceiveLength - uIndex );
        pGDB->ReceiveLength -= uIndex;
    }
}


static char *
_gdb_parsePacket( GDB *pGDB )
{
    unsigned int uCount, uIndex;
    unsigned char uChecksum, uTransmit;


    /*
        Iterate through the received data buffer for the protocol start character 
        '$' - All other characters up until this point are discarded.
    */

    for( uIndex = 0; uIndex < pGDB->ReceiveLength; ++uIndex )
    {
        if( pGDB->ReceiveBuffer[ uIndex ] == '$' )
        {
            break;
        }
    }
retry:
    if( uIndex > 0 )
    {
        if( uIndex >= pGDB->ReceiveLength )
        {
            pGDB->ReceiveLength = 0;
            return NULL;
        }
        else
        {
            ( void ) memmove( &pGDB->ReceiveBuffer[0], &pGDB->ReceiveBuffer[ uIndex ], pGDB->ReceiveLength - uIndex );
            pGDB->ReceiveLength -= uIndex;
        }
    }

    uChecksum = 0;
    uCount = 0;

    for( uIndex = 1; uIndex < ( pGDB->ReceiveLength - 2 ); ++uIndex )
    {
        if( pGDB->ReceiveBuffer[ uIndex ] == '$' ) 
        {
            goto retry;
        }
        if( pGDB->ReceiveBuffer[ uIndex ] == '#' )
        {
            break;
        }

        uChecksum += ( unsigned char ) pGDB->ReceiveBuffer[ uIndex ];
        ++uCount;
    }
    if( uIndex >= ( pGDB->ReceiveLength - 2 ) )
    {
        return NULL;
    }

    uTransmit = ( _gdb_convertHexChar( pGDB->ReceiveBuffer[ uIndex + 1 ] ) << 4 ) +
            _gdb_convertHexChar( pGDB->ReceiveBuffer[ uIndex + 2 ] );
    if( uChecksum != uTransmit )
    {
        _gdb_transmitByte( pGDB, '-' );

        return NULL;
    }
    else
    {
        _gdb_transmitByte( pGDB, '+' );
        if( pGDB->ReceiveBuffer[3] == ':' )
        {
            _gdb_transmitByte( pGDB, pGDB->ReceiveBuffer[1] );
            _gdb_transmitByte( pGDB, pGDB->ReceiveBuffer[2] );
        }

        return &pGDB->ReceiveBuffer[0];
    }
}


static void
_gdb_readCallback( int nHandle, void *pParameter )
{
    GDB *pGDB;
    VM *pVM;
    unsigned int uLength;
    char *pPacket, *pPointer;
    int nResult;


    pGDB = ( GDB * ) pParameter;
    /* assert( pGDB != NULL ); */
    /* assert( pGDB->Client == nHandle ); */
    pVM = ( VM * ) pGDB->Parent;

    uLength = sizeof( pGDB->ReceiveBuffer ) - pGDB->ReceiveLength;
    nResult = read( nHandle, &pGDB->ReceiveBuffer[ pGDB->ReceiveLength ], uLength );
    /* assert( ( uLength + ( unsigned int ) nResult ) <= sizeof( pGDB->ReceiveBuffer ) ); */
    if( ( nResult <= 0 ) &&
            ( errno != EINTR ) )
    {
        _gdb_closeSocket( pGDB, nHandle );
        pGDB->Client = -1;

        return;
    }
    pGDB->ReceiveLength += nResult;

    if( ( pPacket = _gdb_parsePacket( pGDB ) ) == NULL )
    {
        return;
    }

    pGDB->TransmitLength = 0;
    switch( pPacket[1] )
    {
        case '?':

            pGDB->TransmitBuffer[ pGDB->TransmitLength++ ] = 'S';
            pGDB->TransmitBuffer[ pGDB->TransmitLength++ ] = _gdb_hexValues[ ( pVM->Register.ER >> 4 ) & 0xf ];
            pGDB->TransmitBuffer[ pGDB->TransmitLength++ ] = _gdb_hexValues[ pVM->Register.ER & 0xf ];

            break;

        case 'c':

            pVM->State = VM_STATE_RUN;

            _gdb_discardPacket( pGDB );
            return;

            /* break; */

        case 'g':

            pPointer = _gdb_convertMem2Hex( ( unsigned char * ) &pVM->Register, pGDB->TransmitBuffer, sizeof( pVM->Register ) );
            pGDB->TransmitLength = ( pPointer - pGDB->TransmitBuffer );

            break;

        case 'H':

            /*
                Set thread for subsequent operations - Given that there is only a single 
                thread of operations within the IEC 61131-3 virtual machine, this packet is 
                silently acknowledged.
            */

            ( void ) snprintf( pGDB->TransmitBuffer, sizeof( pGDB->TransmitBuffer ), "OK" );
            pGDB->TransmitLength = strlen( pGDB->TransmitBuffer );

            break;
            
        case 'q':

            _gdb_readQueryPacket( pGDB, pPacket );
            break;

        case 'G':
        case 'm':
        case 'M':
        case 's':
        case 'k':
        case 'r':

            LOG_DEBUG( "gdb: %c", pPacket[1] );
        default:

            break;
    }

    _gdb_transmitPacket( pGDB );
    _gdb_discardPacket( pGDB );
}


static int
_gdb_readQueryPacket( GDB *pGDB, char *pPacket )
{
    if( strncmp( "qC", &pPacket[1], strlen( "qC" ) ) == 0 )
    {
        /*
            Return the current thread ID
        */

        ( void ) snprintf( pGDB->TransmitBuffer, sizeof( pGDB->TransmitBuffer ), "QC%x", GDB_TID );
        pGDB->TransmitLength = strlen( pGDB->TransmitBuffer );
    }
    else if( strncmp( "qAttached", &pPacket[1], strlen( "qAttached" ) ) == 0 )
    {
        /*
            Return an indication of whether the remote server attached to an existing 
            process or created a new process.  When the multiprocess protocol 
            extensions are supported, pid is an integer in hexadecimal format 
            identifying the target process.  Otherwise, GDB will omit the pid field and 
            the query packet will be simplified as 'qAttached'.

            This query is used to know whether the remote process should be detached or 
            killed when a GDB session is ended with the quit command.
        */

        pGDB->TransmitBuffer[ pGDB->TransmitLength++ ] = '1';
    }
    else if( strncmp( "qSupported", &pPacket[1], strlen( "qSupported" ) ) == 0 )
    {
        /*
            This packet allows GDB and the remote stub to take advantage of each others 
            features.  qSupported also consolidates multiple feature probes at startup, 
            to improve GDB performance a single larger packet performs better than 
            multiple smaller probe packets on high-latency links.
        */

        ( void ) snprintf( pGDB->TransmitBuffer, sizeof( pGDB->TransmitBuffer ), "PacketSize=%lu", sizeof( pGDB->TransmitBuffer ) );
        pGDB->TransmitLength = strlen( pGDB->TransmitBuffer );
    }
    else {}

    return 0;
}


static int
_gdb_transmitByte( GDB *pGDB, unsigned char uByte )
{
    if( write( pGDB->Client, &uByte, 1 ) != 1 )
    {
        return -errno;
    }
    return 0;
}


static void 
_gdb_transmitPacket( GDB *pGDB )
{
    unsigned int uIndex;
    unsigned char uChecksum;


    _gdb_transmitByte( pGDB, '$' );

    uChecksum = 0;
    for( uIndex = 0; uIndex < pGDB->TransmitLength; ++uIndex )
    {
        _gdb_transmitByte( pGDB, pGDB->TransmitBuffer[ uIndex ] );
        uChecksum += pGDB->TransmitBuffer[ uIndex ];
    }

    _gdb_transmitByte( pGDB, '#' );
    _gdb_transmitByte( pGDB, _gdb_hexValues[ ( uChecksum >> 4 ) & 0xf ] );
    _gdb_transmitByte( pGDB, _gdb_hexValues[ uChecksum & 0xf ] );
}


/*!
    \fn GDB * gdb_initialise( VM *pVM )
    \return Returns pointer to GNU debugger (gdb) context data structure on success, NULL on error
*/

GDB *
gdb_initialise( VM *pVM )
{
    /* assert( pContext != NULL ); */
    ( void ) memset( &stContext, 0, sizeof( stContext ) );
    stContext.Parent = pVM;
    stContext.Server = -1;
    stContext.Client = -1;

    if( _gdb_createServer( &stContext, GDB_TCP_PORT ) != 0 )
    {
        return NULL;
    }
    return &stContext;
}


void
gdb_destroy( void *pPointer )
{
    __UNUSED( pPointer );
}


