#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#include "log.h"
#include "target.h"
#include "vm.h"


int
main( int nArgc, char **pArgv )
{
    VM stVM;
    struct timeval stTimeout;
    unsigned int uCycle;
    uint32_t uStart, uTimeout;
    int nResult;


    ( void ) memset( &stVM, 0, sizeof( stVM ) );
    vm_initialise( &stVM );

    if( ( nResult = vm_parseArguments( &stVM, nArgc, pArgv ) ) != 0 )
    {
        return nResult;
    }

    vm_start( &stVM );

    while( vm_getKernelState( &stVM ) != VM_STATE_SHUTDOWN )
    {
        uStart = target_getMillisecondCounter();
        vm_executeCycle( &stVM );

        uCycle = vm_getCycleTime( &stVM );
        if( uCycle > 0 )
        {
            uTimeout = uCycle - ( ( target_getMillisecondCounter() - uStart ) % uCycle );
            stTimeout.tv_sec = ( uTimeout / 1000 );
            stTimeout.tv_usec = ( uTimeout % 1000 ) * 1000;

            ( void ) handle_processHandles( &stVM.Handles, &stTimeout );
        }
    }

    vm_finish( &stVM );

    return 0;
}
