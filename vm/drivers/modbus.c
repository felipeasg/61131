#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "io.h"
#include "log.h"
#include "vm.h"

#include "modbus.h"


#define _MODBUS_ERR_ILLEGAL_FUNCTION                    (1)
#define _MODBUS_ERR_ILLEGAL_DATA_ADDRESS                (2)
#define _MODBUS_ERR_ILLEGAL_DATA_VALUE                  (3)
#define _MODBUS_ERR_SLAVE_DEVICE_FAILURE                (4)
#define _MODBUS_ERR_ACKNOWLEDGE                         (5)
#define _MODBUS_ERR_SLAVE_DEVICE_BUSY                   (6)
#define _MODBUS_ERR_NAK                                 (7)
#define _MODBUS_ERR_MEMORY_PARITY_ERROR                 (8)


/*
    The following defines specify the Modbus application function codes supported
    by this embedded slave library.
*/

#define _MODBUS_FC_READ_COILS                           (1)
#define _MODBUS_FC_READ_DISCRETE_INPUTS                 (2)
#define _MODBUS_FC_READ_HOLDING_REGISTERS               (3)
#define _MODBUS_FC_READ_INPUT_REGISTERS                 (4)
#define _MODBUS_FC_WRITE_SINGLE_COIL                    (5)
#define _MODBUS_FC_WRITE_SINGLE_REGISTER                (6)
#define _MODBUS_FC_WRITE_MULTIPLE_COILS                 (15)
#define _MODBUS_FC_WRITE_MULTIPLE_REGISTERS             (16)


/*
    The following defines specify the maximum number of each type of register that
    can be read using the given Modbus protocol function code.  The primary
    limitation on the number of each type of register that can be read is the 8-bit
    field width of the length field in the Modbus protocol specification.
*/

#define _MODBUS_MAX_READ_COILS                          (2000)
#define _MODBUS_MAX_READ_DISCRETE_INPUTS                (2000)
#define _MODBUS_MAX_READ_HOLDING_REGISTERS              (125)
#define _MODBUS_MAX_READ_INPUT_REGISTERS                (125)
#define _MODBUS_MAX_WRITE_MULTIPLE_COILS                (2000)
#define _MODBUS_MAX_WRITE_MULTIPLE_REGISTERS            (125)

#define _MODBUS_RTU_OFFSET_ADDRESS                      (0)
#define _MODBUS_RTU_OFFSET_FUNCTION_CODE                (1)
#define _MODBUS_RTU_OFFSET_DATA                         (2)

#define _MODBUS_TCP_OFFSET_TRANSACTION_ID_MSB           (0)
#define _MODBUS_TCP_OFFSET_TRANSACTION_ID_LSB           (1)
#define _MODBUS_TCP_OFFSET_PROTOCOL_ID_MSB              (2)
#define _MODBUS_TCP_OFFSET_PROTOCOL_ID_LSB              (3)
#define _MODBUS_TCP_OFFSET_LENGTH_MSB                   (4)
#define _MODBUS_TCP_OFFSET_LENGTH_LSB                   (5)
#define _MODBUS_TCP_OFFSET_UNIT_IDENTIFIER              (6)
#define _MODBUS_TCP_OFFSET_FUNCTION_CODE                (7)
#define _MODBUS_TCP_OFFSET_DATA                         (8)


static MODBUS stModbus;


static void _modbus_acceptCallback( int nHandle, void *pParameter );

static int _modbus_checkCoils( MODBUS *pModbus, unsigned int uStart, unsigned int uCount );

static int _modbus_checkHoldingRegisters( MODBUS *pModbus, unsigned int uStart, unsigned int uCount );

static void _modbus_closeSocket( MODBUS *pModbus, int nSocket );

static int _modbus_createServer( MODBUS *pModbus, int nPort );

static void _modbus_dispatchException( MODBUS *pModbus, unsigned char Exception );

static int _modbus_processReadCoils( MODBUS *pModbus );

static int _modbus_processReadHoldingRegisters( MODBUS *pModbus );

static int _modbus_processWriteMultipleCoils( MODBUS *pModbus );

static int _modbus_processWriteMultipleRegisters( MODBUS *pModbus );

static int _modbus_processWriteSingleCoil( MODBUS *pModbus );

static int _modbus_processWriteSingleRegister( MODBUS *pModbus );

static int _modbus_readCoils( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData );

static int _modbus_readHoldingRegisters( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData );

static void _modbus_receiveCallback( int nHandle, void *pParameter );

static int _modbus_receiveCallbackTCP( MODBUS *pModbus );

static int _modbus_transmitCallbackTCP( MODBUS *pModbus );

static int _modbus_writeCoils( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData );

static int _modbus_writeHoldingRegisters( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData );


static void
_modbus_acceptCallback( int nHandle, void *pParameter )
{
    MODBUS *pModbus;
    VM *pVM;
    struct sockaddr_in stAddress;
    socklen_t uSize;
    int nSocket;


    pModbus = ( MODBUS * ) pParameter;
    /* assert( pModbus != NULL ); */
    /* assert( pModbus->Server == nHandle ); */
    pVM = ( VM * ) pModbus->Parent;

    uSize = sizeof( stAddress );
    if( ( nSocket = accept( nHandle, ( struct sockaddr * ) &stAddress, &uSize ) ) < 0 )
    {
        return /* -errno */;
    }

    LOG_INFO( "Accepted connection from %s",
            inet_ntoa( stAddress.sin_addr ) );

    if( pModbus->Client >= 0 )
    {
        close( nSocket );
        return;
    }

    if( handle_addReadCallback( &pVM->Handles, nSocket, _modbus_receiveCallback, pModbus ) != 0 )
    {
        close( nSocket );
        return;
    }
    pModbus->Client = nSocket;
}


static int 
_modbus_checkCoils( MODBUS *pModbus, unsigned int uStart, unsigned int uCount )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uIndex;


    for( uIndex = 0; uIndex < pModbus->RegisterCount; ++uIndex )
    {
        pRegister = ( MODBUS_REGISTER * ) &pModbus->Register[ uIndex ];
        if( pRegister->Type != MODBUS_TYPE_COIL )
        {
            continue;
        }
        if( ( pRegister->Start <= uStart ) &&
                ( ( pRegister->Start + pRegister->Count ) >= ( uStart + uCount ) ) )
        {
            return 0;
        }
    }

    return -1;
}


static int 
_modbus_checkHoldingRegisters( MODBUS *pModbus, unsigned int uStart, unsigned int uCount )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uIndex;


    for( uIndex = 0; uIndex < pModbus->RegisterCount; ++uIndex )
    {
        pRegister = ( MODBUS_REGISTER * ) &pModbus->Register[ uIndex ];
        if( pRegister->Type != MODBUS_TYPE_HOLDING_REGISTER )
        {
            continue;
        }
        if( ( pRegister->Start <= uStart ) &&
                ( ( pRegister->Start + pRegister->Count ) >= ( uStart + uCount ) ) )
        {
            return 0;
        }
    }

    return -1;
}


static void 
_modbus_closeSocket( MODBUS *pModbus, int nSocket )
{
    VM *pVM;
    void *pData;


    if( nSocket >= 0 )
    {
        pVM = ( VM * ) pModbus->Parent;

        if( handle_deleteReadCallback( &pVM->Handles, nSocket, &pData ) == 0 )
        {
            /* assert( pData == pModbus ); */
            close( nSocket );
        }
    }
}


static int
_modbus_createServer( MODBUS *pModbus, int nPort )
{
    VM *pVM;
    struct sockaddr_in stAddress;
    int nResult;


    pVM = ( VM * ) pModbus->Parent;

    ( void ) memset( &stAddress, 0, sizeof( stAddress ) );
    stAddress.sin_family = AF_INET;
    stAddress.sin_addr.s_addr = 0;
    stAddress.sin_port = htons( nPort );

    if( ( pModbus->Server = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 )
    {
        LOG_ERR( "Failed to create socket for Modbus operations (%d, %s)",
                errno,
                strerror( errno ) );

        return -errno;
    }

    if( bind( pModbus->Server, ( struct sockaddr * ) &stAddress, sizeof( stAddress ) ) < 0 )
    {
        LOG_ERR( "Failed to bind socket for Modbus operations (%d, %s)",
                errno,
                strerror( errno ) );

        close( pModbus->Server );
        return -errno;
    }

    if( listen( pModbus->Server, 2 ) < 0 )
    {
        close( pModbus->Server );
        return -errno;
    }

    if( ( nResult = handle_addReadCallback( &pVM->Handles, pModbus->Server, _modbus_acceptCallback, pModbus ) ) != 0 )
    {
        close( pModbus->Server );
        return -errno;
    }

    LOG_INFO( "Listening for Modbus operations on port %d", nPort );

    return 0;
}


static void
_modbus_dispatchException( MODBUS *pModbus, unsigned char Exception )
{
    uint8_t uAddress, uFunction;


    uAddress = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
    uFunction = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

    pModbus->MessageLength = 0;
    pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uAddress;
    pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uFunction | 0x80 );
    pModbus->MessageBuffer[ pModbus->MessageLength++ ] = Exception;

    _modbus_transmitCallbackTCP( pModbus );
}


static int 
_modbus_processReadCoils( MODBUS *pModbus )
{
    char sBuffer[ ( ( _MODBUS_MAX_READ_HOLDING_REGISTERS >> 3 ) + 1 ) ];
    uint16_t uCount, uStart;
    uint8_t uAddress, uBytes, uFunction;


    uStart = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 1 ] ) & 0x00ff ) );
    uCount = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 3 ] ) & 0x00ff ) );

    ( void ) memset( sBuffer, 0, sizeof( sBuffer ) );

    if( uCount > _MODBUS_MAX_READ_COILS )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( _modbus_checkCoils( pModbus, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( _modbus_readCoils( pModbus, uStart, uCount, sBuffer ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        uBytes = ( uCount / 8 ) + ( ( ( uCount % 8 ) != 0 ) ? 1 : 0 );

        pModbus->MessageLength = 0;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uAddress;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uFunction;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uBytes;

        ( void ) memcpy( &pModbus->MessageBuffer[ pModbus->MessageLength ], sBuffer, uBytes );
        pModbus->MessageLength += uBytes;
    }

    return 0;
}


static int 
_modbus_processReadHoldingRegisters( MODBUS *pModbus )
{
    char sBuffer[ ( _MODBUS_MAX_READ_HOLDING_REGISTERS << 1 ) ];
    uint16_t uCount, uStart;
    uint8_t uAddress, uFunction;


    uStart = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 1 ] ) & 0x00ff ) );
    uCount = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 3 ] ) & 0x00ff ) );

    ( void ) memset( sBuffer, 0, sizeof( sBuffer ) );

    if( uCount > _MODBUS_MAX_READ_HOLDING_REGISTERS )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( _modbus_checkHoldingRegisters( pModbus, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( _modbus_readHoldingRegisters( pModbus, uStart, uCount, sBuffer ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        pModbus->MessageLength = 0;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uAddress;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uFunction;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uCount * 2 );

        ( void ) memcpy( &pModbus->MessageBuffer[ pModbus->MessageLength ], sBuffer, uCount * 2 );
        pModbus->MessageLength += ( uCount * 2 );
    }

    return 0;
}


static int 
_modbus_processWriteMultipleCoils( MODBUS *pModbus )
{
    uint16_t uCount, uLength, uStart;
    uint8_t uAddress, uBytes, uFunction;


    uStart = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 1 ] ) & 0x00ff ) );
    uCount = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 3 ] ) & 0x00ff ) );
    uLength = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 4 ];

    uBytes = ( uCount / 8 ) + ( ( ( uCount % 8 ) != 0 ) ? 1 : 0 );

    if( ( uCount > _MODBUS_MAX_WRITE_MULTIPLE_COILS ) ||
            ( uLength != uBytes ) )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( _modbus_checkCoils( pModbus, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( _modbus_writeCoils( pModbus, uStart, uCount, ( char * ) &pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 5 ] ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        pModbus->MessageLength = 0;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uAddress;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uFunction;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uStart >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uStart & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uCount >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uCount & 0xff );
    }

    return 0;
}


static int 
_modbus_processWriteMultipleRegisters( MODBUS *pModbus )
{
    uint16_t uCount, uLength, uStart;
    uint8_t uAddress, uFunction;


    uStart = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 1 ] ) & 0x00ff ) );
    uCount = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 3 ] ) & 0x00ff ) );
    uLength = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 4 ];

    if( ( uCount > _MODBUS_MAX_WRITE_MULTIPLE_REGISTERS ) ||
            ( uLength != ( uCount * 2 ) ) )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_VALUE );
        return -1;
    }
    else if( _modbus_checkHoldingRegisters( pModbus, uStart, uCount ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( _modbus_writeHoldingRegisters( pModbus, uStart, uCount, ( char * ) &pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 5 ] ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        pModbus->MessageLength = 0;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uAddress;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uFunction;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uStart >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uStart & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uCount >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uCount & 0xff );
    }

    return 0;
}


static int 
_modbus_processWriteSingleCoil( MODBUS *pModbus )
{
    uint16_t uStart, uValue;
    uint8_t uAddress, uFunction;


    uStart = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 1 ] ) & 0x00ff ) );
    uValue = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 3 ] ) & 0x00ff ) );

    if( _modbus_checkCoils( pModbus, uStart, 1 ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else
    {
    }

    uValue = ( uValue > 0 ) ? 1 : 0;
    if( _modbus_writeCoils( pModbus, uStart, 1, ( char * ) &pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        pModbus->MessageLength = 0;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uAddress;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uFunction;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uStart >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uStart & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uValue >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uValue & 0xff );
    }

    return 0;
}


static int 
_modbus_processWriteSingleRegister( MODBUS *pModbus )
{
    uint16_t uStart, uValue;
    uint8_t uAddress, uFunction;


    uStart = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 0 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 1 ] ) & 0x00ff ) );
    uValue = ( ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] << 8 ) & 0xff00 ) |
            ( ( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 3 ] ) & 0x00ff ) );

    if( _modbus_checkHoldingRegisters( pModbus, uStart, 1 ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_ILLEGAL_DATA_ADDRESS );
        return -1;
    }
    else if( _modbus_writeHoldingRegisters( pModbus, uStart, 1, ( char * ) &pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_DATA + 2 ] ) != 0 )
    {
        _modbus_dispatchException( pModbus, _MODBUS_ERR_SLAVE_DEVICE_FAILURE );
        return -1;
    }
    else
    {
        uAddress = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_ADDRESS ];
        uFunction = pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ];

        pModbus->MessageLength = 0;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uAddress;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = uFunction;
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uStart >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uStart & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( ( uValue >> 8 ) & 0xff );
        pModbus->MessageBuffer[ pModbus->MessageLength++ ] = ( uValue & 0xff );
    }

    return 0;
}


static int 
_modbus_readCoils( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uBit, uByte, uIndex, uNumber;


    for( uIndex = 0; uIndex < pModbus->RegisterCount; ++uIndex )
    {
        pRegister = ( MODBUS_REGISTER * ) &pModbus->Register[ uIndex ];
        if( pRegister->Type != MODBUS_TYPE_COIL )
        {
            continue;
        }
        if( ( pRegister->Start <= uStart ) &&
                ( ( pRegister->Start + pRegister->Count ) >= ( uStart + uCount ) ) )
        {
            for( uNumber = 0; uNumber < uCount; ++uNumber )
            {
                uByte = ( ( uStart - pRegister->Start + uNumber ) / 8 );
                uBit = ( ( uStart - pRegister->Start + uNumber ) % 8 );

                if( ( pRegister->Data[ uByte ] & ( 1 << uBit ) ) != 0 )
                {
                    pData[ uNumber / 8 ] |= ( 1 << ( uNumber % 8 ) );
                }
                else
                {
                    pData[ uNumber / 8 ] &= ~( 1 << ( uNumber % 8 ) );
                }
            }

            return 0;
        }
    }

    return -1;
}


static int 
_modbus_readHoldingRegisters( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uIndex;


    for( uIndex = 0; uIndex < pModbus->RegisterCount; ++uIndex )
    {
        pRegister = ( MODBUS_REGISTER * ) &pModbus->Register[ uIndex ];
        if( pRegister->Type != MODBUS_TYPE_HOLDING_REGISTER )
        {
            continue;
        }
        if( ( pRegister->Start <= uStart ) &&
                ( ( pRegister->Start + pRegister->Count ) >= ( uStart + uCount ) ) )
        {
            ( void ) memcpy( pData, &pRegister->Data[ ( uStart - pRegister->Start ) * 2 ], uCount * 2 );

            return 0;
        }
    }

    return -1;
}


static void 
_modbus_receiveCallback( int nHandle, void *pParameter )
{
    MODBUS *pModbus;
    /* VM *pVM; */
    unsigned int uCount, uLength;
    int nResult;


    pModbus = ( MODBUS * ) pParameter;
    /* assert( pModbus != NULL ); */
    /* assert( pModbus->Client == nHandle ); */
    /* pVM = ( VM * ) pModbus->Parent; */

    uLength = sizeof( pModbus->ReceiveBuffer ) - pModbus->ReceiveLength;
    nResult = read( nHandle, &pModbus->ReceiveBuffer[ pModbus->ReceiveLength ], uLength );
    /* assert( ( uLength + ( unsigned int ) nResult ) <= sizeof( pModbus->ReceiveBuffer ) ); */
    if( ( nResult <= 0 ) &&
            ( errno != EINTR ) )
    {
        _modbus_closeSocket( pModbus, nHandle );
        pModbus->Client = -1;

        return;
    }
    pModbus->ReceiveLength += nResult;

    uCount = _modbus_receiveCallbackTCP( pModbus );
    if( uCount < pModbus->ReceiveLength )
    {
        ( void ) memmove( &pModbus->ReceiveBuffer[0], &pModbus->ReceiveBuffer[ uCount ], pModbus->ReceiveLength - uCount );
        pModbus->ReceiveLength -= uCount;
    }
    else
    {
        pModbus->ReceiveLength = 0;
    }
}


static int 
_modbus_receiveCallbackTCP( MODBUS *pModbus )
{
    uint16_t uLength, uId;
    unsigned int uIndex;
    int nResult;


    /*
        The following code is intended to iterate through the receive buffer and
        parse any Modbus TCP frames which may be present.  This operation is
        performed in a looped fashion such that junk bytes which may be directed
        to the Modbus server may be read and removed from the buffer without
        disturbing the parsing of valid Modbus protocol frames.  While it could
        be argued that this parsing code should simply parse for message length
        and ensure that a corresponding number of bytes have been received, the
        approach taken below, where this initial message parsing additionally
        incorporates synchronisation based upon the Modbus protocol identifier,
        has been adopted to minimise the length of processing time required to
        appropriately identify and discard junk data.
    */

    if( pModbus->ReceiveLength < 8 )
    {
        return 0;
    }

    for( uIndex = 0; uIndex < ( pModbus->ReceiveLength - 8u ); ++uIndex )
    {
        uId = ( ( ( pModbus->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_PROTOCOL_ID_MSB ] << 8 ) & 0xff00 ) |
                ( ( pModbus->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_PROTOCOL_ID_LSB ] ) & 0x00ff ) );
        if( uId != 0 )
        {
            continue;
        }

        pModbus->Transaction = ( ( ( pModbus->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_TRANSACTION_ID_MSB ] << 8 ) & 0xff00 ) |
                ( ( pModbus->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_TRANSACTION_ID_LSB ] ) & 0x00ff ) );

        uLength = ( ( ( pModbus->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_LENGTH_MSB ] << 8 ) & 0xff00 ) |
                ( ( pModbus->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_LENGTH_LSB ] ) & 0x00ff ) );
        if( pModbus->ReceiveLength < ( uLength + 6u ) )
        {
            return 0;
        }


        /*
            At this point, a complete Modbus TCP message frame has been received - The
            unit identifier, function code and associated message data is copied into
            the MessageBuffer element of the MODBUS data structure for processing of 
            the corresponding application layer message callback functions.  This is 
            purposely so as to detach the processing of Modbus application messages 
            from the differing encoding formats supported.
        */

        ( void ) memcpy( &pModbus->MessageBuffer[0], &pModbus->ReceiveBuffer[ uIndex + _MODBUS_TCP_OFFSET_UNIT_IDENTIFIER ], uLength );
        pModbus->MessageLength = uLength;

        switch( pModbus->MessageBuffer[ _MODBUS_RTU_OFFSET_FUNCTION_CODE ] & 0xff )
        {
            case _MODBUS_FC_READ_COILS:

                nResult = _modbus_processReadCoils( pModbus );
                break;

            case _MODBUS_FC_READ_HOLDING_REGISTERS:

                nResult = _modbus_processReadHoldingRegisters( pModbus );
                break;

            case _MODBUS_FC_WRITE_MULTIPLE_COILS:

                nResult = _modbus_processWriteMultipleCoils( pModbus );
                break;

            case _MODBUS_FC_WRITE_MULTIPLE_REGISTERS:

                nResult = _modbus_processWriteMultipleRegisters( pModbus );
                break;

            case _MODBUS_FC_WRITE_SINGLE_COIL:

                nResult = _modbus_processWriteSingleCoil( pModbus );
                break;

            case _MODBUS_FC_WRITE_SINGLE_REGISTER:

                nResult = _modbus_processWriteSingleRegister( pModbus );
                break;

            case _MODBUS_FC_READ_DISCRETE_INPUTS:
            case _MODBUS_FC_READ_INPUT_REGISTERS:
            default:

                nResult = -1;
                break;
        }
        if( nResult == 0 )
        {
            _modbus_transmitCallbackTCP( pModbus );
        }

        uIndex += ( uLength + 6u );
    }

    return uIndex;
}


static int
_modbus_transmitCallbackTCP( MODBUS *pModbus )
{
    int nResult;


    pModbus->TransmitLength = 0;
    pModbus->TransmitBuffer[ pModbus->TransmitLength++ ] = ( ( pModbus->Transaction >> 8 ) & 0xff );
    pModbus->TransmitBuffer[ pModbus->TransmitLength++ ] = ( pModbus->Transaction & 0xff );
    pModbus->TransmitBuffer[ pModbus->TransmitLength++ ] = 0;
    pModbus->TransmitBuffer[ pModbus->TransmitLength++ ] = 0;
    pModbus->TransmitBuffer[ pModbus->TransmitLength++ ] = ( ( pModbus->MessageLength >> 8 ) & 0xff );
    pModbus->TransmitBuffer[ pModbus->TransmitLength++ ] = ( pModbus->MessageLength & 0xff );


    /*
        The following code copies the station address, function code and any data
        bytes already populated in the message buffer into transmission buffer.
    */

    ( void ) memcpy( &pModbus->TransmitBuffer[ pModbus->TransmitLength ], &pModbus->MessageBuffer[0], pModbus->MessageLength );
    pModbus->TransmitLength += pModbus->MessageLength;

    if( ( nResult = write( pModbus->Client, pModbus->TransmitBuffer, pModbus->TransmitLength ) ) <= 0 )
    {
        _modbus_closeSocket( pModbus, pModbus->Client );
    }
    return nResult;
}


static int 
_modbus_writeCoils( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uBit, uByte, uIndex, uNumber;


    for( uIndex = 0; uIndex < pModbus->RegisterCount; ++uIndex )
    {
        pRegister = ( MODBUS_REGISTER * ) &pModbus->Register[ uIndex ];
        if( pRegister->Type != MODBUS_TYPE_COIL )
        {
            continue;
        }
        if( ( pRegister->Start <= uStart ) &&
                ( ( pRegister->Start + pRegister->Count ) >= ( uStart + uCount ) ) )
        {
            for( uNumber = 0; uNumber < uCount; ++uNumber )
            {
                uByte = ( ( uStart - pRegister->Start + uNumber ) / 8 );
                uBit = ( ( uStart - pRegister->Start + uNumber ) % 8 );

                if( ( pData[ uNumber / 8 ] & ( 1 << ( uNumber % 8 ) ) ) != 0 )
                {
                    pRegister->Data[ uByte ] |= ( 1 << uBit );
                }
                else
                {
                    pRegister->Data[ uByte ] &= ~( 1 << uBit );
                }
            }

            return 0;
        }
    }

    return -1;
}


static int 
_modbus_writeHoldingRegisters( MODBUS *pModbus, unsigned int uStart, unsigned int uCount, char *pData )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uIndex;


    for( uIndex = 0; uIndex < pModbus->RegisterCount; ++uIndex )
    {
        pRegister = ( MODBUS_REGISTER * ) &pModbus->Register[ uIndex ];
        if( pRegister->Type != MODBUS_TYPE_HOLDING_REGISTER )
        {
            continue;
        }
        if( ( pRegister->Start <= uStart ) &&
                ( ( pRegister->Start + pRegister->Count ) >= ( uStart + uCount ) ) )
        {
            ( void ) memcpy( &pRegister->Data[ ( uStart - pRegister->Start ) * 2 ], pData, uCount * 2 );

            return 0;
        }
    }

    return -1;
}


void *
modbus_initialise( void *pContext )
{
    IO_DRIVER *pDriver;
    MODBUS_REGISTER *pRegister;
    unsigned int uIndex;
    char sWidth, szDefinition[32];


    ( void ) memset( &stModbus, 0, sizeof( stModbus ) );
    stModbus.Parent = pContext;
    stModbus.Server = -1;
    stModbus.Client = -1;

    if( _modbus_createServer( &stModbus, MODBUS_TCP_PORT ) != 0 )
    {
        return NULL;
    }

    pDriver = ( IO_DRIVER * ) &stModbus;
    /* pDriver->Initialise = modbus_initialise; */
    pDriver->Destroy = modbus_destroy;
    pDriver->Read = modbus_readInputs;
    pDriver->Write = modbus_writeOutputs;


    /*
        The following code registers 10 single bit memory I/O points and 10 16-bit 
        word memory I/O points intended to provide coil and holding register I/O 
        points for access via the Modbus protocol.  This definition is likely to 
        be expanded as support for non-contiguous register ranges are implemented
        for this protocol driver.
    */

#if 0
    pRegister = &stModbus.Register[ stModbus.RegisterCount++ ];
    pRegister->Context = &stModbus;
    pRegister->Type = MODBUS_TYPE_HOLDING_REGISTER;
    pRegister->Start = 0;
    pRegister->Count = 10;

#endif
#if 1
    pRegister = &stModbus.Register[ stModbus.RegisterCount++ ];
    pRegister->Context = &stModbus;
    pRegister->Type = MODBUS_TYPE_COIL;
    pRegister->Start = 0;
    pRegister->Count = 10;

#endif
    for( uIndex = 0; uIndex < stModbus.RegisterCount; ++uIndex )
    {
        pRegister = &stModbus.Register[ uIndex ];
        switch( pRegister->Type )
        {
            case MODBUS_TYPE_COIL:

                LOG_DEBUG( "Registering Modbus coil registers %u..%u",
                        pRegister->Start,
                        pRegister->Start + pRegister->Count - 1 );
                sWidth = 'X';
                break;

            case MODBUS_TYPE_DISCRETE_INPUT:

                LOG_DEBUG( "Registering Modbus discrete input registers %u..%u",
                        pRegister->Start,
                        pRegister->Start + pRegister->Count - 1 );
                sWidth = 'X';
                break;

            case MODBUS_TYPE_HOLDING_REGISTER:

                LOG_DEBUG( "Registering Modbus holding registers %u..%u",
                        pRegister->Start,
                        pRegister->Start + pRegister->Count - 1 );
                sWidth = 'W';
                break;

            case MODBUS_TYPE_INPUT_REGISTER:

                LOG_DEBUG( "Registering Modbus input registers %u..%u",
                        pRegister->Start,
                        pRegister->Start + pRegister->Count - 1 );
                sWidth = 'W';
                break;

            default:

                continue;
        }

        ( void ) snprintf( szDefinition, sizeof( szDefinition ), "%%M%c%u", sWidth, pRegister->Count );
        if( io_registerBlock( pContext, szDefinition, pRegister, pDriver ) != 0 )
        {
            return NULL;
        }
    }

    return &stModbus;
}


void
modbus_destroy( void *pPointer )
{
    MODBUS *pModbus;


    pModbus = ( MODBUS * ) pPointer;
    if( pModbus != NULL )
    {
        _modbus_closeSocket( pModbus, pModbus->Client );
        _modbus_closeSocket( pModbus, pModbus->Server );
    }
}


void
modbus_readInputs( char *pData, void *pContext )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uIndex, uMask;


    pRegister = ( MODBUS_REGISTER * ) pContext;
    /* assert( pRegister != NULL ); */
    for( uIndex = 0; uIndex < pRegister->Count; ++uIndex )
    {
        switch( pRegister->Type )
        {
            case MODBUS_TYPE_COIL:
            case MODBUS_TYPE_DISCRETE_INPUT:

                uMask = ( 1 << ( uIndex % 8 ) );
                if( ( pRegister->Data[ uIndex / 8 ] & uMask ) != 0 )
                {
                    pData[ uIndex / 8 ] |= uMask;
                }
                else
                {
                    pData[ uIndex / 8 ] &= ~uMask;
                }
                break;

            case MODBUS_TYPE_HOLDING_REGISTER:
            case MODBUS_TYPE_INPUT_REGISTER:

                pData[ ( uIndex * 2 ) + 0 ] = pRegister->Data[ ( uIndex * 2 ) + 0 ];
                pData[ ( uIndex * 2 ) + 1 ] = pRegister->Data[ ( uIndex * 2 ) + 1 ];
                break;

            default:

                continue;
        }
    }
}


void
modbus_writeOutputs( char *pData, void *pContext )
{
    MODBUS_REGISTER *pRegister;
    unsigned int uIndex, uMask;


    pRegister = ( MODBUS_REGISTER * ) pContext;
    /* assert( pRegister != NULL ); */
    for( uIndex = 0; uIndex < pRegister->Count; ++uIndex )
    {
        switch( pRegister->Type )
        {
            case MODBUS_TYPE_COIL:
            case MODBUS_TYPE_DISCRETE_INPUT:

                uMask = ( 1 << ( uIndex % 8 ) );
                if( ( pData[ uIndex / 8 ] & uMask ) != 0 )
                {
                    pRegister->Data[ uIndex / 8 ] |= uMask;
                }
                else
                {
                    pRegister->Data[ uIndex / 8 ] &= ~uMask;
                }
                break;

            case MODBUS_TYPE_HOLDING_REGISTER:
            case MODBUS_TYPE_INPUT_REGISTER:

                pRegister->Data[ ( uIndex * 2 ) + 0 ] = pData[ ( uIndex * 2 ) + 0 ];
                pRegister->Data[ ( uIndex * 2 ) + 1 ] = pData[ ( uIndex * 2 ) + 1 ];
                break;

            default:

                continue;
        }
    }
}

