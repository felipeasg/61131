#ifndef __EXAMPLE_H
#define __EXAMPLE_H


#include "io.h"


/*
    This header defines the functions associated with an example I/O driver - This 
    minimal I/O driver is intended to demonstrate the integration of I/O interface 
    within the IEC 61131-3 virtual machine run-time.  This example I/O interface 
    provides two digital input channels - one of which is permanently set to false 
    and the other true - and eight digital output channels.
*/

typedef struct _EXAMPLE_CONTEXT
{
    IO_DRIVER Driver;

    uint8_t Inputs;

    uint8_t Outputs;
}
EXAMPLE_CONTEXT;


void * example_initialise( void *pContext );

void example_destroy( void *pPointer );

void example_readInputs( char *pData, void *pContext );

void example_writeOutputs( char *pData, void *pContext );


#endif
