#ifndef __MODBUS_H
#define __MODBUS_H


#include "io.h"


#define MODBUS_TCP_PORT                         (1502)

#define MODBUS_MESSAGE_BUFFER_MAX               (256)
#define MODBUS_RECEIVE_BUFFER_MAX               (512)
#define MODBUS_TRANSMIT_BUFFER_MAX              (256)


typedef enum _MODBUS_TYPE
{
    MODBUS_TYPE_COIL = 0,
    MODBUS_TYPE_DISCRETE_INPUT,
    MODBUS_TYPE_HOLDING_REGISTER,
    MODBUS_TYPE_INPUT_REGISTER
}
MODBUS_TYPE;

typedef struct _MODBUS_REGISTER
{
    void * Context;

    unsigned int Type;

    unsigned int Start;

    unsigned int Count;

    char Data[20];
}
MODBUS_REGISTER;

typedef struct _MODBUS
{
    IO_DRIVER Driver;

    void * Parent;

    int Server;

    int Client;

    unsigned short Transaction;

    unsigned char MessageBuffer[ MODBUS_MESSAGE_BUFFER_MAX ];

    unsigned int MessageLength;

    unsigned char ReceiveBuffer[ MODBUS_RECEIVE_BUFFER_MAX ];

    unsigned int ReceiveLength;

    unsigned char TransmitBuffer[ MODBUS_TRANSMIT_BUFFER_MAX ];

    unsigned int TransmitLength;

    MODBUS_REGISTER Register[10];

    unsigned int RegisterCount;
}
MODBUS;


void * modbus_initialise( void *pContext );

void modbus_destroy( void *pPointer );

void modbus_readInputs( char *pData, void *pContext );

void modbus_writeOutputs( char *pData, void *pContext );


#endif
