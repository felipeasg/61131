#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include "io.h"

#include "piface.h"


#define __UNUSED( __Object )    ( ( void ) ( ( __Object ) == ( __Object ) ) )


static PIFACE_CONTEXT stPi;

static const char * pDevice[2][2] =
{
    { "/dev/spidev0.0", "/dev/spidev0.1"    },
    { "/dev/spidev1.0", "/dev/spidev1.1"    },
};


/*
    This source file implements an interface to a PiFace Digital I/O board for 
    directly represented variables within the IEC 61131-3 virtual machine 
    run-time.  The code in this source file is closely modelled on the 
    mcp23s17 library provided at https://github.com/piface/libmcp23s17.git.
*/

static uint8_t _piface_getControlByte( uint8_t uAddress, uint8_t uCommand );

static int _piface_open( int nBus, int nSelect );

static int _piface_read( int nFd, uint8_t uAddress, uint8_t uReg );

static int _piface_write( int nFd, uint8_t uAddress, uint8_t uReg, uint8_t uVal );


/*!
    \fn static uint8_t _piface_getControlByte( uint8_t uAddress, uint8_t uCommand )
    \brief Return control byte for MCP23S17 16-bit input/output port expander
    \param uAddress Device SPI slace address
    \param uCommand Device SPI read/write command
    \returns Returns MCP23S17 control byte

    The MCP23S17 16-bit input/output port expander is a slave SPI device.  The 
    control byte transmitted to this device incorporates four fixed bits 
    (0b0100), three user-defined hardware address bits (if enabled via 
    IOCON.HAEN; pins A2, A1 and A0) and a single bit indicating whether the 
    command to follow represents a read or write operation.
*/

static uint8_t 
_piface_getControlByte( uint8_t uAddress, uint8_t uCommand )
{
    uAddress <<= 1;
    uAddress &= 0xe;
    uCommand &= 1;
    return ( 0x40 | uAddress | uCommand );
}


static int
_piface_open( int nBus, int nSelect )
{
    int nFd, nValue;


    if( ( nFd = open( pDevice[ nBus ][ nSelect ], O_RDWR ) ) < 0 )
    {
        return -errno;
    }

    nValue = PIFACE_SPI_MODE;
    if( ioctl( nFd, SPI_IOC_WR_MODE, &nValue ) < 0 )
    {
        return -errno;
    }

    nValue = PIFACE_SPI_BITS;
    if( ioctl( nFd, SPI_IOC_WR_BITS_PER_WORD, &nValue ) < 0 )
    {
        return -errno;
    }

    nValue = PIFACE_SPI_SPEED;
    if( ioctl( nFd, SPI_IOC_WR_MAX_SPEED_HZ, &nValue ) < 0 )
    {
        return -errno;
    }

    return nFd;
}


static int
_piface_read( int nFd, uint8_t uAddress, uint8_t uReg )
{
    struct spi_ioc_transfer stTransfer;
    uint8_t uBuffer[3], uCtrl;


    uCtrl = _piface_getControlByte( uAddress, PIFACE_CMD_READ );
    uBuffer[0] = uCtrl;
    uBuffer[1] = uReg;
    uBuffer[2] = 0;

    ( void ) memset( &stTransfer, 0, sizeof( stTransfer ) );
    stTransfer.tx_buf = ( unsigned long ) uBuffer;
    stTransfer.rx_buf = ( unsigned long ) uBuffer;
    stTransfer.len = sizeof( uBuffer );
    stTransfer.delay_usecs = PIFACE_SPI_DELAY;
    stTransfer.speed_hz = PIFACE_SPI_SPEED;
    stTransfer.bits_per_word = PIFACE_SPI_BITS;

    if( ioctl( nFd, SPI_IOC_MESSAGE(1), &stTransfer ) < 0 )
    {
        return -errno;
    }

    return ( uBuffer[2] & 0xff );
}


static int
_piface_write( int nFd, uint8_t uAddress, uint8_t uReg, uint8_t uVal )
{
    struct spi_ioc_transfer stTransfer;
    uint8_t uBuffer[3], uCtrl;


    uCtrl = _piface_getControlByte( uAddress, PIFACE_CMD_WRITE );
    uBuffer[0] = uCtrl;
    uBuffer[1] = uReg;
    uBuffer[2] = uVal;

    ( void ) memset( &stTransfer, 0, sizeof( stTransfer ) );
    stTransfer.tx_buf = ( unsigned long ) uBuffer;
    stTransfer.rx_buf = ( unsigned long ) uBuffer;
    stTransfer.len = sizeof( uBuffer );
    stTransfer.delay_usecs = PIFACE_SPI_DELAY;
    stTransfer.speed_hz = PIFACE_SPI_SPEED;
    stTransfer.bits_per_word = PIFACE_SPI_BITS;

    if( ioctl( nFd, SPI_IOC_MESSAGE(1), &stTransfer ) < 0 )
    {
        return -errno;
    }

    return 0;
}


/*!
    \fn void * piface_initialise( void )
    \brief Register driver for PiFace Digital I/O operations
    \return Returns pointer to contextual data structure on success, NULL on error
*/

void * 
piface_initialise( void *pContext )
{
    IO_DRIVER *pDriver;
    uint8_t uValue;


    ( void ) memset( &stPi, 0, sizeof( stPi ) );
    pDriver = ( IO_DRIVER * ) &stPi;
    /* pDriver->Initialise = piface_initialise; */
    pDriver->Destroy = piface_destroy;
    pDriver->Read = piface_readInputs;
    pDriver->Write = piface_writeOutputs;

    if( ( stPi.Handle = _piface_open( PIFACE_SPI_BUS, PIFACE_SPI_CHIP_SELECT ) ) < 0 )
    {
        return NULL;
    }

    uValue = PIFACE_IO_BANK_OFF |
            PIFACE_IO_INT_MIRROR_OFF |
            PIFACE_IO_SEQOP_OFF |
            PIFACE_IO_DISSLW_OFF |
            PIFACE_IO_HAEN_ON |
            PIFACE_IO_ODR_OFF |
            PIFACE_IO_INTPOL_LOW;
    _piface_write( stPi.Handle, PIFACE_SPI_BUS, PIFACE_REG_IOCON, uValue );
    _piface_write( stPi.Handle, PIFACE_SPI_BUS, PIFACE_REG_IODIRA, 0x00 );
    _piface_write( stPi.Handle, PIFACE_SPI_BUS, PIFACE_REG_IODIRB, 0xff );
    _piface_write( stPi.Handle, PIFACE_SPI_BUS, PIFACE_REG_GPPUB, 0xff );


    /*
        The following code is intended to register a single byte of digital inputs 
        (8 channels) and a single byte of digital outputs (8 channels) associated 
        with the PiFace Digital hardware.
    */

    if( ( io_registerBlock( pContext, "%IB1", &stPi, pDriver ) != 0 ) ||
            ( io_registerBlock( pContext, "%QB1", &stPi, pDriver ) != 0 ) )
    {
        return NULL;
    }
    return pDriver;
}


void
piface_destroy( void *pPointer )
{
    PIFACE_CONTEXT *pPi;


    pPi = ( PIFACE_CONTEXT * ) pPointer;
    if( pPi != NULL )
    {
        if( pPi->Handle >= 0 )
        {
            close( pPi->Handle );
            pPi->Handle = -1;
        }
    }
}


void
piface_readInputs( char *pData, void *pContext )
{
    PIFACE_CONTEXT *pPi;
    int nResult;


    pPi = ( PIFACE_CONTEXT * ) pContext;
    /* assert( pPi != NULL ); */
    /* assert( pPi == &stPi ); */
    nResult = _piface_read( pPi->Handle, PIFACE_SPI_BUS, PIFACE_REG_INPUT );
    if( nResult >= 0 )
    {
        pData[0] = ( ( nResult ^ 0xff ) & 0xff );
    }
}


void
piface_writeOutputs( char *pData, void *pContext )
{
    PIFACE_CONTEXT *pPi;
    uint8_t uValue;


    pPi = ( PIFACE_CONTEXT * ) pContext;
    /* assert( pPi != NULL ); */
    /* assert( pPi == &stPi ); */
    uValue = ( uint8_t ) ( pData[0] & 0xff );
    _piface_write( pPi->Handle, PIFACE_SPI_BUS, PIFACE_REG_OUTPUT, uValue );
}

