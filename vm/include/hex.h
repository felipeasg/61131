#ifndef __HEX_H
#define __HEX_H


/*!
    \fn int hex_openFile( const char *pFilename, char *pPointer )
    \brief Read and parse an Intel hexadecimal object file
    \param pFilename Filename of Intel hexadecimal object file
    \param pPointer Pointer to memory block for parsed record storage
    \returns Returns zero on success, non-zero on failure

    This function implements a simplified parser for the Intel hexadecimal
    object file format for use in association with the IEC 61131-3 virtual
    machine.  

    The simplified nature of this parser only supports the parsing of Data 
    Records ('00') and End of File Records ('01') as defined within the Intel 
    hexadecimal object file format.  As such this function supports the reading 
    of linear object data, with the default Linear Base Address value of zero 
    only.  All object data read is stored, using the load offset of the Data 
    Record, in the memory passed by pointer reference to this function.

    This implementation for reading Intel hexadecimal object files is based 
    upon that in the libcintelhex library from Martin Helmich 
    (https://github.com/martin-helmich/libcintelhex).
*/

int hex_openFile( const char * Filename, char * Pointer );


#endif
