#ifndef __DRIVERS_H
#define __DRIVERS_H


#include <unistd.h>


/*!
    \typedef DRIVER_INITIALISATION_POINTER
    \brief Function prototype type definition for driver initialisation functions
*/

typedef void * ( *DRIVER_INITIALISATION_POINTER )( void * );

typedef struct _DRIVER_DECLARATION
{
    const char * Name;

    DRIVER_INITIALISATION_POINTER Pointer;
}
DRIVER_DECLARATION;


extern DRIVER_DECLARATION DRIVER_TABLE[];

extern size_t DRIVER_TABLE_COUNT;


#endif
