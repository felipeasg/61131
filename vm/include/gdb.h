#ifndef __GDB_H
#define __GDB_H


#include "vm.h"


#define GDB_BUFFER_MAX          (512)

/*!
    \def GDB_TCP_PORT

    This definition specifies the TCP port number on which a listening socket will 
    be established for incoming debugging sessions from the GNU Debugger (gdb).
*/

#define GDB_TCP_PORT            (61131)

#define GDB_TID                 (0)



typedef struct _GDB
{
    void * Parent;

    int Server;

    int Client;

    char ReceiveBuffer[ GDB_BUFFER_MAX ];

    unsigned int ReceiveLength;

    char TransmitBuffer[ GDB_BUFFER_MAX ];

    unsigned int TransmitLength;
}
GDB;


GDB * gdb_initialise( VM *pVM );

/* void gdb_destroy( void *pPointer ); */


#endif
