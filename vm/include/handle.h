#ifndef __HANDLE_H
#define __HANDLE_H


#include <sys/time.h>


#define HANDLE_ARRAY_MAX    (8)


typedef void ( * HANDLE_CALLBACK )( int nHandle, void *pParameter );


typedef struct _HANDLE_ELEMENT
{
    int Handle;

    HANDLE_CALLBACK ReadCallback;

    void * ReadParameter;

    HANDLE_CALLBACK WriteCallback;

    void * WriteParameter;
}
HANDLE_ELEMENT;


/*!
    \struct HANDLE
    \brief Handle context data structure
*/

typedef struct _HANDLE
{
    HANDLE_ELEMENT Handles[ HANDLE_ARRAY_MAX ];

    unsigned int Read;

    unsigned int Write;
}
HANDLE;


int handle_addReadCallback( HANDLE *pHandle, int nHandle, HANDLE_CALLBACK pCallback, void *pParameter );

int handle_addWriteCallback( HANDLE *pHandle, int nHandle, HANDLE_CALLBACK pCallback, void *pParameter );

int handle_deleteReadCallback( HANDLE *pHandle, int nHandle, void **pParameter );

int handle_deleteWriteCallback( HANDLE *pHandle, int nHandle, void **pParameter );

void handle_initialise( HANDLE *pHandle );

int handle_processHandles( HANDLE *pHandle, struct timeval *pTimeout );


#endif
