#ifndef __IO_H
#define __IO_H


#include <stdint.h>


#define IO_BLOCK_MAX    (64)

#define IO_MEMORY_MAX   (1024)


typedef enum _IO_TYPE
{
    IO_TYPE_NONE        = ( 0 << 0 ),
    IO_TYPE_INPUT       = ( 1 << 0 ),
    IO_TYPE_OUTPUT      = ( 1 << 1 ),
    IO_TYPE_MEMORY      = ( IO_TYPE_INPUT | IO_TYPE_OUTPUT ),
}
IO_TYPE;


/*!
    \struct IO_DRIVER

    This data structure is intended to hold function pointers to code which 
    implements the access to directly represented variables.
*/

typedef struct _IO_DRIVER
{
    /* void * ( *Initialise )( void ); */

    void ( *Destroy )( void * Pointer );

    void ( *Read )( char * Data, void * Context );

    void ( *Write )( char * Data, void * Context );
}
IO_DRIVER;


/*!
    \struct IO_BLOCK

    This data structure is intended to hold information about directly represented 
    variables, their underlying storage and corresponding drivers responsible for 
    reading and writing variable values.  For reasons of flexibility with different 
    hardware platforms that may be represented, this data structure deliberately 
    supports the definition of non-contiguous blocks of directly represented 
    variables.
*/

typedef struct _IO_BLOCK
{
    IO_TYPE Type;

    IO_DRIVER * Driver;

    const void * Context;

    unsigned int Width;

    char * Location;
}
IO_BLOCK;

typedef struct _IO_MEMORY
{
    char Memory[ IO_MEMORY_MAX ];

    unsigned int Length;
}
IO_MEMORY;

typedef struct _IO_CONTEXT
{
    IO_MEMORY Inputs;

    IO_MEMORY Outputs;

    IO_MEMORY Memory;

    IO_BLOCK Block[ IO_BLOCK_MAX ];

    unsigned int Blocks;
}
IO_CONTEXT;


int io_initialise( IO_CONTEXT *pIO );

void io_finish( IO_CONTEXT *pIO );

void io_readInputs( IO_CONTEXT *pIO );

int io_registerBlock( void * pPointer, const char * pDefinition, const void * pContext, IO_DRIVER * pDriver );

void io_writeOutputs( IO_CONTEXT *pIO );


#endif
