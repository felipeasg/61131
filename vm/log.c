#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <limits.h>

#include "log.h"
#include "target.h"


static const char * szLevel[] =
{
    "PANIC",
    "ALERT",
    "CRITICAL",
    "ERROR",
    "WARNING",
    "NOTICE",
    "INFO",
    "DEBUG",
};


static uint32_t uStart = 0;


void
log_initialise( void )
{
    uStart = target_getMillisecondCounter();
}


void
log_printMessage( LOG_LEVEL eLevel, const char *pMessage, ... )
{
    va_list stArg;
    char szMessage[LINE_MAX];
    uint32_t uTime;


    uTime = target_getMillisecondCounter() - uStart;

    va_start( stArg, pMessage );
    ( void ) vsnprintf( szMessage, sizeof( szMessage ), pMessage, stArg );
    va_end( stArg );

    fprintf( stdout, "[%lu.%03lu] %s: %s\n",
            ( unsigned long ) ( uTime / 1000 ),
            ( unsigned long ) ( uTime % 1000 ),
            szLevel[ eLevel ],
            szMessage );
}
