#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>

#include "handle.h"


static HANDLE_ELEMENT *
_handle_getElement( HANDLE *pHandle, int nHandle )
{
    HANDLE_ELEMENT *pElement;
    unsigned int uIndex;


    pElement = NULL;
    uIndex = HANDLE_ARRAY_MAX;

    if( ( pHandle->Read + pHandle->Write ) > 0 )
    {
        for( uIndex = 0; uIndex < HANDLE_ARRAY_MAX; ++uIndex )
        {
            pElement = &pHandle->Handles[ uIndex ];
            if( pElement->Handle == nHandle )
            {
                return pElement;
            }
        }
    }

    for( uIndex = 0; uIndex < HANDLE_ARRAY_MAX; ++uIndex )
    {
        pElement = &pHandle->Handles[ uIndex ];
        if( pElement->Handle < 0 )
        {
            pElement->Handle = nHandle;
            pElement->ReadCallback = NULL;
            pElement->ReadParameter = NULL;
            pElement->WriteCallback = NULL;
            pElement->WriteParameter = NULL;

            return pElement;
        }
    }

    return NULL;
}


int 
handle_addReadCallback( HANDLE *pHandle, int nHandle, HANDLE_CALLBACK pCallback, void *pParameter )
{
    HANDLE_ELEMENT *pElement;


    /* assert( pHandle != NULL ); */
    if( ( pElement = _handle_getElement( pHandle, nHandle ) ) == NULL )
    {
        return -ENOMEM;
    }
    if( pElement->ReadCallback != NULL )
    {
        return -EEXIST;
    }

    pElement->ReadCallback = pCallback;
    pElement->ReadParameter = pParameter;

    ++pHandle->Read;

    return 0;
}


int 
handle_addWriteCallback( HANDLE *pHandle, int nHandle, HANDLE_CALLBACK pCallback, void *pParameter )
{
    HANDLE_ELEMENT *pElement;


    /* assert( pHandle != NULL ); */
    if( ( pElement = _handle_getElement( pHandle, nHandle ) ) == NULL )
    {
        return -ENOMEM;
    }
    if( pElement->WriteCallback != NULL )
    {
        return -EEXIST;
    }

    pElement->WriteCallback = pCallback;
    pElement->WriteParameter = pParameter;

    ++pHandle->Write;

    return 0;
}


int
handle_deleteReadCallback( HANDLE *pHandle, int nHandle, void **pParameter )
{
    HANDLE_ELEMENT *pElement;


    /* assert( pHandle != NULL ); */
    if( ( pElement = _handle_getElement( pHandle, nHandle ) ) == NULL )
    {
        return -ENOMEM;
    }

    if( pElement->ReadCallback != NULL )
    {
        *pParameter = pElement->ReadParameter;

        pElement->ReadCallback = NULL;
        pElement->ReadParameter = NULL;

        /* assert( pHandle->Read > 0 ); */
        --pHandle->Read;
    }
    if( pElement->WriteCallback == NULL )
    {
        pElement->Handle = -1;
    }

    return 0;
}


int 
handle_deleteWriteCallback( HANDLE *pHandle, int nHandle, void **pParameter )
{
    HANDLE_ELEMENT *pElement;


    /* assert( pHandle != NULL ); */
    if( ( pElement = _handle_getElement( pHandle, nHandle ) ) == NULL )
    {
        return -ENOMEM;
    }

    if( pElement->WriteCallback != NULL )
    {
        *pParameter = pElement->WriteParameter;

        pElement->WriteCallback = NULL;
        pElement->WriteParameter = NULL;

        /* assert( pHandle->Write > 0 ); */
        --pHandle->Write;
    }
    if( pElement->ReadCallback == NULL )
    {
        pElement->Handle = -1;
    }

    return 0;
}


void
handle_initialise( HANDLE *pHandle )
{
    HANDLE_ELEMENT *pElement;
    unsigned int uIndex;


    for( uIndex = 0; uIndex < HANDLE_ARRAY_MAX; ++uIndex )
    {
        pElement = &pHandle->Handles[ uIndex ];
        pElement->Handle = -1;
        pElement->ReadCallback = NULL;
        pElement->ReadParameter = NULL;
        pElement->WriteCallback = NULL;
        pElement->WriteParameter = NULL;
    }

    pHandle->Read = 0;
    pHandle->Write = 0;
}


int 
handle_processHandles( HANDLE *pHandle, struct timeval *pTimeout )
{
    HANDLE_ELEMENT *pElement;
    fd_set stRead, stWrite;
    void *pRead, *pWrite;
    unsigned int uIndex;
    int nMaximum, nResult;


    /* assert( pHandle != NULL ); */
    nMaximum = -1;
    FD_ZERO( &stRead );
    FD_ZERO( &stWrite );
    pRead = NULL;
    pWrite = NULL;

    if( ( pHandle->Read + pHandle->Write ) > 0 )
    {
        for( uIndex = 0; uIndex < HANDLE_ARRAY_MAX; ++uIndex )
        {
            pElement = &pHandle->Handles[ uIndex ];
            if( pElement->Handle < 0 )
            {
                continue;
            }

            if( pElement->ReadCallback != NULL )
            {
                FD_SET( pElement->Handle, &stRead );
                nMaximum = ( nMaximum > pElement->Handle ) ? nMaximum : pElement->Handle;
                pRead = &stRead;
            }
            if( pElement->WriteCallback != NULL )
            {
                FD_SET( pElement->Handle, &stWrite );
                nMaximum = ( nMaximum > pElement->Handle ) ? nMaximum : pElement->Handle;
                pWrite = &stWrite;
            }
        }
    }

    if( ( ( nResult = select( nMaximum + 1, pRead, pWrite, NULL, pTimeout ) ) < 0 ) &&
            ( errno != EINTR ) )
    {
        return -errno;
    }
    else if( nResult <= 0 )
    {
        return 0;
    }

    for( uIndex = 0; uIndex < HANDLE_ARRAY_MAX; ++uIndex )
    {
        pElement = &pHandle->Handles[ uIndex ];
        if( pElement->Handle < 0 )
        {
            continue;
        }

        if( /* ( pElement->Handle >= 0 ) && */
                ( FD_ISSET( pElement->Handle, &stRead ) != 0 ) &&
                ( pElement->ReadCallback != NULL ) )
        {
            ( pElement->ReadCallback )( pElement->Handle, pElement->ReadParameter );
        }
        if( ( pElement->Handle >= 0 ) &&
                ( FD_ISSET( pElement->Handle, &stWrite ) != 0 ) &&
                ( pElement->WriteCallback != NULL ) )
        {
            ( pElement->WriteCallback )( pElement->Handle, pElement->WriteParameter );
        }
    }

    return nResult;
}

