#include <string.h>
#include <math.h>
#include <endian.h>
#include <errno.h>

#include "iec61131_bytecode.h"
#include "iec61131_external.h"

#include "base.h"
#include "config.h"
#include "log.h"
#include "op.h"
#include "target.h"


/*!
    \def OP_CR_TYPE_CHECK

    This macro declaration enables type checking of the Current Register for a
    series of instructions.  This functionality is largely for development
    purposes, allowing for the validation of compiler byte code generation and
    virtual machine byte code execution.  This functionality should however be
    disabled in production builds due to the additional per-instruction
    overhead which this type checking incorporates.

    #define OP_CR_TYPE_CHECK( p, ... )
*/

#if 1
#define OP_CR_TYPE_CHECK( p, ... )      if( _op_typeCheck( (p), __VA_ARGS__ ) != BOOL_TRUE ) break
#else
#define OP_CR_TYPE_CHECK( p, ... )
#endif


#define OP_CR_COPY( p, v )              ( memcpy( (v), &( (p)->Register.CR ), sizeof( IEC61131_DATA ) ) )
                                        
#define OP_PARAMETER_CHECK_MAX( p )     if( (p)->Parameters.Count == VM_PARAMETER_MAX ) { (p)->Register.ER = ERROR_PARAMETER_OVERFLOW; break; }

#define OP_PARAMETER_CHECK_ZERO( p )    if( (p)->Parameters.Count == 0 ) { (p)->Register.ER = ERROR_PARAMETER_EMPTY; break; }

#define OP_STACK_CHECK_MAX( p )         if( (p)->Stack.Count == VM_STACK_MAX ) { (p)->Register.ER = ERROR_STACK_OVERFLOW; break; } 

#define OP_STACK_CHECK_ZERO( p )        if( (p)->Stack.Count == 0 ) { (p)->Register.ER = ERROR_STACK_EMPTY; break; }


static unsigned int _op_byteAlignment( VM *pVM, size_t uBytes );

static char * _op_byteAlignInput( VM *pVM, IO_TYPE eIo, char *pInterface, IEC61131_TYPE eType, unsigned int uOffset, char *pBuffer );

static void _op_byteAlignOutput( VM *pVM, IO_TYPE eIo, char *pInterface, IEC61131_TYPE eType, unsigned int uOffset, char *pBuffer );

static size_t _op_byteSize( IEC61131_TYPE eType );

static int _op_callLibraryFunction( VM *pVM, uint32_t uTarget );

static int _op_callLibraryFunctionBlock( VM *pVM, uint32_t uTarget );

static uint8_t _op_typeCheck( VM *pVM, ... );


static unsigned int
_op_byteAlignment( VM *pVM, size_t uBytes )
{
     /* assert( pVM != NULL ); */
     return ( unsigned int ) ( ( ( uBytes / pVM->Align ) * pVM->Align ) +
            ( ( ( uBytes % pVM->Align ) > 0 ) ?
                    pVM->Align :
                    0 ) );
}


static char *
_op_byteAlignInput( VM *pVM, IO_TYPE eIo, char *pInterface, IEC61131_TYPE eType, unsigned int uOffset, char *pBuffer )
{
    IO_BLOCK *pBlock;
    IO_CONTEXT *pIO;
    unsigned int uBit, uByte, uCount, uIndex;


    pIO = &pVM->IO;
    uCount = 0;

    for( uIndex = 0; uIndex < pIO->Blocks; ++uIndex )
    {
        pBlock = &pIO->Block[ uIndex ];
        if( pBlock->Type != eIo )
        {
            continue;
        }
        if( uOffset >= ( uCount + pBlock->Width ) )
        {
            uCount += pBlock->Width;
            continue;
        }

        uByte = ( ( uOffset - uCount ) / 8 );

        switch( eType )
        {
            case TYPE_BOOL:

                uBit = ( ( uOffset - uCount ) % 8 );
                pBuffer[0] = ( ( pInterface[ uByte ] & ( 1 << uBit ) ) > 0 ) ? ~0 : 0;
                return pBuffer;
                /* break; */

            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                /* assert( ( uOffset % 8 ) == 0 ); */
                uByte = ( ( uOffset - uCount ) / 8 );
                pBuffer[0] = pInterface[ uByte ];
                return pBuffer;
                /* break; */

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                /* assert( ( uOffset % 16 ) == 0 ); */
                uByte = ( ( uOffset - uCount ) / 16 );
                ( void ) memcpy( pBuffer, &pInterface[ uByte ], 2 );
                return pBuffer;
                /* break; */

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:
#ifdef CONFIG_SUPPORT_REAL
            case TYPE_REAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case TYPE_TIME:
#endif

                /* assert( ( uOffset % 32 ) == 0 ); */
                uByte = ( ( uOffset - uCount ) / 32 );
                ( void ) memcpy( pBuffer, &pInterface[ uByte ], 4 );
                return pBuffer;
                /* break; */

            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:
#ifdef CONFIG_SUPPORT_LREAL
            case TYPE_LREAL:
#endif

                /* assert( ( uOffset % 64 ) == 0 ); */
                uByte = ( ( uOffset - uCount ) / 64 );
                ( void ) memcpy( pBuffer, &pInterface[ uByte ], 8 );
                return pBuffer;
                /* break; */

            default:

                break;
        }

        break;
    }

    return NULL;
}


static void
_op_byteAlignOutput( VM *pVM, IO_TYPE eIo, char *pInterface, IEC61131_TYPE eType, unsigned int uOffset, char *pBuffer )
{
    IO_BLOCK *pBlock;
    IO_CONTEXT *pIO;
    unsigned int uBit, uByte, uCount, uIndex, uNumber;


    pIO = &pVM->IO;
    uCount = 0;

    for( uIndex = 0; uIndex < pIO->Blocks; ++uIndex )
    {
        pBlock = &pIO->Block[ uIndex ];
        if( pBlock->Type != eIo )
        {
            continue;
        }
        if( uOffset >= ( uCount + pBlock->Width ) )
        {
            uCount += pBlock->Width;
            continue;
        }

        uByte = ( ( uOffset - uCount ) / 8 );
        uNumber = 0;

        switch( eType )
        {
            case TYPE_BOOL:

                uBit = ( ( uOffset - uCount ) % 8 );
                if( pBuffer[0] != 0 )
                {
                    pInterface[ uByte ] |= ( 1 << uBit );
                }
                else
                {
                    pInterface[ uByte ] &= ~( 1 << uBit );
                }
                continue;
                /* break; */

            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                uNumber = 1;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                uNumber = 2;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:
#ifdef CONFIG_SUPPORT_REAL
            case TYPE_REAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case TYPE_TIME:
#endif

                uNumber = 4;
                break;

            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:
#ifdef CONFIG_SUPPORT_LREAL
            case TYPE_LREAL:
#endif

                uNumber = 8;
                break;

            default:

                break;
        }

        if( uNumber > 0 )
        {
            ( void ) memcpy( &pInterface[ uByte ], &pBuffer[0], uNumber );
        }
    }
}


static size_t
_op_byteSize( IEC61131_TYPE eType )
{
    switch( eType )
    {
        case TYPE_BOOL:
        case TYPE_SINT:
        case TYPE_USINT:
        case TYPE_BYTE:

            return sizeof( uint8_t );

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            return sizeof( uint16_t );

        case TYPE_DINT:
        case TYPE_UDINT:
        case TYPE_DWORD:

            return sizeof( uint32_t );

#ifdef CONFIG_SUPPORT_LONGLONG
        case TYPE_LINT:
        case TYPE_ULINT:
        case TYPE_LWORD:

            return sizeof( uint64_t );

#endif
#ifdef CONFIG_SUPPORT_REAL
        case TYPE_REAL:

            return sizeof( float );

#endif
#ifdef CONFIG_SUPPORT_LREAL
        case TYPE_LREAL:

            return sizeof( double );

#endif
#ifdef CONFIG_SUPPORT_DATETIME
        case TYPE_TIME:

            return sizeof( int32_t );

#endif
        case TYPE_FUNCTIONBLOCK:

            return sizeof( uint32_t );

        default:

            return 0;
    }
}


static int
_op_callLibraryFunction( VM *pVM, uint32_t uTarget )
{
    IEC61131_DATA *pValue;
    EXTERNAL_FUNCTION_DECLARATION *pDeclaration;
    unsigned int uIndex;
    int nResult;


    /* assert( ( uTarget & IEC61131_BYTECODE_FUNCTION ) != 0 ); */

    uIndex = ( unsigned int ) ( uTarget & ~( IEC61131_BYTECODE_FUNCTION | IEC61131_BYTECODE_FUNCTION_BLOCK ) );
    if( uIndex >= ( unsigned int ) IEC61131_FUNCTION_COUNT )
    {
        pVM->Register.ER = ERROR_FUNCTION_UNSUPPORTED;
        return -1;
    }
    pDeclaration = ( EXTERNAL_FUNCTION_DECLARATION * ) &IEC61131_FUNCTION[ uIndex ];
    if( pDeclaration->Pointer == NULL )
    {
        pVM->Register.ER = ERROR_FUNCTION_UNSUPPORTED;
        return -1;
    }

    nResult = pDeclaration->Pointer( pVM->Parameters.Value, pVM->Parameters.Count );
    switch( nResult )
    {
        /*
            At this stage of development, it is assumed that functions may only return 
            a single value - This assumption is likely to change as the scope of 
            support for library functions is extended to include function block 
            operations.  Until this time however, if a function returns any parameter 
            count other than one, the exception register will be set to 
            ERROR_FUNCTION_FAULT.
        */

        case -EFAULT:
        default:

            pVM->Register.ER = ERROR_FUNCTION_FAULT;
            return -1;
            /* break; */

        case -EINVAL:

            pVM->Register.ER = ERROR_FUNCTION_PARAMETER;
            return -1;
            /* break; */

        case 1:

            pValue = &pVM->Parameters.Value[0];
            ( void ) memcpy( &pVM->Register.CR, pValue, sizeof( IEC61131_DATA ) );
            break;
    }

    pVM->Parameters.Count = 0;

    return 0;
}


static int 
_op_callLibraryFunctionBlock( VM *pVM, uint32_t uTarget )
{
    EXTERNAL_FUNCTION_BLOCK_DECLARATION *pDeclaration;
    IEC61131_DATA *pValue;
    unsigned int uIndex;
    int nResult;


    uIndex = ( unsigned int ) ( uTarget & ~( IEC61131_BYTECODE_FUNCTION | IEC61131_BYTECODE_FUNCTION_BLOCK ) );
    if( uIndex >= ( unsigned int ) IEC61131_FUNCTION_BLOCK_COUNT )
    {
        pVM->Register.ER = ERROR_FUNCTION_UNSUPPORTED;
        return -1;
    }
    pDeclaration = ( EXTERNAL_FUNCTION_BLOCK_DECLARATION * ) &IEC61131_FUNCTION_BLOCK[ uIndex ];
    if( ( pDeclaration->Size == 0 ) ||
            ( pDeclaration->Parameter == NULL ) ||
            ( pDeclaration->Pointer == NULL ) )
    {
        pVM->Register.ER = ERROR_FUNCTION_UNSUPPORTED;
        return -1;
    }

    /* assert( pVM->Parameters.Count == 1 ); */
    if( pVM->Parameters.Count != 1 )
    {
        pVM->Register.ER = ( pVM->Parameters.Count > 0 ) ? ERROR_PARAMETER_OVERFLOW : ERROR_PARAMETER_EMPTY;
        return -1;
    }

    pValue = &pVM->Parameters.Value[0];
    nResult = pDeclaration->Pointer( pVM->Symbol + pValue->Value.B32 );
    switch( nResult )
    {
        case 0:

            break;

        case -EINVAL:

            pVM->Register.ER = ERROR_FUNCTION_PARAMETER;
            return -1;
            /* break; */

        case -EFAULT:
        default:

            pVM->Register.ER = ERROR_FUNCTION_FAULT;
            return -1;
            /* break; */
    }

    pVM->Parameters.Count = 0;

    return 0;
}


static uint8_t
_op_typeCheck( VM *pVM, ... )
{
    va_list stAp;
    unsigned int uType;
    uint8_t uResult;


    va_start( stAp, pVM );
    for( uResult = BOOL_FALSE;; )
    {
        uType = va_arg( stAp, unsigned int );
        if( uType == TYPE_NONE )
        {
            pVM->Register.ER = ERROR_INVALID_OPERAND;
            break;
        }
        if( uType == OP_CR_TYPE( pVM ) )
        {
            uResult = BOOL_TRUE;
            break;
        }
    }
    va_end( stAp );

    return uResult;
}


int
op_executeByteCode( VM *pVM )
{
    IEC61131_DATA stValue;
    IEC61131_FORMAT *pFormat;
    size_t uSize;
    uint32_t uOffset, uPC;
#ifdef CONFIG_SUPPORT_LONGLONG
    uint64_t uVal64;
#endif
    uint32_t uVal32;
    uint16_t uVal16;
    uint8_t uVal8;
#ifdef CONFIG_SUPPORT_REAL
    float fVal;
#endif
#ifdef CONFIG_SUPPORT_LREAL
    double dVal;
#endif
    uint8_t bCondition, bResult;
    char *pByteCode, *pPointer;
    char sBuffer[8];


    /*
        Continue execution of IEC 61131-3 virtual machine byte code while no
        exceptions have been raised and the program counter has not reached the
        end of the loaded application.
    */

    pFormat = ( IEC61131_FORMAT * ) pVM->App;

    if( ( pVM->State == VM_STATE_DEBUG ) &&
            ( pVM->Step == 0 ) )
    {
        return pVM->Register.ER;
    }
    pVM->Step = 0;

    while( ( pVM->Register.ER == 0 ) &&
            ( pVM->Register.PC < pVM->Length ) )
    {
        uPC = pVM->Register.PC;

        pByteCode = pVM->App + pVM->Register.PC;
        pVM->Register.PC += _op_byteAlignment( pVM, sizeof( IEC61131_BYTECODE ) );


        /*
            By default, the pointer to the instruction operand points to the next word 
            that follows the instruction operand.  This pointer may be changed to a 
            byte-aligned location in the symbol table or a location in the memory 
            associated for the I/O interface.  For the latter however, explicit action 
            needs to be taken to ensure that this access is byte aligned and as such 
            may involve copy-out and copy-in operations.
        */

        pPointer = pVM->App + pVM->Register.PC;
        pVM->Register.BC = be32toh( target_getB32( pByteCode ) );

        uOffset = OP_OPERAND_B32( pPointer );

        if( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) == OPERAND_SYMBOL )
        {
            pPointer = pVM->Symbol + 
                    pVM->Register.OF +
                    OP_OPERAND_B32( pPointer );
        }
        else if( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 )
        {
            switch( G_OPERAND( pVM->Register.BC ) & OPERAND_DIRECT_MASK )
            {
                case OPERAND_DIRECT_INPUT:

                    pPointer = _op_byteAlignInput( 
                            pVM, 
                            IO_TYPE_INPUT,
                            pVM->IO.Inputs.Memory, 
                            ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK ),
                            uOffset, 
                            sBuffer );
                    break;

                case OPERAND_DIRECT_MEMORY:

                    pPointer = _op_byteAlignInput( 
                            pVM, 
                            IO_TYPE_MEMORY,
                            pVM->IO.Memory.Memory, 
                            ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK ),
                            uOffset, 
                            sBuffer );
                    break;

                case OPERAND_DIRECT_OUTPUT:

                    pPointer = _op_byteAlignInput( 
                            pVM, 
                            IO_TYPE_OUTPUT,
                            pVM->IO.Outputs.Memory, 
                            ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK ),
                            uOffset, 
                            sBuffer );
                    break;

                case OPERAND_NONE:
                default:

                    break;
            }
        }
        else { }

        if( pVM->Debug != 0 )
        {
            LOG_DEBUG( "ST %u, PR %u, CR %08lx, PC %u, BC %04x (Operator %u, Operand %u)",
                    pVM->Stack.Count,
                    pVM->Parameters.Count,
                    OP_CR_VAL_B32( pVM ),
                    uPC - pFormat->ByteCode,
                    pVM->Register.BC,
                    G_OP( pVM->Register.BC ),
                    G_OPERAND( pVM->Register.BC ) );
        }

        if( pPointer == NULL )
        {
            pVM->Register.ER = ERROR_INVALID_IO;
            continue;
        }

        switch( ( pVM->Register.BC & ~OPERAND_MASK ) )
        {
            case OP_NOP_NONE:

                /*
                    While this no-operation opcode (NOP) is not defined within the IEC 61131-3
                    standard, this operation code has been included for development purposes.
                    Where encountered, no explicit operations are performed short of advancing
                    the program counter register to the next opcode.
                */

                break;

            case OP_PUSH_BOOL:
            case OP_PUSH_SINT:
            case OP_PUSH_INT:
            case OP_PUSH_DINT:
            case OP_PUSH_USINT:
            case OP_PUSH_UINT:
            case OP_PUSH_UDINT:
            case OP_PUSH_BYTE:
            case OP_PUSH_WORD:
            case OP_PUSH_DWORD:
            case OP_PUSH_C_BOOL:
            case OP_PUSH_C_SINT:
            case OP_PUSH_C_INT:
            case OP_PUSH_C_DINT:
            case OP_PUSH_C_USINT:
            case OP_PUSH_C_UINT:
            case OP_PUSH_C_UDINT:
            case OP_PUSH_C_BYTE:
            case OP_PUSH_C_WORD:
            case OP_PUSH_C_DWORD:
            case OP_PUSH_CN_BOOL:
            case OP_PUSH_CN_SINT:
            case OP_PUSH_CN_INT:
            case OP_PUSH_CN_DINT:
            case OP_PUSH_CN_USINT:
            case OP_PUSH_CN_UINT:
            case OP_PUSH_CN_UDINT:
            case OP_PUSH_CN_BYTE:
            case OP_PUSH_CN_WORD:
            case OP_PUSH_CN_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_PUSH_LINT:
            case OP_PUSH_ULINT:
            case OP_PUSH_LWORD:
            case OP_PUSH_C_LINT:
            case OP_PUSH_C_ULINT:
            case OP_PUSH_C_LWORD:
            case OP_PUSH_CN_LINT:
            case OP_PUSH_CN_ULINT:
            case OP_PUSH_CN_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_PUSH_REAL:
            case OP_PUSH_C_REAL:
            case OP_PUSH_CN_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_PUSH_LREAL:
            case OP_PUSH_C_LREAL:
            case OP_PUSH_CN_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_PUSH_TIME:
            case OP_PUSH_C_TIME:
            case OP_PUSH_CN_TIME:
#endif
            case OP_PUSH_FUNCTIONBLOCK:
            case OP_PUSH_C_FUNCTIONBLOCK:
            case OP_PUSH_CN_FUNCTIONBLOCK:

                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );
                }
                OP_PARAMETER_CHECK_MAX( pVM );

                uSize = _op_byteSize( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );

                bCondition = BOOL_TRUE;
                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    bResult = ( uint8_t ) ( ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? BOOL_FALSE : BOOL_TRUE );
                    bCondition = ( uint8_t ) ( OP_CR_VAL_B8( pVM ) == bResult );
                }

                if( bCondition == BOOL_TRUE )
                {
                    ( void ) memset( &stValue, 0, sizeof( stValue ) );
                    stValue.Type = ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );

                    switch( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK )
                    {
                        case OPERAND_BOOL:
                        case OPERAND_SINT:
                        case OPERAND_USINT:
                        case OPERAND_BYTE:

                            stValue.Value.B8 = OP_OPERAND_B8( pPointer );
                            /* stValue.Length = sizeof( uint8_t ); */
                            break;

                        case OPERAND_INT:
                        case OPERAND_UINT:
                        case OPERAND_WORD:

                            stValue.Value.B16 = OP_OPERAND_B16( pPointer );
                            /* stValue.Length = sizeof( uint16_t ); */
                            break;

                        case OPERAND_DINT:
                        case OPERAND_UDINT:
                        case OPERAND_DWORD:

                            stValue.Value.B32 = OP_OPERAND_B32( pPointer );
                            /* stValue.Length = sizeof( uint32_t ); */
                            break;

#ifdef CONFIG_SUPPORT_LONGLONG
                        case OPERAND_LINT:
                        case OPERAND_ULINT:
                        case OPERAND_LWORD:

                            stValue.Value.B64 = OP_OPERAND_B64( pPointer );
                            /* stValue.Length = sizeof( uint64_t ); */
                            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                        case OPERAND_REAL:

                            stValue.Value.Float = OP_OPERAND_FLOAT( pPointer );
                            /* stValue.Length = sizeof( float ); */
                            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                        case OPERAND_LREAL:

                            stValue.Value.Double = OP_OPERAND_DOUBLE( pPointer );
                            /* stValue.Length = sizeof( double ); */
                            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                        case OPERAND_TIME:

                            stValue.Value.Time = OP_OPERAND_B32( pPointer );
                            /* stValue.Length = sizeof( int32_t ); */
                            break;

#endif
                        case OPERAND_FUNCTIONBLOCK: 

                            stValue.Value.B32 = OP_OPERAND_B32( pPointer );
                            /* stValue.Length = sizeof( uint32_t ); */
                            break;

                        default:

                            pVM->Register.ER = ERROR_INVALID_OPERAND;
                            break;
                    }

                    ( void ) memcpy( &pVM->Parameters.Value[ pVM->Parameters.Count++ ], &stValue, sizeof( IEC61131_DATA ) );
                }

                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                uSize );
                break;


            case OP_POP_BOOL:
            case OP_POP_SINT:
            case OP_POP_INT:
            case OP_POP_DINT:
            case OP_POP_USINT:
            case OP_POP_UINT:
            case OP_POP_UDINT:
            case OP_POP_BYTE:
            case OP_POP_WORD:
            case OP_POP_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_POP_LINT:
            case OP_POP_ULINT:
            case OP_POP_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_POP_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_POP_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_POP_TIME:
#endif
            case OP_POP_FUNCTIONBLOCK:

                /* assert( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) > 0 ); */
                OP_PARAMETER_CHECK_ZERO( pVM );

                ( void ) memcpy( &stValue, &pVM->Parameters.Value[ --pVM->Parameters.Count ], sizeof( IEC61131_DATA ) );

                if( ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK ) != stValue.Type )
                {
                    pVM->Register.ER = ERROR_INVALID_OPERAND;
                    break;
                }

                switch( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK )
                {
                    case OPERAND_BOOL:
                    case OPERAND_SINT:
                    case OPERAND_USINT:
                    case OPERAND_BYTE:

                        OP_OPERAND_B8( pPointer ) = stValue.Value.B8;
                        break;

                    case OPERAND_INT:
                    case OPERAND_UINT:
                    case OPERAND_WORD:

                        OP_OPERAND_B16( pPointer ) = stValue.Value.B16;
                        break;

                    case OPERAND_DINT:
                    case OPERAND_UDINT:
                    case OPERAND_DWORD:

                        OP_OPERAND_B32( pPointer ) = stValue.Value.B32;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OPERAND_LINT:
                    case OPERAND_ULINT:
                    case OPERAND_LWORD:

                        OP_OPERAND_B64( pPointer ) = stValue.Value.B64;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OPERAND_REAL:

                        OP_OPERAND_FLOAT( pPointer ) = stValue.Value.Float;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OPERAND_LREAL:

                        OP_OPERAND_DOUBLE( pPointer ) = stValue.Value.Double;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OPERAND_TIME:

                        OP_OPERAND_B32( pPointer ) = stValue.Value.B32;
                        break;

#endif
                    case OPERAND_FUNCTIONBLOCK:

                        /*
                            The following sets the symbol table offset register (OF) for the current 
                            scope of IEC 61131-3 virtual machine operations.  It should be noted that 
                            this value is _not_ explicitly cleared, but it implicitly cleared through 
                            the shifting of the register frame by call (CAL) or return (RET) operator.
                        */

                        pVM->Register.OF = stValue.Value.B32;
                        break;

                    default:

                        pVM->Register.ER = ERROR_INVALID_OPERAND;
                        break;
                }

                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

            case OP_LD_BOOL:
            case OP_LD_SINT:
            case OP_LD_USINT:
            case OP_LD_BYTE:
            case OP_LD_N_BOOL:
            case OP_LD_N_SINT:
            case OP_LD_N_USINT:
            case OP_LD_N_BYTE:

                /*
                    Note that operand byte size of this operator differs depending upon whether
                    the operand is a symbol location (presently stored/assumed to be 32-bits,
                    uint32_t) or a literal 8-bit value (uint8_t).  This pattern is repeated
                    with a number of virtual machine operators.
                */

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                OP_CR_TYPE( pVM ) = ( IEC61131_TYPE ) ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_LD_INT:
            case OP_LD_UINT:
            case OP_LD_WORD:
            case OP_LD_N_INT:
            case OP_LD_N_UINT:
            case OP_LD_N_WORD:

                uVal16 = OP_OPERAND_B16( pPointer );
                OP_CR_VAL_B16( pVM ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal16 : uVal16;
                OP_CR_TYPE( pVM ) = ( IEC61131_TYPE ) ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_LD_DINT:
            case OP_LD_UDINT:
            case OP_LD_DWORD:
            case OP_LD_N_DINT:
            case OP_LD_N_UDINT:
            case OP_LD_N_DWORD:

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B32( pVM ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal32 : uVal32;
                OP_CR_TYPE( pVM ) = ( IEC61131_TYPE ) ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_LD_LINT:
            case OP_LD_ULINT:
            case OP_LD_LWORD:
            case OP_LD_N_LINT:
            case OP_LD_N_ULINT:
            case OP_LD_N_LWORD:

                uVal64 = OP_OPERAND_B64( pPointer );
                OP_CR_VAL_B64( pVM ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal64 : uVal64;
                OP_CR_TYPE( pVM ) = ( IEC61131_TYPE ) ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_LD_REAL:

                OP_CR_VAL_FLOAT( pVM ) = OP_OPERAND_FLOAT( pPointer );
                OP_CR_TYPE( pVM ) = TYPE_REAL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_LD_LREAL:

                OP_CR_VAL_DOUBLE( pVM ) = OP_OPERAND_DOUBLE( pPointer );
                OP_CR_TYPE( pVM ) = TYPE_LREAL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_LD_TIME:

                OP_CR_VAL_B32( pVM ) = OP_OPERAND_B32( pPointer );
                OP_CR_TYPE( pVM ) = TYPE_TIME;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_ST_BOOL:
            case OP_ST_SINT:
            case OP_ST_USINT:
            case OP_ST_BYTE:
            case OP_ST_N_BOOL:
            case OP_ST_N_SINT:
            case OP_ST_N_USINT:
            case OP_ST_N_BYTE:

                /*
                    Note that as literal values are not accepted as operands for the ST
                    operator, there is no requirement for the implementation of differential
                    operand byte size management with this operator (as there is in association
                    with the corresponding LD operator).
                */

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_CR_VAL_B8( pVM );
                OP_OPERAND_B8( pPointer ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

            case OP_ST_INT:
            case OP_ST_UINT:
            case OP_ST_WORD:
            case OP_ST_N_INT:
            case OP_ST_N_UINT:
            case OP_ST_N_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_CR_VAL_B16( pVM );
                OP_OPERAND_B16( pPointer ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal16 : uVal16;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

            case OP_ST_DINT:
            case OP_ST_UDINT:
            case OP_ST_DWORD:
            case OP_ST_N_DINT:
            case OP_ST_N_UDINT:
            case OP_ST_N_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_CR_VAL_B32( pVM );
                OP_OPERAND_B32( pPointer ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal32 : uVal32;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_ST_LINT:
            case OP_ST_ULINT:
            case OP_ST_LWORD:
            case OP_ST_N_LINT:
            case OP_ST_N_ULINT:
            case OP_ST_N_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_CR_VAL_B64( pVM );
                OP_OPERAND_B64( pPointer ) = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal64 : uVal64;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_ST_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                OP_OPERAND_FLOAT( pPointer ) = OP_CR_VAL_FLOAT( pVM );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_ST_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                OP_OPERAND_DOUBLE( pPointer ) = OP_CR_VAL_DOUBLE( pVM );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_ST_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                OP_OPERAND_B32( pPointer ) = OP_CR_VAL_B32( pVM );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_S_BOOL:
            case OP_R_BOOL:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );

                OP_OPERAND_B8( pPointer ) = ( uint8_t ) ( ( ( pVM->Register.BC & ~OPERAND_MASK ) == OP_S_BOOL ) ? ~0 : 0 );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

            case OP_AND_BOOL:
            case OP_AND_N_BOOL:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) &= ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_OR_BOOL:
            case OP_OR_N_BOOL:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) |= ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_XOR_BOOL:
            case OP_XOR_N_BOOL:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) ^= ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_NOT_SINT:
            case OP_NOT_USINT:
            case OP_NOT_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                OP_CR_VAL_B8( pVM ) = ~( OP_OPERAND_B8( pPointer ) );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_NOT_INT:
            case OP_NOT_UINT:
            case OP_NOT_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                OP_CR_VAL_B16( pVM ) = ~( OP_OPERAND_B16( pPointer ) );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_NOT_DINT:
            case OP_NOT_UDINT:
            case OP_NOT_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                OP_CR_VAL_B32( pVM ) = ~( OP_OPERAND_B32( pPointer ) );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_NOT_LINT:
            case OP_NOT_ULINT:
            case OP_NOT_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                OP_CR_VAL_B64( pVM ) = ~( OP_OPERAND_B64( pPointer ) );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
            case OP_ADD_SINT:
            case OP_ADD_USINT:
            case OP_ADD_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                OP_CR_VAL_B8( pVM ) += OP_OPERAND_B8( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_ADD_INT:
            case OP_ADD_UINT:
            case OP_ADD_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                OP_CR_VAL_B16( pVM ) += OP_OPERAND_B16( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_ADD_DINT:
            case OP_ADD_UDINT:
            case OP_ADD_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                OP_CR_VAL_B32( pVM ) += OP_OPERAND_B32( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_ADD_LINT:
            case OP_ADD_ULINT:
            case OP_ADD_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                OP_CR_VAL_B64( pVM ) += OP_OPERAND_B64( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_ADD_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                OP_CR_VAL_FLOAT( pVM ) += OP_OPERAND_FLOAT( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_ADD_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                OP_CR_VAL_DOUBLE( pVM ) += OP_OPERAND_DOUBLE( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_ADD_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                OP_CR_VAL_B32( pVM ) += OP_OPERAND_B32( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_SUB_SINT:
            case OP_SUB_USINT:
            case OP_SUB_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                OP_CR_VAL_B8( pVM ) -= OP_OPERAND_B8( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_SUB_INT:
            case OP_SUB_UINT:
            case OP_SUB_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                OP_CR_VAL_B16( pVM ) -= OP_OPERAND_B16( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_SUB_DINT:
            case OP_SUB_UDINT:
            case OP_SUB_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                OP_CR_VAL_B32( pVM ) -= OP_OPERAND_B32( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_SUB_LINT:
            case OP_SUB_ULINT:
            case OP_SUB_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                OP_CR_VAL_B64( pVM ) -= OP_OPERAND_B64( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_SUB_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                OP_CR_VAL_FLOAT( pVM ) -= OP_OPERAND_FLOAT( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_SUB_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                OP_CR_VAL_DOUBLE( pVM ) -= OP_OPERAND_DOUBLE( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_SUB_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                OP_CR_VAL_B32( pVM ) -= OP_OPERAND_B32( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_MUL_SINT:
            case OP_MUL_USINT:
            case OP_MUL_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                OP_CR_VAL_B8( pVM ) *= OP_OPERAND_B8( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_MUL_INT:
            case OP_MUL_UINT:
            case OP_MUL_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                OP_CR_VAL_B16( pVM ) *= OP_OPERAND_B16( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_MUL_DINT:
            case OP_MUL_UDINT:
            case OP_MUL_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                OP_CR_VAL_B32( pVM ) *= OP_OPERAND_B32( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_MUL_LINT:
            case OP_MUL_ULINT:
            case OP_MUL_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                OP_CR_VAL_B64( pVM ) *= OP_OPERAND_B64( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_MUL_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                OP_CR_VAL_FLOAT( pVM ) *= OP_OPERAND_FLOAT( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_MUL_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                OP_CR_VAL_DOUBLE( pVM ) *= OP_OPERAND_DOUBLE( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
            case OP_DIV_SINT:
            case OP_DIV_USINT:
            case OP_DIV_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                if( uVal8 == 0u )
                {
                    pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                    break;
                }

                OP_CR_VAL_B8( pVM ) /= uVal8;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_DIV_INT:
            case OP_DIV_UINT:
            case OP_DIV_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_OPERAND_B16( pPointer );
                if( uVal16 == 0u )
                {
                    pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                    break;
                }

                OP_CR_VAL_B16( pVM ) /= uVal16;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_DIV_DINT:
            case OP_DIV_UDINT:
            case OP_DIV_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                if( uVal32 == 0ul )
                {
                    pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                    break;
                }

                OP_CR_VAL_B32( pVM ) /= uVal32;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_DIV_LINT:
            case OP_DIV_ULINT:
            case OP_DIV_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_OPERAND_B64( pPointer );
                if( uVal64 == 0ull )
                {
                    pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                    break;
                }

                OP_CR_VAL_B64( pVM ) /= uVal64;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_DIV_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                fVal = OP_OPERAND_FLOAT( pPointer );
                if( fVal == 0.0f )
                {
                    pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                    break;
                }

                OP_CR_VAL_FLOAT( pVM ) /= fVal;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_DIV_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                dVal = OP_OPERAND_DOUBLE( pPointer );
                if( dVal == 0.0 )
                {
                    pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                    break;
                }

                OP_CR_VAL_DOUBLE( pVM ) /= dVal;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
            case OP_MOD_SINT:
            case OP_MOD_USINT:
            case OP_MOD_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                OP_CR_VAL_B8( pVM ) %= OP_OPERAND_B8( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_MOD_INT:
            case OP_MOD_UINT:
            case OP_MOD_WORD:
            
                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                OP_CR_VAL_B16( pVM ) %= OP_OPERAND_B16( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_MOD_DINT:
            case OP_MOD_UDINT:
            case OP_MOD_DWORD:
            
                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                OP_CR_VAL_B32( pVM ) %= OP_OPERAND_B32( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_MOD_LINT:
            case OP_MOD_ULINT:
            case OP_MOD_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                OP_CR_VAL_B64( pVM ) %= OP_OPERAND_B64( pPointer );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_MOD_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                OP_CR_VAL_FLOAT( pVM ) = ( float ) fmod( ( double ) OP_CR_VAL_FLOAT( pVM ), ( double ) OP_OPERAND_FLOAT( pPointer ) );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_MOD_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                OP_CR_VAL_DOUBLE( pVM ) = fmod( OP_CR_VAL_DOUBLE( pVM ), OP_OPERAND_DOUBLE( pPointer ) );
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
            case OP_GT_BOOL:
            case OP_GT_SINT:
            case OP_GT_USINT:
            case OP_GT_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B8( pVM ) > uVal8 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_GT_INT:
            case OP_GT_UINT:
            case OP_GT_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_OPERAND_B16( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B16( pVM ) > uVal16 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_GT_DINT:
            case OP_GT_UDINT:
            case OP_GT_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) > uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_GT_LINT:
            case OP_GT_ULINT:
            case OP_GT_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_OPERAND_B64( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B64( pVM ) > uVal64 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_GT_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                fVal = OP_OPERAND_FLOAT( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_FLOAT( pVM ) > fVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_GT_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                dVal = OP_OPERAND_DOUBLE( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_DOUBLE( pVM ) > dVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_GT_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) > uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_GE_BOOL:
            case OP_GE_SINT:
            case OP_GE_USINT:
            case OP_GE_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B8( pVM ) >= uVal8 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_GE_INT:
            case OP_GE_UINT:
            case OP_GE_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_OPERAND_B16( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B16( pVM ) >= uVal16 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_GE_DINT:
            case OP_GE_UDINT:
            case OP_GE_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) >= uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_GE_LINT:
            case OP_GE_ULINT:
            case OP_GE_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_OPERAND_B64( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B64( pVM ) >= uVal64 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_GE_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                fVal = OP_OPERAND_FLOAT( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_FLOAT( pVM ) >= fVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_GE_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                dVal = OP_OPERAND_DOUBLE( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_DOUBLE( pVM ) >= dVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_GE_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) >= uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_EQ_BOOL:
            case OP_EQ_SINT:
            case OP_EQ_USINT:
            case OP_EQ_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B8( pVM ) == uVal8 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_EQ_INT:
            case OP_EQ_UINT:
            case OP_EQ_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_OPERAND_B16( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B16( pVM ) == uVal16 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_EQ_DINT:
            case OP_EQ_UDINT:
            case OP_EQ_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) == uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_EQ_LINT:
            case OP_EQ_ULINT:
            case OP_EQ_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_OPERAND_B64( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B64( pVM ) == uVal64 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_EQ_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                fVal = OP_OPERAND_FLOAT( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_FLOAT( pVM ) == fVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_EQ_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                dVal = OP_OPERAND_DOUBLE( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_DOUBLE( pVM ) == dVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_EQ_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) == uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_NE_BOOL:
            case OP_NE_SINT:
            case OP_NE_USINT:
            case OP_NE_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B8( pVM ) != uVal8 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_NE_INT:
            case OP_NE_UINT:
            case OP_NE_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_OPERAND_B16( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B16( pVM ) != uVal16 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_NE_DINT:
            case OP_NE_UDINT:
            case OP_NE_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) != uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_NE_LINT:
            case OP_NE_ULINT:
            case OP_NE_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_OPERAND_B64( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B64( pVM ) != uVal64 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_NE_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                fVal = OP_OPERAND_FLOAT( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_FLOAT( pVM ) != fVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_NE_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                dVal = OP_OPERAND_DOUBLE( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_DOUBLE( pVM ) != dVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_NE_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) != uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_LE_BOOL:
            case OP_LE_SINT:
            case OP_LE_USINT:
            case OP_LE_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B8( pVM ) <= uVal8 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_LE_INT:
            case OP_LE_UINT:
            case OP_LE_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_OPERAND_B16( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B16( pVM ) <= uVal16 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_LE_DINT:
            case OP_LE_UDINT:
            case OP_LE_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) <= uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_LE_LINT:
            case OP_LE_ULINT:
            case OP_LE_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_OPERAND_B64( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B64( pVM ) <= uVal64 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_LE_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                fVal = OP_OPERAND_FLOAT( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_FLOAT( pVM ) <= fVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_LE_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                dVal = OP_OPERAND_DOUBLE( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_DOUBLE( pVM ) <= dVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_LE_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) <= uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_LT_BOOL:
            case OP_LT_SINT:
            case OP_LT_USINT:
            case OP_LT_BYTE:

                OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, TYPE_SINT, TYPE_USINT, TYPE_BYTE, 0 );

                uVal8 = OP_OPERAND_B8( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B8( pVM ) < uVal8 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint8_t ) );
                break;

            case OP_LT_INT:
            case OP_LT_UINT:
            case OP_LT_WORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_INT, TYPE_UINT, TYPE_WORD, 0 );

                uVal16 = OP_OPERAND_B16( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B16( pVM ) < uVal16 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint16_t ) );
                break;

            case OP_LT_DINT:
            case OP_LT_UDINT:
            case OP_LT_DWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_DINT, TYPE_UDINT, TYPE_DWORD, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) < uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_LT_LINT:
            case OP_LT_ULINT:
            case OP_LT_LWORD:

                OP_CR_TYPE_CHECK( pVM, TYPE_LINT, TYPE_ULINT, TYPE_LWORD, 0 );

                uVal64 = OP_OPERAND_B64( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B64( pVM ) < uVal64 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( uint64_t ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_LT_REAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_REAL, 0 );

                fVal = OP_OPERAND_FLOAT( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_FLOAT( pVM ) < fVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( float ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_LT_LREAL:

                OP_CR_TYPE_CHECK( pVM, TYPE_LREAL, 0 );

                dVal = OP_OPERAND_DOUBLE( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_DOUBLE( pVM ) < dVal ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                sizeof( double ) );
                break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_LT_TIME:

                OP_CR_TYPE_CHECK( pVM, TYPE_TIME, 0 );

                uVal32 = OP_OPERAND_B32( pPointer );
                OP_CR_VAL_B8( pVM ) = ( uint8_t ) ( ( OP_CR_VAL_B32( pVM ) < uVal32 ) ? BOOL_TRUE : BOOL_FALSE );
                OP_CR_TYPE( pVM ) = TYPE_BOOL;
                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );
                break;

#endif
            case OP_JMP_NONE:
            case OP_JMP_C_NONE:
            case OP_JMP_CN_NONE:

                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );
                }

                bCondition = BOOL_TRUE;
                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    bResult = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? BOOL_FALSE : BOOL_TRUE;
                    bCondition = ( uint8_t ) ( OP_CR_VAL_B8( pVM ) == bResult );
                }

                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );

                if( bCondition == BOOL_TRUE )
                {
                    uVal32 = OP_OPERAND_B32( pPointer );
                    pVM->Register.PC = uVal32;
                }

                break;

            case OP_RET_NONE:
            case OP_RET_C_NONE:
            case OP_RET_CN_NONE:
            case OP_RET_BOOL:
            case OP_RET_C_BOOL:
            case OP_RET_CN_BOOL:
            case OP_RET_SINT:
            case OP_RET_C_SINT:
            case OP_RET_CN_SINT:
            case OP_RET_INT:
            case OP_RET_C_INT:
            case OP_RET_CN_INT:
            case OP_RET_DINT:
            case OP_RET_C_DINT:
            case OP_RET_CN_DINT:
            case OP_RET_USINT:
            case OP_RET_C_USINT:
            case OP_RET_CN_USINT:
            case OP_RET_UINT:
            case OP_RET_C_UINT:
            case OP_RET_CN_UINT:
            case OP_RET_UDINT:
            case OP_RET_C_UDINT:
            case OP_RET_CN_UDINT:
            case OP_RET_BYTE:
            case OP_RET_C_BYTE:
            case OP_RET_CN_BYTE:
            case OP_RET_WORD:
            case OP_RET_C_WORD:
            case OP_RET_CN_WORD:
            case OP_RET_DWORD:
            case OP_RET_C_DWORD:
            case OP_RET_CN_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_RET_LINT:
            case OP_RET_C_LINT:
            case OP_RET_CN_LINT:
            case OP_RET_ULINT:
            case OP_RET_C_ULINT:
            case OP_RET_CN_ULINT:
            case OP_RET_LWORD:
            case OP_RET_C_LWORD:
            case OP_RET_CN_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_RET_REAL:
            case OP_RET_C_REAL:
            case OP_RET_CN_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_RET_LREAL:
            case OP_RET_C_LREAL:
            case OP_RET_CN_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_RET_TIME:
            case OP_RET_C_TIME:
            case OP_RET_CN_TIME:
#endif

                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );
                }

                uSize = _op_byteSize( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );

                bCondition = BOOL_TRUE;
                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    bResult = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? BOOL_FALSE : BOOL_TRUE;
                    bCondition = ( uint8_t ) ( OP_CR_VAL_B8( pVM ) == bResult );
                }

                pVM->Register.PC += _op_byteAlignment( pVM, 
                        ( ( G_OPERAND( pVM->Register.BC ) & OPERAND_MASK ) != 0 ) ? 
                                sizeof( uint32_t ) : 
                                uSize );

                if( bCondition == BOOL_TRUE )
                {
                    if( pVM->Stack.Count == 0 )
                    {
                        /* pVM->Register.PC = pFormat->ByteCode; */
                        pVM->Finish = 1;

                        return 0;
                    }

                    ( void ) memset( &stValue, 0, sizeof( stValue ) );
                    stValue.Type = ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK );

                    switch( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK )
                    {
                        case OPERAND_BOOL:
                        case OPERAND_SINT:
                        case OPERAND_USINT:
                        case OPERAND_BYTE:

                            stValue.Value.B8 = OP_OPERAND_B8( pPointer );
                            /* stValue.Length = sizeof( uint8_t ); */
                            break;

                        case OPERAND_INT:
                        case OPERAND_UINT:
                        case OPERAND_WORD:

                            stValue.Value.B16 = OP_OPERAND_B16( pPointer );
                            /* stValue.Length = sizeof( uint16_t ); */
                            break;

                        case OPERAND_DINT:
                        case OPERAND_UDINT:
                        case OPERAND_DWORD:

                            stValue.Value.B32 = OP_OPERAND_B32( pPointer );
                            /* stValue.Length = sizeof( uint32_t ); */
                            break;

#ifdef CONFIG_SUPPORT_LONGLONG
                        case OPERAND_LINT:
                        case OPERAND_ULINT:
                        case OPERAND_LWORD:

                            stValue.Value.B64 = OP_OPERAND_B64( pPointer );
                            /* stValue.Length = sizeof( uint64_t ); */
                            break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                        case OPERAND_REAL:

                            stValue.Value.Float = OP_OPERAND_FLOAT( pPointer );
                            /* stValue.Length = sizeof( float ); */
                            break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                        case OPERAND_LREAL:

                            stValue.Value.Double = OP_OPERAND_DOUBLE( pPointer );
                            /* stValue.Length = sizeof( double ); */
                            break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                        case OPERAND_TIME:

                            stValue.Value.Time = OP_OPERAND_B32( pPointer );
                            /* stValue.Length = sizeof( uint32_t ); */
                            break;

#endif
                        case OPERAND_NONE:
                        default:

                            break;
                    }


                    /*
                        The following code restores the previous register-set from the stack and 
                        depending upon whether a return type has been specified for the former 
                        program organisation unit (POU), the current register value is overwritten 
                        with the value returned from the POU.  For determining whether this latter 
                        action should be performed, the IEC61131_TYPE associated with the return 
                        instruction is used, but indirectly by way of the size variable populated 
                        from this information in the event that conditions required for performing 
                        the return from the POU had not been met.
                    */

                    ( void ) memcpy( &pVM->Register, &pVM->Stack.Frame[ --pVM->Stack.Count ], sizeof( VM_REGISTER ) );

                    if( uSize != 0 )
                    {
                        ( void ) memcpy( &pVM->Register.CR, &stValue, sizeof( IEC61131_DATA ) );
                    }
                }

                break;

            case OP_ADD_P_SINT:
            case OP_ADD_P_USINT:
            case OP_ADD_P_BYTE:
            case OP_ADD_P_INT:
            case OP_ADD_P_UINT:
            case OP_ADD_P_WORD:
            case OP_ADD_P_DINT:
            case OP_ADD_P_UDINT:
            case OP_ADD_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_ADD_P_LINT:
            case OP_ADD_P_ULINT:
            case OP_ADD_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_ADD_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_ADD_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_ADD_P_TIME:
#endif
            case OP_AND_P_BOOL:
            case OP_AND_NP_BOOL:
            case OP_DIV_P_SINT:
            case OP_DIV_P_USINT:
            case OP_DIV_P_BYTE:
            case OP_DIV_P_INT:
            case OP_DIV_P_UINT:
            case OP_DIV_P_WORD:
            case OP_DIV_P_DINT:
            case OP_DIV_P_UDINT:
            case OP_DIV_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_DIV_P_LINT:
            case OP_DIV_P_ULINT:
            case OP_DIV_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_DIV_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_DIV_P_LREAL:
#endif
            case OP_EQ_P_SINT:
            case OP_EQ_P_USINT:
            case OP_EQ_P_BYTE:
            case OP_EQ_P_INT:
            case OP_EQ_P_UINT:
            case OP_EQ_P_WORD:
            case OP_EQ_P_DINT:
            case OP_EQ_P_UDINT:
            case OP_EQ_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_EQ_P_LINT:
            case OP_EQ_P_ULINT:
            case OP_EQ_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_EQ_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_EQ_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_EQ_P_TIME:
#endif
            case OP_GE_P_SINT:
            case OP_GE_P_USINT:
            case OP_GE_P_BYTE:
            case OP_GE_P_INT:
            case OP_GE_P_UINT:
            case OP_GE_P_WORD:
            case OP_GE_P_DINT:
            case OP_GE_P_UDINT:
            case OP_GE_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_GE_P_LINT:
            case OP_GE_P_ULINT:
            case OP_GE_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_GE_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_GE_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_GE_P_TIME:
#endif
            case OP_GT_P_SINT:
            case OP_GT_P_USINT:
            case OP_GT_P_BYTE:
            case OP_GT_P_INT:
            case OP_GT_P_UINT:
            case OP_GT_P_WORD:
            case OP_GT_P_DINT:
            case OP_GT_P_UDINT:
            case OP_GT_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_GT_P_LINT:
            case OP_GT_P_ULINT:
            case OP_GT_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_GT_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_GT_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_GT_P_TIME:
#endif
            case OP_LE_P_SINT:
            case OP_LE_P_USINT:
            case OP_LE_P_BYTE:
            case OP_LE_P_INT:
            case OP_LE_P_UINT:
            case OP_LE_P_WORD:
            case OP_LE_P_DINT:
            case OP_LE_P_UDINT:
            case OP_LE_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_LE_P_LINT:
            case OP_LE_P_ULINT:
            case OP_LE_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_LE_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_LE_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_LE_P_TIME:
#endif
            case OP_LT_P_SINT:
            case OP_LT_P_USINT:
            case OP_LT_P_BYTE:
            case OP_LT_P_INT:
            case OP_LT_P_UINT:
            case OP_LT_P_WORD:
            case OP_LT_P_DINT:
            case OP_LT_P_DWORD:
            case OP_LT_P_UDINT:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_LT_P_LINT:
            case OP_LT_P_ULINT:
            case OP_LT_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_LT_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_LT_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_LT_P_TIME:
#endif
            case OP_MOD_P_SINT:
            case OP_MOD_P_USINT:
            case OP_MOD_P_BYTE:
            case OP_MOD_P_INT:
            case OP_MOD_P_UINT:
            case OP_MOD_P_WORD:
            case OP_MOD_P_DINT:
            case OP_MOD_P_UDINT:
            case OP_MOD_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_MOD_P_LINT:
            case OP_MOD_P_ULINT:
            case OP_MOD_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_MOD_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_MOD_P_LREAL:
#endif
            case OP_MUL_P_SINT:
            case OP_MUL_P_USINT:
            case OP_MUL_P_BYTE:
            case OP_MUL_P_INT:
            case OP_MUL_P_UINT:
            case OP_MUL_P_WORD:
            case OP_MUL_P_DINT:
            case OP_MUL_P_UDINT:
            case OP_MUL_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_MUL_P_LINT:
            case OP_MUL_P_ULINT:
            case OP_MUL_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_MUL_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_MUL_P_LREAL:
#endif
            case OP_NE_P_SINT:
            case OP_NE_P_USINT:
            case OP_NE_P_BYTE:
            case OP_NE_P_INT:
            case OP_NE_P_UINT:
            case OP_NE_P_WORD:
            case OP_NE_P_DINT:
            case OP_NE_P_UDINT:
            case OP_NE_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_NE_P_LINT:
            case OP_NE_P_ULINT:
            case OP_NE_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_NE_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_NE_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_NE_P_TIME:
#endif
            case OP_OR_P_BOOL:
            case OP_OR_NP_BOOL:
            case OP_SUB_P_SINT:
            case OP_SUB_P_USINT:
            case OP_SUB_P_BYTE:
            case OP_SUB_P_INT:
            case OP_SUB_P_UINT:
            case OP_SUB_P_WORD:
            case OP_SUB_P_DINT:
            case OP_SUB_P_UDINT:
            case OP_SUB_P_DWORD:
#ifdef CONFIG_SUPPORT_LONGLONG
            case OP_SUB_P_LINT:
            case OP_SUB_P_ULINT:
            case OP_SUB_P_LWORD:
#endif
#ifdef CONFIG_SUPPORT_REAL
            case OP_SUB_P_REAL:
#endif
#ifdef CONFIG_SUPPORT_LREAL
            case OP_SUB_P_LREAL:
#endif
#ifdef CONFIG_SUPPORT_DATETIME
            case OP_SUB_P_TIME:
#endif
            case OP_XOR_P_BOOL:
            case OP_XOR_NP_BOOL:

                /*
                    For operations associated with deferred execution, the current register set 
                    is pushed onto the stack and execution of the operation subset is 
                    performed.  It is only upon the completion of execution of these subset 
                    operations that the execution of the immediate operator is subsequently 
                    performed - See OP_PARENTHESIS_NONE.
                */

                OP_CR_TYPE_CHECK( pVM, ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK ), 0 );
                OP_STACK_CHECK_MAX( pVM );

                ( void ) memcpy( &pVM->Stack.Frame[ pVM->Stack.Count++ ], &pVM->Register, sizeof( VM_REGISTER ) );

                break;

            case OP_PARENTHESIS_NONE:

                uPC = pVM->Register.PC;
                OP_CR_COPY( pVM, &stValue );

                OP_STACK_CHECK_ZERO( pVM );

                ( void ) memcpy( &pVM->Register, &pVM->Stack.Frame[ --pVM->Stack.Count ], sizeof( VM_REGISTER ) );

                if( G_OPERAND( pVM->Register.BC ) != stValue.Type )
                {
                    pVM->Register.ER = ERROR_INVALID_OPERAND;
                    break;
                }

                switch( pVM->Register.BC )
                {
                    case OP_ADD_P_SINT:
                    case OP_ADD_P_USINT:
                    case OP_ADD_P_BYTE:

                        OP_CR_VAL_B8( pVM ) += stValue.Value.B8;
                        break;

                    case OP_ADD_P_INT:
                    case OP_ADD_P_UINT:
                    case OP_ADD_P_WORD:

                        OP_CR_VAL_B16( pVM ) += stValue.Value.B16;
                        break;

                    case OP_ADD_P_DINT:
                    case OP_ADD_P_UDINT:
                    case OP_ADD_P_DWORD:

                        OP_CR_VAL_B32( pVM ) += stValue.Value.B32;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_ADD_P_LINT:
                    case OP_ADD_P_ULINT:
                    case OP_ADD_P_LWORD:

                        OP_CR_VAL_B64( pVM ) += stValue.Value.B64;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_ADD_P_REAL:

                        OP_CR_VAL_FLOAT( pVM ) += stValue.Value.Float;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_ADD_P_LREAL:

                        OP_CR_VAL_DOUBLE( pVM ) += stValue.Value.Double;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_ADD_P_TIME:

                        OP_CR_VAL_B32( pVM ) += stValue.Value.B32;
                        break;

#endif
                    case OP_AND_P_BOOL:
                    case OP_AND_NP_BOOL:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) &= ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                        break;

                    case OP_DIV_P_SINT:
                    case OP_DIV_P_USINT:
                    case OP_DIV_P_BYTE:

                        uVal8 = stValue.Value.B8;
                        if( uVal8 == 0u )
                        {
                            pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                            break;
                        }

                        OP_CR_VAL_B8( pVM ) /= uVal8;
                        break;

                    case OP_DIV_P_INT:
                    case OP_DIV_P_UINT:
                    case OP_DIV_P_WORD:

                        uVal16 = stValue.Value.B16;
                        if( uVal16 == 0u )
                        {
                            pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                            break;
                        }

                        OP_CR_VAL_B16( pVM ) /= uVal16;
                        break;

                    case OP_DIV_P_DINT:
                    case OP_DIV_P_UDINT:
                    case OP_DIV_P_DWORD:

                        uVal32 = stValue.Value.B32;
                        if( uVal32 == 0ul )
                        {
                            pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                            break;
                        }

                        OP_CR_VAL_B32( pVM ) /= uVal32;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_DIV_P_LINT:
                    case OP_DIV_P_ULINT:
                    case OP_DIV_P_LWORD:

                        uVal64 = stValue.Value.B64;
                        if( uVal64 == 0ull )
                        {
                            pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                            break;
                        }

                        OP_CR_VAL_B64( pVM ) /= uVal64;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_DIV_P_REAL:

                        fVal = stValue.Value.Float;
                        if( fVal == 0.0f )
                        {
                            pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                            break;
                        }

                        OP_CR_VAL_FLOAT( pVM ) /= fVal;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_DIV_P_LREAL:

                        dVal = stValue.Value.Double;
                        if( dVal == 0.0 )
                        {
                            pVM->Register.ER = ERROR_DIVIDE_BY_ZERO;
                            break;
                        }

                        OP_CR_VAL_DOUBLE( pVM ) /= dVal;
                        break;

#endif
                    case OP_EQ_P_SINT:
                    case OP_EQ_P_USINT:
                    case OP_EQ_P_BYTE:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B8( pVM ) == uVal8 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_EQ_P_INT:
                    case OP_EQ_P_UINT:
                    case OP_EQ_P_WORD:

                        uVal16 = stValue.Value.B16;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B16( pVM ) == uVal16 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_EQ_P_DINT:
                    case OP_EQ_P_UDINT:
                    case OP_EQ_P_DWORD:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) == uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_EQ_P_LINT:
                    case OP_EQ_P_ULINT:
                    case OP_EQ_P_LWORD:

                        uVal64 = stValue.Value.B64;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B64( pVM ) == uVal64 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_EQ_P_REAL:

                        fVal = stValue.Value.Float;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_FLOAT( pVM ) == fVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_EQ_P_LREAL:

                        dVal = stValue.Value.Double;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_DOUBLE( pVM ) == dVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_EQ_P_TIME:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) == uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
                    case OP_GE_P_SINT:
                    case OP_GE_P_USINT:
                    case OP_GE_P_BYTE:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B8( pVM ) >= uVal8 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_GE_P_INT:
                    case OP_GE_P_UINT:
                    case OP_GE_P_WORD:

                        uVal16 = stValue.Value.B16;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B16( pVM ) >= uVal16 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_GE_P_DINT:
                    case OP_GE_P_UDINT:
                    case OP_GE_P_DWORD:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) >= uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_GE_P_LINT:
                    case OP_GE_P_ULINT:
                    case OP_GE_P_LWORD:

                        uVal64 = stValue.Value.B64;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B64( pVM ) >= uVal64 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;
#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_GE_P_REAL:

                        fVal = stValue.Value.Float;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_FLOAT( pVM ) >= fVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_GE_P_LREAL:

                        dVal = stValue.Value.Double;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_DOUBLE( pVM ) >= dVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_GE_P_TIME:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) >= uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
                    case OP_GT_P_SINT:
                    case OP_GT_P_USINT:
                    case OP_GT_P_BYTE:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B8( pVM ) > uVal8 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_GT_P_INT:
                    case OP_GT_P_UINT:
                    case OP_GT_P_WORD:

                        uVal16 = stValue.Value.B16;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B16( pVM ) > uVal16 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_GT_P_DINT:
                    case OP_GT_P_UDINT:
                    case OP_GT_P_DWORD:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) > uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_GT_P_LINT:
                    case OP_GT_P_ULINT:
                    case OP_GT_P_LWORD:

                        uVal64 = stValue.Value.B64;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B64( pVM ) > uVal64 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_GT_P_REAL:

                        fVal = stValue.Value.Float;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_FLOAT( pVM ) > fVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_GT_P_LREAL:

                        dVal = stValue.Value.Double;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_DOUBLE( pVM ) > dVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_GT_P_TIME:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) > uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
                    case OP_LE_P_SINT:
                    case OP_LE_P_USINT:
                    case OP_LE_P_BYTE:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B8( pVM ) <= uVal8 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_LE_P_INT:
                    case OP_LE_P_UINT:
                    case OP_LE_P_WORD:

                        uVal16 = stValue.Value.B16;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B16( pVM ) <= uVal16 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_LE_P_DINT:
                    case OP_LE_P_UDINT:
                    case OP_LE_P_DWORD:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) <= uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_LE_P_LINT:
                    case OP_LE_P_ULINT:
                    case OP_LE_P_LWORD:

                        uVal64 = stValue.Value.B64;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B64( pVM ) <= uVal64 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_LE_P_REAL:

                        fVal = stValue.Value.Float;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_FLOAT( pVM ) <= fVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_LE_P_LREAL:

                        dVal = stValue.Value.Double;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_DOUBLE( pVM ) <= dVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_LE_P_TIME:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) <= uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
                    case OP_LT_P_SINT:
                    case OP_LT_P_USINT:
                    case OP_LT_P_BYTE:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B8( pVM ) < uVal8 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_LT_P_INT:
                    case OP_LT_P_UINT:
                    case OP_LT_P_WORD:

                        uVal16 = stValue.Value.B16;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B16( pVM ) < uVal16 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_LT_P_DINT:
                    case OP_LT_P_UDINT:
                    case OP_LT_P_DWORD:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) < uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_LT_P_LINT:
                    case OP_LT_P_ULINT:
                    case OP_LT_P_LWORD:

                        uVal64 = stValue.Value.B64;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B64( pVM ) < uVal64 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_LT_P_REAL:

                        fVal = stValue.Value.Float;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_FLOAT( pVM ) < fVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_LT_P_LREAL:

                        dVal = stValue.Value.Double;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_DOUBLE( pVM ) < dVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_LT_P_TIME:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) < uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
                    case OP_MOD_P_SINT:
                    case OP_MOD_P_USINT:
                    case OP_MOD_P_BYTE:

                        OP_CR_VAL_B8( pVM ) %= stValue.Value.B8;
                        break;

                    case OP_MOD_P_INT:
                    case OP_MOD_P_UINT:
                    case OP_MOD_P_WORD:

                        OP_CR_VAL_B16( pVM ) %= stValue.Value.B16;
                        break;

                    case OP_MOD_P_DINT:
                    case OP_MOD_P_UDINT:
                    case OP_MOD_P_DWORD:

                        OP_CR_VAL_B32( pVM ) %= stValue.Value.B32;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_MOD_P_LINT:
                    case OP_MOD_P_ULINT:
                    case OP_MOD_P_LWORD:

                        OP_CR_VAL_B64( pVM ) %= stValue.Value.B64;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_MOD_P_REAL:

                        OP_CR_VAL_FLOAT( pVM ) = ( float ) fmod( ( double ) OP_CR_VAL_FLOAT( pVM ), ( double ) stValue.Value.Float );
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_MOD_P_LREAL:

                        OP_CR_VAL_DOUBLE( pVM ) = fmod( OP_CR_VAL_DOUBLE( pVM ), stValue.Value.Double );
                        break;

#endif
                    case OP_MUL_P_SINT:
                    case OP_MUL_P_USINT:
                    case OP_MUL_P_BYTE:

                        OP_CR_VAL_B8( pVM ) *= stValue.Value.B8;
                        break;

                    case OP_MUL_P_INT:
                    case OP_MUL_P_UINT:
                    case OP_MUL_P_WORD:

                        OP_CR_VAL_B16( pVM ) *= stValue.Value.B16;
                        break;

                    case OP_MUL_P_DINT:
                    case OP_MUL_P_UDINT:
                    case OP_MUL_P_DWORD:

                        OP_CR_VAL_B32( pVM ) *= stValue.Value.B32;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_MUL_P_LINT:
                    case OP_MUL_P_ULINT:
                    case OP_MUL_P_LWORD:

                        OP_CR_VAL_B64( pVM ) *= stValue.Value.B64;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_MUL_P_REAL:

                        OP_CR_VAL_FLOAT( pVM ) *= stValue.Value.Float;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_MUL_P_LREAL:

                        OP_CR_VAL_DOUBLE( pVM ) *= stValue.Value.Double;
                        break;

#endif
                    case OP_NE_P_SINT:
                    case OP_NE_P_USINT:
                    case OP_NE_P_BYTE:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B8( pVM ) != uVal8 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_NE_P_INT:
                    case OP_NE_P_UINT:
                    case OP_NE_P_WORD:

                        uVal16 = stValue.Value.B16;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B16( pVM ) != uVal16 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

                    case OP_NE_P_DINT:
                    case OP_NE_P_UDINT:
                    case OP_NE_P_DWORD:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) != uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_NE_P_LINT:
                    case OP_NE_P_ULINT:
                    case OP_NE_P_LWORD:

                        uVal64 = stValue.Value.B64;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B64( pVM ) != uVal64 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_NE_P_REAL:

                        fVal = stValue.Value.Float;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_FLOAT( pVM ) != fVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_NE_P_LREAL:

                        dVal = stValue.Value.Double;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_DOUBLE( pVM ) != dVal ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_NE_P_TIME:

                        uVal32 = stValue.Value.B32;
                        OP_CR_VAL_B8( pVM ) = ( OP_CR_VAL_B32( pVM ) != uVal32 ) ? BOOL_TRUE : BOOL_FALSE;
                        OP_CR_TYPE( pVM ) = TYPE_BOOL;
                        break;

#endif
                    case OP_OR_P_BOOL:
                    case OP_OR_NP_BOOL:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) |= ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                        break;

                    case OP_SUB_P_SINT:
                    case OP_SUB_P_USINT:
                    case OP_SUB_P_BYTE:

                        OP_CR_VAL_B8( pVM ) -= stValue.Value.B8;
                        break;

                    case OP_SUB_P_INT:
                    case OP_SUB_P_UINT:
                    case OP_SUB_P_WORD:

                        OP_CR_VAL_B16( pVM ) -= stValue.Value.B16;
                        break;

                    case OP_SUB_P_DINT:
                    case OP_SUB_P_UDINT:
                    case OP_SUB_P_DWORD:

                        OP_CR_VAL_B32( pVM ) -= stValue.Value.B32;
                        break;

#ifdef CONFIG_SUPPORT_LONGLONG
                    case OP_SUB_P_LINT:
                    case OP_SUB_P_ULINT:
                    case OP_SUB_P_LWORD:

                        OP_CR_VAL_B64( pVM ) -= stValue.Value.B64;
                        break;

#endif
#ifdef CONFIG_SUPPORT_REAL
                    case OP_SUB_P_REAL:

                        OP_CR_VAL_FLOAT( pVM ) -= stValue.Value.Float;
                        break;

#endif
#ifdef CONFIG_SUPPORT_LREAL
                    case OP_SUB_P_LREAL:

                        OP_CR_VAL_DOUBLE( pVM ) -= stValue.Value.Double;
                        break;

#endif
#ifdef CONFIG_SUPPORT_DATETIME
                    case OP_SUB_P_TIME:

                        OP_CR_VAL_B32( pVM ) -= stValue.Value.B32;
                        break;

#endif
                    case OP_XOR_P_BOOL:
                    case OP_XOR_NP_BOOL:

                        uVal8 = stValue.Value.B8;
                        OP_CR_VAL_B8( pVM ) ^= ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? ~uVal8 : uVal8;
                        break;

                    default:

                        pVM->Register.ER = ERROR_INVALID_BYTECODE;
                        break;
                }

                pVM->Register.PC = uPC;
                break;

            case OP_CAL_NONE:
            case OP_CAL_C_NONE:
            case OP_CAL_CN_NONE:

                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    OP_CR_TYPE_CHECK( pVM, TYPE_BOOL, 0 );
                }

                bCondition = BOOL_TRUE;
                if( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_C ) != 0 )
                {
                    bResult = ( ( G_MODIFIER( pVM->Register.BC ) & MODIFIER_N ) != 0 ) ? BOOL_FALSE : BOOL_TRUE;
                    bCondition = ( uint8_t ) ( OP_CR_VAL_B8( pVM ) == bResult );
                }

                pVM->Register.PC += _op_byteAlignment( pVM, sizeof( uint32_t ) );

                if( bCondition == BOOL_TRUE )
                {
                    /*
                        It should be noted that the handling of the call operation will differ from 
                        this point depending upon whether the target function is an application 
                        program organisation unit or an embedded library function.  In the former 
                        case, the current register set is pushed onto the stack and program counter 
                        register updated to point to the target application program organisation 
                        unit, which will have been resolved at compile time.  For embedded library 
                        functions however, the operation is a little different with the target 
                        function called and return values handled in the context of the current 
                        register set.
                    */

                    uVal32 = OP_OPERAND_B32( pPointer );
                    switch( uVal32 & ( IEC61131_BYTECODE_FUNCTION | IEC61131_BYTECODE_FUNCTION_BLOCK ) )
                    {
                        case IEC61131_BYTECODE_FUNCTION:

                            ( void ) _op_callLibraryFunction( pVM, ( uVal32 & ~IEC61131_BYTECODE_FUNCTION ) );
                            break;

                        case IEC61131_BYTECODE_FUNCTION_BLOCK:

                            ( void ) _op_callLibraryFunctionBlock( pVM, ( uVal32 & ~IEC61131_BYTECODE_FUNCTION_BLOCK ) );
                            break;

                        default:

                            OP_STACK_CHECK_MAX( pVM );

                            ( void ) memcpy( &pVM->Stack.Frame[ pVM->Stack.Count++ ], &pVM->Register, sizeof( VM_REGISTER ) );
                            ( void ) memset( &pVM->Register.CR, 0, sizeof( pVM->Register.CR ) );
                            pVM->Register.CR.Type = TYPE_NONE;
                            pVM->Register.PC = uVal32;

                            break;
                    }

                }

                break;

            default:

                if( pVM->Debug != 0 )
                {
                    LOG_ERR( "Unhandled BC %04x, PC %u", 
                            ( pVM->Register.BC & ~OPERAND_MASK ),
                            uPC - pFormat->ByteCode );
                }
                pVM->Register.ER = ERROR_INVALID_BYTECODE;
                break;
        }

        if( ( G_OPERAND( pVM->Register.BC ) & OPERAND_DIRECT_MASK ) != 0 )
        {
            switch( G_OPERAND( pVM->Register.BC ) & OPERAND_DIRECT_MASK )
            {
                case OPERAND_DIRECT_MEMORY:

                    _op_byteAlignOutput( 
                            pVM, 
                            IO_TYPE_MEMORY,
                            pVM->IO.Memory.Memory, 
                            ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK ),
                            uOffset, 
                            sBuffer );
                    break;

                case OPERAND_DIRECT_OUTPUT:

                    _op_byteAlignOutput( 
                            pVM, 
                            IO_TYPE_OUTPUT,
                            pVM->IO.Outputs.Memory, 
                            ( G_OPERAND( pVM->Register.BC ) & ~OPERAND_MASK ),
                            uOffset, 
                            sBuffer );
                    break;

                case OPERAND_DIRECT_INPUT:
                default:

                    break;
            }
        }

        if( pVM->State == VM_STATE_DEBUG )
        {
            break;
        }
    }


    /*
        The following code is something of a sanity check for both the IEC 61131-3 
        compiler and virtual machine run-time ensuring that all stack and parameter 
        operations are correctly cleaned up at the end of each cycle of byte code 
        execution.  This code may be disabled for performance optimisation if 
        required.
    */

    if( pVM->Register.ER == 0 )
    {
        for( ;; )
        {
            if( pVM->Stack.Count > 0 )
            {
                pVM->Register.ER = ERROR_STACK_UNBALANCED;
                break;
            }
            if( pVM->Parameters.Count > 0 )
            {
                pVM->Register.ER = ERROR_PARAMETER_UNBALANCED;
                break;
            }

            /* pVM->Register.PC = pFormat->ByteCode; */
            pVM->Finish = 1;
            break;
        }
    }

    return ( int ) pVM->Register.ER;
}


