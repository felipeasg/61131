#include <stdlib.h>
#include <errno.h>

#include "iec61131_list.h"


static void _iec61131_destroyList( void *pSelf );


/*!
    \fn static void _iec61131_destroyList( void *pSelf )
    \brief Function for the destruction of a IEC61131_LIST linked-list data structure
    \param pSelf Pointer to IEC61131_LIST data structure
    \returns No return value

    This function will perform the destruction of a IEC61131_LIST data
    structure.  This is performed through the iteration of all list elements,
    sequentially destroying each of these ahead of destruction of the container
    IEC61131_LIST data structure.
*/

static void
_iec61131_destroyList( void *pSelf )
{
    IEC61131_LIST *pList;
    IEC61131_LIST_ELEMENT *pElement;


    if( pSelf != NULL )
    {
        pList = ( IEC61131_LIST * ) pSelf;

        while( ( pElement = IEC61131_LIST_HEAD( pList ) ) != NULL )
        {
            BASE_DESTROY( pElement );
            pElement = NULL;
        }

        free( pList );
        pList = NULL;
    }
}


/*!
    \fn IEC61131_LIST * iec61131_allocateList( void )
    \brief Function to allocate memory for IEC61131_LIST linked-list data structure
    \returns Returns pointer to newly allocated IEC61131_LIST linked-list data structure on success, NULL on failure
*/

IEC61131_LIST *
iec61131_allocateList( void )
{
    IEC61131_LIST *pList;


    if( ( pList = ( IEC61131_LIST * ) calloc( 1, sizeof( *pList ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pList, TYPE_LIST );
    BASE_SET_DESTROY( pList, _iec61131_destroyList );

    iec61131_listInitialise( pList );

    return pList;
}


void
iec61131_listConsolidate( IEC61131_LIST *pList, IEC61131_LIST *pList2 )
{
    IEC61131_LIST_ELEMENT *pElement;


    if( pList2->Head == NULL )
    {
        return;
    }

    if( pList->Head == NULL )
    {
        pList->Head = pList2->Head;
        pList->Tail = pList2->Tail;
        pList->Size = pList2->Size;
    }
    else
    {
        pElement = pList->Tail;
        pElement->Next = pList2->Head;
        pElement->Next->Previous = pElement;
        pList->Tail = pList2->Tail;
        pList->Size += pList2->Size;
    }

    pList2->Head = NULL;
    pList2->Tail = NULL;
    pList2->Size = 0;
    BASE_DESTROY( pList2 );
}


/*!
    \fn void iec61131_listInitialise( IEC61131_LIST *pList )
    \brief Initialise a IEC61131_LIST linked-list data structure
    \param pList Pointer to IEC61131_LIST linked-list data structure
    \returns No return value
*/

void
iec61131_listInitialise( IEC61131_LIST *pList )
{
    pList->Head = NULL;
    pList->Tail = NULL;
    pList->Size = 0;
}


int
iec61131_listInsertHead( IEC61131_LIST *pList, void *pData )
{
    IEC61131_LIST_ELEMENT *pElement, *pNew;


    if( ( pNew = ( IEC61131_LIST_ELEMENT * ) calloc( 1, sizeof( *pNew ) ) ) == NULL )
    {
        return -errno;
    }
    BASE_SET_TYPE( pNew, TYPE_LIST_ELEMENT );
    BASE_SET_DESTROY( pNew, free );
    pNew->Data = pData;
    pNew->Previous = NULL;
    pNew->Next = NULL;

    if( pList->Head == NULL )
    {
        pList->Head = pNew;
        pList->Head->Previous = NULL;
        pList->Head->Next = NULL;
        pList->Tail = pNew;
    }
    else
    {
        pElement = pList->Head;
        pElement->Previous = pNew;
        pNew->Next = pElement;
        pList->Head = pNew;
    }

    ++pList->Size;
    return 0;
}


int
iec61131_listInsertTail( IEC61131_LIST *pList, void *pData )
{
    IEC61131_LIST_ELEMENT *pElement, *pNew;


    if( ( pNew = ( IEC61131_LIST_ELEMENT * ) calloc( 1, sizeof( *pNew ) ) ) == NULL )
    {
        return -errno;
    }
    BASE_SET_TYPE( pNew, TYPE_LIST_ELEMENT );
    BASE_SET_DESTROY( pNew, free );
    pNew->Data = pData;
    pNew->Previous = NULL;
    pNew->Next = NULL;

    if( pList->Head == NULL )
    {
        pList->Head = pNew;
        pList->Head->Previous = NULL;
        pList->Head->Next = NULL;
        pList->Tail = pNew;
    }
    else
    {
        pElement = pList->Tail;
        pElement->Next = pNew;
        pNew->Previous = pElement;
        pList->Tail = pNew;
    }

    ++pList->Size;
    return 0;
}


int
iec61131_listRemove( IEC61131_LIST *pList, IEC61131_LIST_ELEMENT *pElement, void **pData )
{
    if( ( pElement == NULL ) ||
            ( pList->Size == 0 ) )
    {
        return -ENOENT;
    }
    *pData = pElement->Data;

    if( pElement == pList->Head )
    {
        pList->Head = pElement->Next;
        if( pList->Head == NULL )
        {
            pList->Tail = NULL;
        }
        else
        {
            pElement->Next->Previous = NULL;
        }
    }
    else
    {
        pElement->Previous->Next = pElement->Next;
        if( pElement->Next == NULL )
        {
            pList->Tail = pElement->Previous;
        }
        else
        {
            pElement->Next->Previous = pElement->Previous;
        }
    }
    BASE_DESTROY( pElement );

    --pList->Size;
    return 0;
}

