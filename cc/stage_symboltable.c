#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>

#include "iec61131_bytecode.h"
#include "iec61131_symbol.h"
#include "stage.h"


int
stage_symboltable( IEC61131_CC *pContext )
{
    IEC61131_FORMAT stFormat;
    IEC61131_SYMBOL *pSymbol;
    unsigned int uPad, uSymbol;
    char *pData;


    /*
        The following code writes the byte code location for the symbol table into 
        the IEC61131_FORMAT structure that constitutes the output file format and 
        then iterates through the symbol table, writing initial values to the 
        output memory block in a memory aligned fashion.
    */

    ( void ) memset( &stFormat, 0, sizeof( stFormat ) );
    stFormat.Symbols = pContext->ByteCode.End;
    memblock_write( &pContext->ByteCode, offsetof( IEC61131_FORMAT, Symbols ), ( char * ) &stFormat.Symbols, sizeof( stFormat.Symbols ) );

    /* uStart = pContext->ByteCode.End; */
    for( uSymbol = 0; uSymbol < pContext->SymbolTable.Count; ++uSymbol )
    {
        pSymbol = ( IEC61131_SYMBOL * ) &pContext->SymbolTable.Entries[ uSymbol ];
        /* assert( pSymbol->Offset == pContext->ByteCode.End - uStart ); */

        pData = NULL;
        switch( pSymbol->Data.Type )
        {
            case TYPE_BOOL:
            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                pData = ( char * ) &pSymbol->Data.Value.B1;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                pData = ( char * ) &pSymbol->Data.Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:

                pData = ( char * ) &pSymbol->Data.Value.B32;
                break;

            case TYPE_LINT:
            case TYPE_ULINT:

                pData = ( char * ) &pSymbol->Data.Value.B64;
                break;

            case TYPE_REAL:

                pData = ( char * ) &pSymbol->Data.Value.Float;
                break;

            case TYPE_LREAL:

                pData = ( char * ) &pSymbol->Data.Value.Double;
                break;

            default:

                continue;
        }

        /* assert( pData != NULL ); */
        memblock_write( &pContext->ByteCode, pContext->ByteCode.End, pData, pSymbol->Size );
        if( ( pSymbol->Size % pContext->Align ) != 0 )
        {
            for( uPad = pContext->Align - ( pSymbol->Size % pContext->Align ); uPad > 0; --uPad )
            {
                memblock_append( &pContext->ByteCode, 0 );
            }
        }
    }

    return 0;
}

