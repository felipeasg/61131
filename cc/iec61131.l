%option case-insensitive
%option never-interactive
%option noinput
%option nounput
%option noyy_top_state 
%option noyywrap
%option yylineno
%pointer

%s Declaration_State
%s Body_State
%s IL_State

%{
#include <stdio.h>

#include "iec61131_cc.h"
#include "iec61131_token.h"

#include "iec61131.h"


#define IEC61131_RETURN_INSTRUCTION( __Id ) {                                           \
    yylval.token = ( IEC61131_TOKEN * ) IEC61131_NEW_INSTRUCTION( __Id );               \
    return __Id; }

#define IEC61131_RETURN_RAW( __Id, __Data, __Length ) {                                 \
    yylval.token = ( IEC61131_TOKEN * ) IEC61131_NEW_RAW( __Id, __Data, __Length );     \
    return __Id; }

#define IEC61131_RETURN_TOKEN( __Id ) {                                                 \
    yylval.token = ( IEC61131_TOKEN * ) IEC61131_NEW_TOKEN( __Id );                     \
    return __Id; }


int yycolumn = 1;

#define YY_USER_ACTION {                                                                \
    yylloc.first_line = yylloc.last_line = yylineno;                                    \
    yylloc.first_column = yycolumn;                                                     \
    yylloc.last_column = yycolumn + yyleng - 1;                                         \
    yycolumn += yyleng;                                                                 \
    }

%}


extern YYSTYPE yylval;


asterisk                "*"
not_asterisk            [^*]
not_close_or_asterisk   [^*)]
comment_text            {not_asterisk}|(({asterisk}+){not_close_or_asterisk})
comment                 "(*"({comment_text}*)({asterisk}+)")"

whitespace_only         [ \f\n\r\t\v]*
whitespace_text         {whitespace_only}|{comment}
whitespace              {whitespace_text}*

il_whitespace_only      [ \f\r\t\v]*
il_whitespace_text      {il_whitespace_only}|{comment}
il_whitespace           {il_whitespace_text}*
                        

/* 
    B.1.1 Letters, digits and identifiers

    letter  ::=  'A' | 'B' | <...> | 'Z' | 'a' | 'b' | <...> | 'z' 
    digit ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' 
    octal_digit ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7'
    hex_digit ::= digit | 'A'|'B'|'C'|'D'|'E'|'F'
    identifier ::= (letter | ('_' (letter | digit))) {['_'] (letter | digit)}
*/

letter                  [A-Za-z]
digit                   [0-9]
octal_digit             [0-7]
hex_digit               {digit}|[A-Fa-f]
identifier              ({letter}|(_({letter}|{digit})))((_?({letter}|{digit}))*)


/*
    B.1.2 Constants

    B.1.2.1 Numeric literals

    integer ::= digit {['_'] digit}
    binary_integer ::= '2#' bit {['_'] bit}
    bit ::= '1' | '0'
    octal_integer ::= '8#' octal_digit {['_'] octal_digit}
    hex_integer ::= '16#' hex_digit {['_'] hex_digit}
    real_literal ::= [ real_type_name '#' ]
            signed_integer '.' integer [exponent]
    exponent ::= ('E' | 'e') ['+'|'-'] integer
*/

integer                 {digit}((_?{digit})*)
binary_integer          2#{bit}((_?{bit})*)
bit                     [0-1]
octal_integer           8#{octal_digit}((_?{octal_digit})*)
hex_integer             16#{hex_digit}((_?{hex_digit})*)
real                    {integer}\.{integer}{exponent}?
exponent                [Ee]([+-]?){integer}


/*
    B.1.2.2 Character strings

    single_byte_character_string ::= "'" {single_byte_character_representation} "'"
    double_byte_character_string ::= '"' {double_byte_character_representation} '"'
    single_byte_character_representation ::= common_character_representation 
            | "$'" | '"' | '$' hex_digit hex_digit
    double_byte_character_representation ::= common_character_representation 
            | '$"' | "'"| '$' hex_digit hex_digit hex_digit hex_digit
    common_character_representation ::= 
            <any printable character except '$', '"' or "'"> 
            | '$$' | '$L' | '$N' | '$P' | '$R' | '$T' 
            | '$l' | '$n' | '$p' | '$r' | '$t' 
*/

single_byte_character_string            '({single_byte_character_representation}*)'
double_byte_character_string            \"({double_byte_character_representation}*)\"
single_byte_character_representation    {common_character_representation}|$'|\"|(${hex_digit}{hex_digit})
double_byte_character_representation    {common_character_representation}|$\"|'|(${hex_digit}{hex_digit}{hex_digit}{hex_digit})
common_character_representation         [\x20\x21\x23\x25\x26\x28-\x7e]|{escape_character}
escape_character                        $$|$L|$N|$P|$R|$T|$l|$n|$p|$r|$t


/*
    B.1.2.3.1 Duration

    duration ::= ('T' | 'TIME') '#' ['-'] interval
    interval ::= days | hours | minutes | seconds | milliseconds
    days ::= fixed_point ('d') | integer ('d') ['_'] hours
    fixed_point ::= integer [ '.' integer]
    hours ::= fixed_point ('h') | integer ('h') ['_'] minutes
    minutes ::= fixed_point ('m')  | integer ('m') ['_'] seconds
    seconds ::= fixed_point ('s') | integer ('s') ['_'] milliseconds
    milliseconds ::= fixed_point ('ms')
*/

duration                ((T)|(TIME))#(-)?{interval}
interval                {days}?{hours}?{minutes}?{seconds}?{milliseconds}?
days                    {integer}d(_?{hours})?
/* fixed_point             {integer}\.{integer} */
hours                   {integer}h(_?{minutes})?
minutes                 {integer}m(_?{seconds})?
seconds                 {integer}s(_?{milliseconds})?
milliseconds            ({integer}ms)


/*
    B.1.2.3.2 Time of day and date

    time_of_day ::= ('TIME_OF_DAY' | 'TOD')  '#' daytime
    daytime ::= day_hour ':' day_minute ':' day_second
    day_hour ::= integer
    day_minute ::= integer
    day_second ::= fixed_point
    date ::= ('DATE' | 'D') '#' date_literal
    date_literal ::= year '-' month '-' day
    year ::= integer
    month ::= integer
    day ::= integer
    date_and_time ::= ('DATE_AND_TIME' | 'DT') '#' date_literal '-' daytime
*/

time_of_day             ((TIME_OF_DAY)|(TOD))#{daytime}
daytime                 {day_hour}:{day_hour}:{day_second}
day_hour                {integer}
day_minute              {integer}
day_second              {integer}
date                    ((DATE)|(D))#{date_literal}
date_literal            {year}-{month}-{day}
year                    {integer}
month                   {integer}
day                     {integer}


/*
    B.1.4.1 Directly represented variables

    direct_variable ::= '%' location_prefix size_prefix integer {'.' integer}
    location_prefix ::= 'I' | 'Q' | 'M'
    size_prefix ::= NIL | 'X' | 'B' | 'W' | 'D' | 'L'
*/

direct_variable         %{location_prefix}({size_prefix}?){integer}((.{integer})*)
location_prefix         [IQM]
size_prefix             [XBWDL]


%%

{
{il_whitespace}
}

<INITIAL>{
\n                      { yycolumn = 1; }

FUNCTION                BEGIN(Body_State); return FUNCTION;
FUNCTION_BLOCK          BEGIN(Body_State); return FUNCTION_BLOCK;
PROGRAM					BEGIN(Body_State); return PROGRAM;
}

<Body_State>{
\n                      { yycolumn = 1; }

}

<Body_State,IL_State>{

AT                      return AT;
CONSTANT                return CONSTANT;
END_FUNCTION            BEGIN(INITIAL); return END_FUNCTION; 
END_FUNCTION_BLOCK      BEGIN(INITIAL); return END_FUNCTION_BLOCK;
END_PROGRAM             BEGIN(INITIAL); return END_PROGRAM;
END_VAR                 return END_VAR;
NON_RETAIN              return NON_RETAIN;
READ_ONLY               return READ_ONLY;
READ_WRITE              return READ_WRITE;
RETAIN                  return RETAIN;
VAR                     return VAR;
VAR_ACCESS              return VAR_ACCESS;
VAR_CONFIG              return VAR_CONFIG;
VAR_EXTERNAL            return VAR_EXTERNAL;
VAR_GLOBAL              return VAR_GLOBAL;
VAR_IN_OUT              return VAR_IN_OUT;
VAR_INPUT               return VAR_INPUT;
VAR_OUTPUT              return VAR_OUTPUT;
VAR_TEMP                return VAR_TEMP;

    /* B.1.2.1 Numeric literals */

BOOL#0                  IEC61131_RETURN_RAW( boolean, "BOOL#0", 6 );
BOOL#FALSE              IEC61131_RETURN_RAW( boolean, "BOOL#0", 6 );
FALSE                   IEC61131_RETURN_RAW( boolean, "BOOL#0", 6 );
BOOL#1                  IEC61131_RETURN_RAW( boolean, "BOOL#1", 6 );
BOOL#TRUE               IEC61131_RETURN_RAW( boolean, "BOOL#1", 6 );
TRUE                    IEC61131_RETURN_RAW( boolean, "BOOL#1", 6 );

	/* B.1.3.1 Elementary data types */

BOOL                    IEC61131_RETURN_RAW( BOOL, yytext, yyleng );
BYTE                    IEC61131_RETURN_RAW( BYTE, yytext, yyleng );
WORD                    IEC61131_RETURN_RAW( WORD, yytext, yyleng );
DWORD                   IEC61131_RETURN_RAW( DWORD, yytext, yyleng );
LWORD                   IEC61131_RETURN_RAW( LWORD, yytext, yyleng );
DATE                    IEC61131_RETURN_RAW( DATE, yytext, yyleng );
TIME_OF_DAY             IEC61131_RETURN_RAW( TIME_OF_DAY, yytext, yyleng );
TOD                     IEC61131_RETURN_RAW( TOD, yytext, yyleng );
DATE_AND_TIME           IEC61131_RETURN_RAW( DATE_AND_TIME, yytext, yyleng );
DT                      IEC61131_RETURN_RAW( DT, yytext, yyleng );
REAL                    IEC61131_RETURN_RAW( REAL, yytext, yyleng );
LREAL                   IEC61131_RETURN_RAW( LREAL, yytext, yyleng );
USINT                   IEC61131_RETURN_RAW( USINT, yytext, yyleng );
UINT                    IEC61131_RETURN_RAW( UINT, yytext, yyleng );
UDINT                   IEC61131_RETURN_RAW( UDINT, yytext, yyleng );
ULINT                   IEC61131_RETURN_RAW( ULINT, yytext, yyleng );
SINT                    IEC61131_RETURN_RAW( SINT, yytext, yyleng );
INT                     IEC61131_RETURN_RAW( INT, yytext, yyleng );
DINT                    IEC61131_RETURN_RAW( DINT, yytext, yyleng );
LINT                    IEC61131_RETURN_RAW( LINT, yytext, yyleng );
STRING                  IEC61131_RETURN_RAW( STRING, yytext, yyleng );
WSTRING                 IEC61131_RETURN_RAW( WSTRING, yytext, yyleng );
TIME                    IEC61131_RETURN_RAW( TIME, yytext, yyleng );

	/* B.2.2 Operators */

NOP                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( NOP );
LD                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( LD );
LDN                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( LDN );
ST                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( ST );
STN                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( STN );
NOT                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( NOT );
S                       BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( S );
R                       BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( R );
 /* S1                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( S1 ); */
 /* R1                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( R1 ); */
 /* CLK                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( CLK ); */
 /* CU                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( CU ); */
 /* CD                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( CD ); */
 /* PV                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( PV ); */
 /* IN                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( IN ); */
 /* PT                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( PT ); */
AND                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( AND );
 /* &                       BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( AND2 ); */
OR                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( OR );
XOR                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( XOR );
ANDN                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( ANDN );
 /* &N                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( ANDN2 ); */
ORN                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( ORN );
XORN                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( XORN );
ADD                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( ADD );
SUB                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( SUB );
MUL                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( MUL );
DIV                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( DIV );
MOD                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( MOD );
GT                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( GT );
GE                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( GE );
">="                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( GE2 );
EQ                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( EQ );
LT                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( LT );
LE                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( LE );
"<="                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( LE2 );
NE                      BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( NE );
"<>"                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( NE2 );
"**"                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( PWR );
CAL                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( CAL );
CALC                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( CALC );
CALCN                   BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( CALCN );
RET                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( RET );
RETC                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( RETC );
RETCN                   BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( RETCN );
JMP                     BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( JMP );
JMPC                    BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( JMPC );
JMPCN                   BEGIN( IL_State ); IEC61131_RETURN_INSTRUCTION( JMPCN );
}

<IL_State>{
\n                      { yycolumn = 1; return EOL; }
}

":="                    IEC61131_RETURN_INSTRUCTION( ASSIGN );
"=>"                    IEC61131_RETURN_INSTRUCTION( SENDTO );

{binary_integer}        IEC61131_RETURN_RAW( binary_integer, yytext, yyleng );
{date}                  IEC61131_RETURN_RAW( date, yytext, yyleng );
{direct_variable}       IEC61131_RETURN_RAW( direct_variable, yytext, yyleng );
{duration}              IEC61131_RETURN_RAW( duration, yytext, yyleng );
{hex_integer}           IEC61131_RETURN_RAW( hex_integer, yytext, yyleng );
{integer}               IEC61131_RETURN_RAW( integer, yytext, yyleng );
{octal_integer}         IEC61131_RETURN_RAW( octal_integer, yytext, yyleng );
{real}                  IEC61131_RETURN_RAW( real, yytext, yyleng );
{time_of_day}           IEC61131_RETURN_RAW( time_of_day, yytext, yyleng );

{identifier}            IEC61131_RETURN_RAW( identifier, yytext, yyleng );

.                       { return yytext[0]; }

%%

