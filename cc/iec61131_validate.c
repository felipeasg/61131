#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include "iec61131_cc.h"
#include "iec61131_error.h"
#include "iec61131_list.h"
#include "iec61131_token.h"
#include "iec61131_validate.h"

#include "iec61131.h"


static int _iec61131_validateVariable( IEC61131_TOKEN_POU *pPOU, char *pName );


static int
_iec61131_validateVariable( IEC61131_TOKEN_POU *pPOU, char *pName )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN_VARIABLE *pVar;


    for( pElement = IEC61131_LIST_HEAD( pPOU->Variables );
            pElement;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pVar = ( IEC61131_TOKEN_VARIABLE * ) pElement->Data;
        if( strcmp( pVar->Name, pName ) == 0 )
        {
            return 0;
        }
    }
    return -ENOENT;
}


int
iec61131_validatePOU( IEC61131_CC *pContext, IEC61131_TOKEN_POU *pPOU )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN_INSTRUCTION *pInstr;
    IEC61131_TOKEN_RAW *pRaw;
    char *pField, szMessage[LINE_MAX], szVariable[LINE_MAX];
    int nResult;


    /*
        Step through each of the identifier tokens within the POU body and, where 
        possible, validate these against other library definitions.
    */

    for( pElement = IEC61131_LIST_HEAD( pPOU->Body ), nResult = 0;
            ( ( pElement != NULL ) &&
                    ( nResult == 0 ) );
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;
        pRaw = ( IEC61131_TOKEN_RAW * ) pInstr->Operand;

        if( ( pInstr->Operand == NULL ) ||
                ( pInstr->Operand->Id != identifier ) )
        {
            continue;
        }


        /*
            The following reduces structured variables to their record name only - At 
            this point, no check is performed on the field selector portion of the 
            structured variable name.  This is primarily because this validation is 
            performed during, rather than after the completion of, source code
            parsing.
        */

        ( void ) strcpy( szVariable, pRaw->Data );
        if( ( pField = strchr( szVariable, '.' ) ) != NULL )
        {
            *pField++ = '\0';
        }

        switch( pInstr->Token.Id )
        {
            case CAL:           /* Function block */
            case CALC:          /* Function block */
            case CALCN:         /* Function block */
            case JMP:           /* Label */
            case JMPC:          /* Label */
            case JMPCN:         /* Label */

                break;

            default:            /* Variable */

                nResult = _iec61131_validateVariable( pPOU, szVariable );
                break;
        }

        if( pField != NULL )
        {
        }
    }

    if( nResult != 0 )
    {
        ( void ) snprintf( szMessage, sizeof( szMessage ), "'%s' undeclared (first use in %s POU)",
                pRaw->Data,
                pPOU->Name );

        IEC61131_ERROR( pContext->Source, pRaw->Token.Line, pRaw->Token.Column, szMessage );
    }

    return nResult;
}
