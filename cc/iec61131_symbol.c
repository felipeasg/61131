#include <stdlib.h>

#include "iec61131_cc.h"
#include "iec61131_types.h"
#include "iec61131.h"


static void _iec61131_destroySymbol( void *pSelf );


/*!
    \fn static void _iec61131_destroySymbol( void *pSelf )
    \brief Destructor for IEC61131_SYMBOL data structure
    \param pSelf Pointer to IEC61131_SYMBOL data structure
    \returns No return value
*/

static void
_iec61131_destroySymbol( void *pSelf )
{
    IEC61131_SYMBOL *pSymbol;


    /* assert( pSelf != NULL ); */
    if( pSelf != NULL )
    {
        pSymbol = ( IEC61131_SYMBOL * ) pSelf;
        /* assert( IEC61131_TYPE( pSymbol ) == TYPE_SYMBOL ); */

        free( pSymbol );
        pSymbol = NULL;
    }
}


/*!
    \fn IEC61131_SYMBOL * iec61131_allocateSymbol( IEC61131_DATA_TYPE eType, char *pName )
    \brief Allocate memory and initialise IEC61131_SYMBOL data structure
    \param eType IEC 61131-3 symbol type identifier
    \param pName IEC 61131-3 symbol name
    \returns Returns pointer to new IEC61131_SYMBOL data structure on success, NULL on failure
*/

IEC61131_SYMBOL *
iec61131_allocateSymbol( IEC61131_TYPE eType, char *pName )
{
    IEC61131_SYMBOL *pSymbol;


    if( pName == NULL )
    {
        return NULL;
    }

    if( ( pSymbol = ( IEC61131_SYMBOL * ) calloc( 1, sizeof( *pSymbol ) ) ) == NULL )
    {
        return NULL;
    }
    BASE_SET_TYPE( pSymbol, TYPE_SYMBOL );
    BASE_SET_DESTROY( pSymbol, _iec61131_destroySymbol );

    ( void ) strncpy( pSymbol->Name, pName, sizeof( pSymbol->Name ) );
    pSymbol->Data.Type = eType;
    pSymbol->Data.Length = 0;
    pSymbol->Direct = 0;
    pSymbol->Offset = 0;
    pSymbol->Size = 0;
    pSymbol->Location = NULL;
    pSymbol->Scope = NULL;

    return pSymbol;
}


IEC61131_TYPE
iec61131_symbolTypeMapping( int nId )
{
    switch( nId )
    {
        case BOOL:              return TYPE_BOOL;
        case BYTE:              return TYPE_BYTE;
        case WORD:              return TYPE_WORD;
        case DWORD:             return TYPE_DWORD;
        case LWORD:             return TYPE_LWORD;
        case DATE:              return TYPE_DATE;
        case TIME_OF_DAY:       /* break; */
        case TOD:               return TYPE_TOD;
        case DATE_AND_TIME:     /* break; */
        case DT:                return TYPE_DT;
        case REAL:              return TYPE_REAL;
        case LREAL:             return TYPE_LREAL;
        case USINT:             return TYPE_USINT;
        case UINT:              return TYPE_UINT;
        case UDINT:             return TYPE_UDINT;
        case ULINT:             return TYPE_ULINT;
        case SINT:              return TYPE_SINT;
        case INT:               return TYPE_INT;
        case DINT:              return TYPE_DINT;
        case LINT:              return TYPE_LINT;
        case STRING:            return TYPE_STRING;
        case WSTRING:           return TYPE_WSTRING;
        case TIME:              return TYPE_TIME;
        default:

            return TYPE_NONE;
    }
}


/*!
    \fn unsigned int iec61131_symbolTypeSize( IEC61131_DATA_TYPE eType )
    \brief Returns data storage size for IEC 61131-3 symbol type
    \param eType IEC 61131-3 symbol type
    \returns Returns data storage size for IEC 61131-3 symbol type in bytes
*/

unsigned int
iec61131_symbolTypeSize( IEC61131_TYPE eType )
{
    switch( eType )
    {
        case TYPE_BOOL:
        case TYPE_BYTE:
        case TYPE_SINT:
        case TYPE_USINT:

            return 1;

        case TYPE_INT:
        case TYPE_UINT:
        case TYPE_WORD:

            return 2;

        case TYPE_DINT:
        case TYPE_DWORD:
        case TYPE_REAL:
        case TYPE_TIME:
        case TYPE_UDINT:

            return 4;

        case TYPE_LINT:
        case TYPE_LREAL:
        case TYPE_LWORD:
        case TYPE_ULINT:

            return 8;

        case TYPE_DATE:
        case TYPE_DT:
        case TYPE_STRING:
        case TYPE_TOD:
        case TYPE_WSTRING:
        default:

            break;
    }

    return 0;
}
