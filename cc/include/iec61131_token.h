#ifndef __IEC61131_TOKEN_H
#define __IEC61131_TOKEN_H


#include <stdlib.h>
#include <stdint.h>

#include "base.h"
#include "iec61131_list.h"
#include "iec61131_symbol.h"


#define IEC61131_NEW_COLLECTION( __Id )                         ( iec61131_allocateTokenCollection( __Id ) )

#define IEC61131_NEW_INSTRUCTION( __Id )                        ( iec61131_allocateTokenInstruction( __Id ) )

#define IEC61131_NEW_LITERAL( __Id, __Name )                    ( iec61131_allocateTokenLiteral( __Id, __Name ) )

#define IEC61131_NEW_POU( __Id, __Name )                        ( iec61131_allocateTokenPOU( __Id, __Name ) )

#define IEC61131_NEW_RAW( __Id, __Data, __Length )              ( iec61131_allocateTokenRaw( __Id, __Data, __Length ) )

#define IEC61131_NEW_TOKEN( __Id )                              ( iec61131_allocateToken( __Id ) )

#define IEC61131_NEW_TYPE( __Id, __Name )                       ( iec61131_allocateTokenType( __Id, __Name ) )

#define IEC61131_NEW_VAR( __Id, __Name )                        ( iec61131_allocateTokenVar( __Id, __Name ) )


#define IEC61131_TOKEN_COLUMN( __Token, __Column )              ( ( IEC61131_TOKEN * ) __Token )->Column = ( __Column ) 

#define IEC61131_TOKEN_ID( __Token, __Id )                      ( ( ( IEC61131_TOKEN * ) __Token )->Id = ( __Id ) )

#define IEC61131_TOKEN_LINE( __Token, __Line )                  ( ( IEC61131_TOKEN * ) __Token )->Line = ( __Line ) 

#define IEC61131_TOKEN_LOCATION( __Token, __Line, __Column )    ( ( IEC61131_TOKEN * ) __Token )->Line = ( __Line ); ( ( IEC61131_TOKEN * ) __Token )->Column = ( __Column );

#define IEC61131_INSTRUCTION_LABEL( __Instr, __Label )          ( iec61131_allocateTokenLabel( __Instr, __Label ) )


typedef enum _IEC61131_VAR_FLAGS
{
    FLAGS_NONE          = 0,
    FLAGS_LITERAL      = ( 1 << 0 ),
    FLAGS_INPUT         = ( 1 << 1 ),
    FLAGS_OUTPUT        = ( 1 << 2 ),
    FLAGS_EXTERNAL      = ( 1 << 3 ),
    FLAGS_TEMP          = ( 1 << 4 ),
    FLAGS_ACCESS        = ( 1 << 5 ),
    FLAGS_GLOBAL        = ( 1 << 6 ),
    FLAGS_CONFIG        = ( 1 << 7 ),
    FLAGS_RETAIN        = ( 1 << 8 ),
    FLAGS_NON_RETAIN    = ( 1 << 9 ),
}
IEC61131_VAR_FLAGS;


/*!
    \struct IEC61131_TOKEN

    This is the primary data structure employed for the storage of token
    information parsed from IEC 61131-3 source files.  This data structure is
    intended to serve as a base from which specific token data structures are
    assumed to be derived.
*/

typedef struct _IEC61131_TOKEN
{
    /*!
        \param Base

        The first member of this data structure is a BASE data structure from which
        all complex data structures within this code base are assumed to be derived.
        This arrangement provides common means for identification and destruction
        accessors within all complex data structure types.
    */

    BASE Base;

    /*!
        \param Id

        This member of this data structure is intended to hold a unique identifier
        associated with the parsed IEC 61131-3 token.
    */

    unsigned int Id;

    /*!
        \param Line

        This member of this data structure is intended to hold the line number in
        which the parsed IEC 61131-3 token was encountered.
    */

    unsigned int Line;

    /*!
        \param Column

        This member of this data structure is intended to hold the column number
        in which the parsed IEC 61131-3 token was encountered.
    */

    unsigned int Column;

    void *Scope;
}
IEC61131_TOKEN;


/*!
    \struct IEC61131_TOKEN_COLLECTION

    This data structure is intended to permit the amalgation of a multi-element
    list into a single token data structure as may be required for fulfilling
    grammar requirements.  This data structure is of primary use where there is
    no higher level collection information necessitating the definition and
    allocation of a dedicated collection token for the storage of list elements
    (such as program, function or function block organisation unit).
*/

typedef struct _IEC61131_TOKEN_COLLECTION
{
    /*!
        \param Token

        The first member of this data structure is a IEC61131_TOKEN data structure
        from which all token related data structures within this code base are
        assumed to be derived.  This arrangement provides common means for
        identification and location accessors within all token data structures.
    */

    IEC61131_TOKEN Token;

    IEC61131_LIST *Members;
}
IEC61131_TOKEN_COLLECTION;


/*!
    \struct IEC61131_TOKEN_LITERAL

    This data structure is intended to represent literal and constant values
    which may be encountered while parsing a IEC 61131-3 source file.
*/

typedef struct _IEC61131_TOKEN_LITERAL
{
    /*!
        \param Token

        The first member of this data structure is a IEC61131_TOKEN data structure
        from which all token related data structures within this code base are
        assumed to be derived.  This arrangement provides common means for
        identification and location accessors within all token data structures.
    */

    IEC61131_TOKEN Token;

    /*!
        \param Value
    */

    IEC61131_DATA Value;

    char *Name;

    unsigned char Fixed_Type;

    unsigned char Assigned;
}
IEC61131_TOKEN_LITERAL;


/*!
    \struct IEC61131_TOKEN_INSTRUCTION
*/

typedef struct _IEC61131_TOKEN_INSTRUCTION
{
    /*!
        \param Token

        The first member of this data structure is a IEC61131_TOKEN data structure
        from which all token related data structures within this code base are
        assumed to be derived.  This arrangement provides common means for
        identification and location accessors within all token data structures.
    */

    IEC61131_TOKEN Token;

    char *Label;

    IEC61131_TOKEN *Operand;

    IEC61131_TOKEN *Branch;

    IEC61131_LIST *List;

    struct
    {
        IEC61131_TYPE Operand;
    }
    Meta;


    /*!
    	\param PC

    	This data structure member is intended to hold the program counter location
    	of the current instruction when written to byte code.  This data structure
    	member is populated during the byte code stage of IEC 61131-3 compiler
    	operations, but ultimately used in the linking stage.
    */

    unsigned int PC;

    unsigned int Target;
}
IEC61131_TOKEN_INSTRUCTION;


/*!
    \struct IEC61131_TOKEN_POU

    This data structure is intended to store information associated with a
    program organisation unit (POU) parsed from a IEC 61131-3 source file.  This
    data structure builds upon the IEC61131_TOKEN base token data structure to
    include parameters specific to POUs including name, variables and operation
    tokens parsed in the context of the POU.
*/

typedef struct _IEC61131_TOKEN_POU
{
    /*!
        \param Token

        The first member of this data structure is a IEC61131_TOKEN data structure
        from which all token related data structures within this code base are
        assumed to be derived.  This arrangement provides common means for
        identification and location accessors within all token data structures.
    */

    IEC61131_TOKEN Token;

    unsigned int Id;

    /*!
        \param Name

        This data structure member is intended to include the name of a POU parsed
        from a IEC 61131-3 source file.
    */

    char *Name;

    /*!
        \param Type

        This data structure member is intended to hold the elementary or derived type
        of the program organisation unit where a result value is explicitly returned
        from the program organisation unit code.
    */

    IEC61131_TOKEN *Type;

    IEC61131_LIST *Body;

    IEC61131_LIST *Variables;


    /*!
        \param PC

        This data structure member is intended to hold the program counter location
        of the start (or entry point) of the program organisation unit (POU) when 
        written to byte code.  This data structure is populated during the byte 
        code stage of IEC 61131-3 compiler operations, byt ultimately used in the 
        linking stage.
    */

    unsigned int PC;

    unsigned int Size;
}
IEC61131_TOKEN_POU;


typedef struct _IEC61131_TOKEN_RAW
{
    /*!
        \param Token

        The first member of this data structure is a IEC61131_TOKEN data structure
        from which all token related data structures within this code base are
        assumed to be derived.  This arrangement provides common means for
        identification and location accessors within all token data structures.
    */

    IEC61131_TOKEN Token;

    size_t Length;

    char *Data;
}
IEC61131_TOKEN_RAW;


/*!
    \struct IEC61131_TOKEN_TYPE

    This data structure is intended to represent multi-element data structures
    as defined within the IEC 61131-3 standard.
*/

typedef struct _IEC61131_TOKEN_TYPE
{
    /*!
        \param Token

        The first member of this data structure is a IEC61131_TOKEN data structure
        from which all token related data structures within this code base are
        assumed to be derived.  This arrangement provides common means for
        identification and location accessors within all token data structures.
    */

    IEC61131_TOKEN Token;

    /*!
        \param Name

        This data structure member is intended to include the name of the type
        parsed from a IEC 61131-3 source file.
    */

    char *Name;

    IEC61131_LIST *Elements;
}
IEC61131_TOKEN_TYPE;


/*!
    \struct IEC61131_TOKEN_VARIABLE
*/

typedef struct _IEC61131_TOKEN_VARIABLE
{
    /*!
        \param Token

        The first member of this data structure is a IEC61131_TOKEN data structure
        from which all token related data structures within this code base are
        assumed to be derived.  This arrangement provides common means for
        identification and location accessors within all token data structures.
    */

    IEC61131_TOKEN Token;

    /*!
        \param Name

        This data structure member is intended to include the name of a variable
        parsed from a IEC 61131-3 source file.
    */

    char *Name;

    IEC61131_VAR_FLAGS Flags;

    IEC61131_TOKEN_LITERAL *Type;

    IEC61131_TOKEN_LITERAL *Location;
}
IEC61131_TOKEN_VARIABLE;


IEC61131_TOKEN * iec61131_allocateToken( unsigned int Id );

IEC61131_TOKEN_COLLECTION * iec61131_allocateTokenCollection( unsigned int Id );

IEC61131_TOKEN_LITERAL * iec61131_allocateTokenLiteral( unsigned int Id, char *Name );

IEC61131_TOKEN_INSTRUCTION * iec61131_allocateTokenInstruction( unsigned int Id );

void iec61131_allocateTokenLabel( IEC61131_TOKEN_INSTRUCTION *Instr, const char *Label );

IEC61131_TOKEN_POU * iec61131_allocateTokenPOU( unsigned int Id, char *Name );

IEC61131_TOKEN_RAW * iec61131_allocateTokenRaw( unsigned int Id, char *Data, size_t Length );

IEC61131_TOKEN_TYPE * iec61131_allocateTokenType( unsigned int Id, char *Name );

IEC61131_TOKEN_VARIABLE * iec61131_allocateTokenVar( unsigned int Id, char *Name );


#endif
