#ifndef __IEC61131_PARSE_H
#define __IEC61131_PARSE_H


#include "iec61131_token.h"
#include "iec61131_types.h"


int iec61131_castBitstring( IEC61131_TOKEN *Value, IEC61131_TYPE Type );

int iec61131_castInteger( IEC61131_TOKEN *Value, IEC61131_TYPE Type );

int iec61131_castReal( IEC61131_TOKEN *Value, IEC61131_TYPE Type );

IEC61131_TOKEN * iec61131_parseBitstring( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column, unsigned int Base );

IEC61131_TOKEN * iec61131_parseBoolean( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column );

IEC61131_TOKEN * iec61131_parseDuration( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column );

IEC61131_TOKEN * iec61131_parseInteger( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column, unsigned int Base );

IEC61131_TOKEN * iec61131_parseReal( IEC61131_TOKEN *Token, unsigned int Line, unsigned int Column );


#endif
