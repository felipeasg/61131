#ifndef __STAGE_H
#define __STAGE_H


#include "iec61131_cc.h"


/*
	This header file is intended to define the various stage functions that are
	executed in a stepwise fashion in order to parse, compile and output byte
	code for the IEC 61131-3 virtual machine defined within this code base for
	a given Instruction List (IL) source file.
*/

int stage_bytecode( IEC61131_CC *Context );

int stage_format( IEC61131_CC *Context );

int stage_functionblock( IEC61131_CC *Context );

int stage_link( IEC61131_CC *Context );

int stage_symboltable( IEC61131_CC *Context );

int stage_tables( IEC61131_CC *Context );

int stage_token( IEC61131_CC *Context, int Argc, char **Argv );

int stage_type( IEC61131_CC *Context );


#endif
