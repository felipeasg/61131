#ifndef __HEX_H
#define __HEX_H


#define HEX_BINARY_OFFSET_RECLEN		(0x00)
#define HEX_BINARY_OFFSET_LOAD			(0x01)
#define HEX_BINARY_OFFSET_RECTYP		(0x03)
#define HEX_BINARY_OFFSET_DATA			(0x04)
                                        
#define HEX_RECTYP_DATA			        (0x00)
#define HEX_RECTYP_EOFR			        (0x01)
#define HEX_RECTYP_ESAR			        (0x02)
#define HEX_RECTYP_SSAR			        (0x03)
#define HEX_RECTYP_ELAR			        (0x04)
#define HEX_RECTYP_SLAR			        (0x05)


/*!
	\fn int hex_writeFile( const char * Filename, char * Pointer, unsigned int Length )
	\brief Writes an Intel hexadecimal object file for supplied binary data
    \param Filename Filename of Intel hexadecimal object file to be created
    \param Pointer Pointer to memory block of binary data
    \param Length
    \returns Returns zero on success, non-zero on failure
*/

int hex_writeFile( const char * Filename, char * Pointer, unsigned int Length );


#endif /* __HEX_H */
