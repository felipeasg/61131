#ifndef __IEC61131_SYMBOL_H
#define __IEC61131_SYMBOL_H


#include <string.h>
#include <stdint.h>

#include "base.h"
#include "iec61131_types.h"


#define IEC61131_SYMBOL_NAME_MAX        (128)


#define IEC61131_NEW_SYMBOL( __Type, __Name )           ( iec61131_allocateSymbol( __Type, __Name ) )


/*!
    \struct IEC61131_SYMBOL

    This is the primary data structure employed for the storage of value and
    meta-data associated with symbols parsed from IEC 61131-3 source files.
*/

typedef struct _IEC61131_SYMBOL
{
    /*!
        \param Base

        The first member of this data structure is a IEC61131_BASE data structure
        from which all complex data structures within this code base are  assumed to
        be derived.  This arrangement provides common means for identification and
        destruction accessors within all complex data structure types.
    */

    BASE Base;

    /*!
        \param Id

        This member of this data structure is intended to hold a unique identifier
        associated with a IEC 61131-3 symbol.
    */

    unsigned int Id;

    /*!
        \param Name

        This member of this data structure is intended to hold the name of a
        IEC 61131-3 symbol with a maximum length of 128 characters.
    */

    char Name[ IEC61131_SYMBOL_NAME_MAX ];

    /*!
        \param Direct

        This member of this data structure is intended to indicate whether the 
        symbol is directly represented as described within the IEC 61131-3 
        standard.  In turn the memory location offset stored within the Offset 
        member of this data structure will relate to different memory blocks, 
        depending upon the value of this data structure member.
    */

    uint32_t Direct;

    uint32_t Index;

    uint32_t Offset;

    /*!
        \param Size

        This member of this data structure is intended to hold the memory size
        required to store the value associated with a IEC 61131-3 symbol.  It should
        be noted that this is expected to be maximum size permitted and may include
        allocated length otherwise not used in variable length data types.
    */

    uint32_t Size;

    /*!
        \param Scope
    */

    void * Scope;

    void * Type;

    char * Location;

    /*!
        \param Data

        This member of this data structure is intended to hold type, value and size
        information associated with a IEC 61131-3 symbol.
    */

    IEC61131_DATA Data;
}
IEC61131_SYMBOL;


IEC61131_SYMBOL * iec61131_allocateSymbol( IEC61131_TYPE Type, char *Name );

IEC61131_TYPE iec61131_symbolTypeMapping( int Id );

unsigned int iec61131_symbolTypeSize( IEC61131_TYPE Type );


#endif
