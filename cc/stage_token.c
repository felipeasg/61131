#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "iec61131_cc.h"
#include "stage.h"

#include "iec61131.h"


extern int yydebug;
extern FILE * yyin;
extern int yyparse( IEC61131_CC *pContext );


/*!
    \fn int stage_token( IEC61131_CC *pContext, int nArgc, char **pArgv )
    \brief Perform parse and tokenisation of IEC 61131-3 source files
    \param pContext Pointer to IEC 61131-3 compiler context data structure
    \param nArgc Number of command-line arguments
    \param pArgv Array of character string pointers holding command-line arguments
    \returns Returns zero on success, non-zero on error

	This stage of the IEC 61131-3 compilation process involves the tokenisation
	of the IEC 61131-3 Instruction List (IL) source file.
*/

int
stage_token( IEC61131_CC *pContext, int nArgc, char **pArgv )
{
    FILE *pFile;
    int nIndex, nResult;


    for( nIndex = 0, nResult = 0; nIndex < nArgc; ++nIndex )
    {
        if( ( pFile = fopen( pArgv[ nIndex ], "r" ) ) == NULL )
        {
            printf( "%s: %s: %s\n",
                    pContext->Name,
                    pArgv[ nIndex ],
                    strerror( errno ) );

            nResult = -errno;
            continue;
        }
        pContext->Source = pArgv[ nIndex ];

        yydebug = pContext->Debug;
        yyin = pFile;
        if( ( nResult = yyparse( pContext ) ) != 0 )
        {
        }

        fclose( pFile );


        /*
            The following is a deliberate hack in order to ensure only a single source
            file is processed for compilation presently.
        */

        break;
    }

    return nResult;
}
