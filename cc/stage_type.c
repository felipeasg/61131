#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>

#include "iec61131_cc.h"
#include "iec61131_error.h"
#include "iec61131_external.h"
#include "iec61131_list.h"
#include "iec61131_parse.h"
#include "iec61131_token.h"
#include "stage.h"
#include "symbol.h"

#include "iec61131.h"


static IEC61131_TYPE _stage_evaluateInstructionList( IEC61131_CC *pContext, IEC61131_LIST *pList );

static IEC61131_TYPE _stage_getDirectWidth( IEC61131_CC *pContext, char *pName );

static const char * _stage_getTypeName( IEC61131_TYPE eType );

static int _stage_setOperandType( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr, IEC61131_TYPE eType );



/*
    \fn static IEC61131_TYPE _stage_evaluateInstructionList( IEC61131_CC *pContext, IEC61131_LIST *pList )
    \brief Evaluate a list of IEC 61131-3 byte code instructions and return Current Result register type
    \param pContext IEC 61131-3 contextual data structure
    \param pList List of IEC 61131-3 byte code instructions
*/

static IEC61131_TYPE 
_stage_evaluateInstructionList( IEC61131_CC *pContext, IEC61131_LIST *pList )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN_INSTRUCTION *pInstr;
    IEC61131_TYPE eType;
    int nResult;


    nResult = 0;
    eType = TYPE_NONE;

    for( pElement = IEC61131_LIST_HEAD( pList );
            ( ( pElement != NULL ) &&
                    ( nResult == 0 ) );
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement->Data;
        pInstr->Meta.Operand = TYPE_NONE;

        switch( pInstr->Token.Id )
        {
            case LD:
            case LDN:

                nResult = _stage_setOperandType( pContext, pInstr, TYPE_NONE );
                eType = pInstr->Meta.Operand;
                break;

            case ST:
            case STN:
            case S:
            case R:
            case NOT:

                nResult = _stage_setOperandType( pContext, pInstr, eType );
                break;

            case AND:
            case ANDN:
            case OR:
            case ORN:
            case XOR:
            case XORN:
            case ADD:
            case SUB:
            case MUL:
            case DIV:
            case MOD:

                /*
                    The following code is intended to test for any deferred execution that may 
                    be associated withe current instruction and in turn evaluate such code 
                    prior to performing type checking on the immediate operation.  It should be 
                    noted that this is performed through a recursive call this this function, 
                    but no limits have yet been defined or implemented on recursion depth!
                */

                if( ( pInstr->List != NULL ) &&
                        ( IEC61131_LIST_SIZE( pInstr->List ) > 0 ) )
                {
                    pInstr->Meta.Operand = _stage_evaluateInstructionList( pContext, pInstr->List );
                }

                nResult = _stage_setOperandType( pContext, pInstr, eType );
                break;

            case GT:
            case GE:
            case EQ:
            case NE:
            case LE:
            case LT:

                eType = TYPE_BOOL;
                break;

            case CAL:
            case CALC:
            case CALCN:
            case JMP:
            case JMPC:
            case JMPCN:
            case RET:
            case RETC:
            case RETCN:
            default:

                break;
        }
    }

    return eType;
}


static IEC61131_TYPE 
_stage_getDirectWidth( IEC61131_CC *pContext, char *pName )
{
    BASE_UNUSED( pContext );


    /*
        direct_variable ::= '%' location_prefix size_prefix integer {'.' integer}

        location_prefix ::= 'I' | 'Q' | 'M'

        size_prefix ::= NIL | 'X' | 'B' | 'W' | 'D' | 'L'
    */

    if( pName == NULL )
    {
        return TYPE_NONE;
    }
    
    switch( pName[2] )
    {
        case 'X':       return TYPE_BOOL;
        case 'B':       return TYPE_BYTE;
        case 'W':       return TYPE_WORD;
        case 'D':       return TYPE_DWORD;
        case 'L':       return TYPE_LWORD;
        default:

            return TYPE_NONE;
    }
}


static const char * 
_stage_getTypeName( IEC61131_TYPE eType )
{
    switch( eType )
    {
        case TYPE_BOOL:     return "BOOL";
        case TYPE_SINT:     return "SINT";
        case TYPE_INT:      return "INT";
        case TYPE_DINT:     return "DINT";
        case TYPE_LINT:     return "LINT";
        case TYPE_USINT:    return "USINT";
        case TYPE_UINT:     return "UINT";
        case TYPE_UDINT:    return "UDINT";
        case TYPE_ULINT:    return "ULINT";
        case TYPE_REAL:     return "REAL";
        case TYPE_LREAL:    return "LREAL";
        case TYPE_TIME:     return "TIME";
        case TYPE_DATE:     return "DATE";
        case TYPE_TOD:      return "TOD";
        case TYPE_DT:       return "DT";
        case TYPE_STRING:   return "STRING";
        case TYPE_BYTE:     return "BYTE";
        case TYPE_WORD:     return "WORD";
        case TYPE_DWORD:    return "DWORD";
        case TYPE_LWORD:    return "LWORD";
        case TYPE_WSTRING:  return "WSTRING";
        default:

            return "UNKNOWN";
    }
}


static int
_stage_setOperandType( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr, IEC61131_TYPE eType )
{
    IEC61131_TOKEN *pToken;
    IEC61131_TOKEN_LITERAL *pLiteral;


    pToken = ( IEC61131_TOKEN * ) pInstr->Operand;
    if( pToken != NULL )
    {
        switch( pToken->Id )
        {
            case boolean:
            case duration:

                pLiteral = ( IEC61131_TOKEN_LITERAL * ) pInstr->Operand;
                pInstr->Meta.Operand = pLiteral->Value.Type;

                return 0;

            case real:

                if( eType != TYPE_NONE )
                {
                    if( iec61131_castReal( pInstr->Operand, eType ) )
                    {
                        printf( "%s:%u:%u: error: Cannot cast real value to %s type\n",
                                pContext->Source,
                                pToken->Line,
                                pToken->Column,
                                _stage_getTypeName( eType ) );
                        return -EINVAL;
                    }
                }

                pLiteral = ( IEC61131_TOKEN_LITERAL * ) pInstr->Operand;
                pInstr->Meta.Operand = pLiteral->Value.Type;

                return 0;

            case bit_string:
            case integer:

                if( eType != TYPE_NONE )
                {
                    if( iec61131_castInteger( pInstr->Operand, eType ) )
                    {
                        printf( "%s:%u:%u: error: Cannot cast integer value to %s type\n",
                                pContext->Source,
                                pToken->Line,
                                pToken->Column,
                                _stage_getTypeName( eType ) );
                        return -EINVAL;
                    }
                }

                pLiteral = ( IEC61131_TOKEN_LITERAL * ) pInstr->Operand;
                pInstr->Meta.Operand = pLiteral->Value.Type;

                return 0;

            case direct_variable:

                if( ( pInstr->Meta.Operand = _stage_getDirectWidth(
                                pContext,
                                ( ( IEC61131_TOKEN_RAW * ) pToken )->Data 
                        ) ) == TYPE_NONE )
                {
                    printf( "%s:%u:%u: error: Cannot determine direct variable bit width\n",
                            pContext->Source,
                            pToken->Line,
                            pToken->Column );
                    return -ENOENT;
                }
                break;

            case identifier:

                if( ( pInstr->Meta.Operand = symbol_getSymbolType(
                                pContext,
                                ( ( IEC61131_TOKEN_RAW * ) pToken )->Data,
                                ( ( IEC61131_TOKEN * ) pInstr )->Scope
                        ) ) == TYPE_NONE )
                {
                    printf( "%s:%u:%u: error: Cannot determine target operand type\n",
                            pContext->Source,
                            pToken->Line,
                            pToken->Column );
                    return -ENOENT;
                }
                break;

            default:

printf( "[%s] Unhandled operand type {%u}\n",
        __func__,
        pToken->Id );
                break;
        }
    }

    if( ( eType != TYPE_NONE ) &&
            ( eType != pInstr->Meta.Operand ) )
    {
        printf( "%s:%u:%u: error: Mismatch between current and target operand type\n",
                pContext->Source,
                pToken->Line,
                pToken->Column );
        return -EINVAL;
    }

    return 0;
}


/*!
	\fn int stage_type( IEC61131_CC *pContext )
	\brief Perform type checking for IEC 61131-3 operations
	\param pContext Pointer to IEC 61131-3 compiler contextual data structure
	\returns Returns 0 on success, non-zero on error

    This stage of IEC 61131-3 compiler operations steps through the parsed byte 
    code tokens and verifies consistency in the types of operands between 
    operators as appropriate.  In addition to this, this stage will also 
    perform cast typing of literal value representations to the appropriate 
    data type for operator actions.
*/

int
stage_type( IEC61131_CC *pContext )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN_POU *pPOU;


    for( pElement = IEC61131_LIST_HEAD( &pContext->Library );
            pElement != NULL;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement->Data;

        _stage_evaluateInstructionList( pContext, pPOU->Body );
    }

    return 0;
}

