#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "iec61131_bytecode.h"
#include "iec61131_cc.h"
#include "iec61131_external.h"
#include "stage.h"

#include "iec61131.h"


static int _stage_compareSymbols( IEC61131_SYMBOL *pSymbol1, IEC61131_SYMBOL *pSymbol2 );

static int _stage_getDirectLocation( char *pDefinition );

static int _stage_getDirectType( IEC61131_TOKEN_LITERAL *pLocation );

static int _stage_insertLabelTable( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static int _stage_insertSymbolTable( IEC61131_CC *pContext, IEC61131_TOKEN_VARIABLE *pVariable );


static int
_stage_compareSymbols( IEC61131_SYMBOL *pSymbol1, IEC61131_SYMBOL *pSymbol2 )
{
    if( pSymbol1->Scope == pSymbol2->Scope )
    {
        return strcmp( pSymbol1->Name, pSymbol2->Name );
    }

    return ( pSymbol1->Scope < pSymbol2->Scope ) ? -1 : 1;
}


static int
_stage_getDirectLocation( char *pDefinition )
{
    unsigned int uSize, uValue;
    char *pChar;


    for( pChar = pDefinition;; )
    {
        if( pChar == NULL )
        {
            return 0;
        }

        switch( *pChar )
        {
            case 'I':
            case 'M':
            case 'Q':

                break;

            case '%':

                if( pChar == pDefinition )
                {
                    ++pChar;
                    continue;
                }
                /* break; */

            default:

                return 0;
        }

        ++pChar;
        if( *pChar == '\0' )
        {
            return 0;
        }

        break;
    }

    uSize = 1;
    switch( *pChar )
    {
        case '*':

            return 0;

        case 'X':
        case 'B':
        case 'W':
        case 'D':
        case 'L':

            switch( *pChar )
            {
                case 'X':   uSize = 1; break;
                case 'B':   uSize = 8; break;
                case 'W':   uSize = 16; break;
                case 'D':   uSize = 32; break;
                case 'L':   uSize = 64; break;
            }

            ++pChar;
            if( *pChar == '\0' )
            {
                return uSize;
            }
            break;

        default:

            break;
    }

    uValue = strtoul( pChar, NULL, 10 );
    return ( uSize * uValue );
}


static int
_stage_getDirectType( IEC61131_TOKEN_LITERAL *pLocation )
{
    IEC61131_TOKEN_RAW *pRaw;
    char *pChar;


    if( pLocation == NULL )
    {
        return OPERAND_DIRECT_NONE;
    }

    pRaw = ( IEC61131_TOKEN_RAW * ) pLocation;
    for( pChar = pRaw->Data;; )
    {
        switch( *pChar )
        {
            case 'I':   return OPERAND_DIRECT_INPUT;
            case 'M':   return OPERAND_DIRECT_MEMORY;
            case 'Q':   return OPERAND_DIRECT_OUTPUT;
            case '%':

                if( pChar == pRaw->Data )
                {
                    ++pChar;
                    continue;
                }
                /* break; */

            default:

                return OPERAND_DIRECT_NONE;
        }
    }
}


static int 
_stage_insertLabelTable( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    if( ( pContext->LabelTable.Entries = ( IEC61131_TOKEN_INSTRUCTION ** ) realloc(
            pContext->LabelTable.Entries,
            ( sizeof( IEC61131_TOKEN_INSTRUCTION * ) * ( pContext->LabelTable.Count + 1 ) ) ) ) == NULL )
    {
        return -errno;
    }
    pContext->LabelTable.Entries[ pContext->LabelTable.Count++ ] = pInstr;

    return 0;
}


/*!
    \fn static int _stage_insertSymbolTable( IEC61131_CC *pContext, IEC61131_TOKEN_VARIABLE *pVariable )
    \brief Perform an insertion sort for IEC 61131-3 symbol within library symbol table
    \param pContext Pointer to IEC 61131-3 compiler context data structure
    \param pVariable Pointer to IEC 61131-3 variable token data structure
    \returns Returns number of bytes associated with symbol storage on success, negative value on error
*/

static int
_stage_insertSymbolTable( IEC61131_CC *pContext, IEC61131_TOKEN_VARIABLE *pVariable )
{
    EXTERNAL_FUNCTION_BLOCK_DECLARATION *pDeclaration;
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_SYMBOL *pSymbol, stSymbol;
    IEC61131_TOKEN *pType;
    IEC61131_TOKEN_POU *pPOU;
    IEC61131_TOKEN_RAW *pLocation;
    unsigned int uIndex;
    int nIndex1, nIndex2;


    /*
        Dynamically reallocate memory for the new symbol within the symbol table
        and perform an insertion sort for the new entry based on symbol scope and
        name.
    */

    if( ( pContext->SymbolTable.Entries = ( IEC61131_SYMBOL * ) realloc(
            pContext->SymbolTable.Entries,
            ( sizeof( IEC61131_SYMBOL ) * ( pContext->SymbolTable.Count + 1 ) ) ) ) == NULL )
    {
        return -errno;
    }

    pType = ( IEC61131_TOKEN * ) pVariable->Type;

    pSymbol = &pContext->SymbolTable.Entries[ pContext->SymbolTable.Count++ ];
    ( void ) memset( pSymbol, 0, sizeof( IEC61131_SYMBOL ) );
    BASE_SET_TYPE( pSymbol, TYPE_SYMBOL );

    ( void ) strncpy( pSymbol->Name, pVariable->Name, sizeof( pSymbol->Name ) );
    pSymbol->Scope = ( ( IEC61131_TOKEN * ) pVariable )->Scope;
    pSymbol->Direct = _stage_getDirectType( pVariable->Location );
    pSymbol->Type = pVariable->Type;
    pSymbol->Data.Type = iec61131_symbolTypeMapping( pType->Id );
    if( pSymbol->Data.Type == TYPE_NONE )
    {
        /*
            At this point, the type assigned to the new symbol does not appear to 
            correlate with the elemental symbol types defined within the IEC 61131-3 
            standard.  As such, this code must now perform a look-up of both derived 
            symbol types and function blocks to determine symbol size.  At this point
            however, there is no support for derived symbol types and as such look-up
            is performed against the list of function blocks only.

            It should also be noted that the order of name checking implemented below 
            purposely allows user-defined function blocks (in IEC 61131-3 languages) to 
            override built-in function blocks.
        */

        /* assert( pVariable->Type->Name != NULL ); */
        pSymbol->Size = 0;

        //  IEC 61131-3 language function blocks

        for( pElement = IEC61131_LIST_HEAD( &pContext->Library );
                pElement;
                pElement = IEC61131_LIST_NEXT( pElement ) )
        {
            pPOU = ( IEC61131_TOKEN_POU * ) pElement->Data;
            if( strcasecmp( pVariable->Type->Name, pPOU->Name ) == 0 )
            {
                pSymbol->Size = pPOU->Size;
                break;
            }
        }

        //  C library function blocks

        if( pSymbol->Size == 0 )
        {
            for( uIndex = 0; uIndex < IEC61131_FUNCTION_BLOCK_COUNT; ++uIndex )
            {
                pDeclaration = ( EXTERNAL_FUNCTION_BLOCK_DECLARATION * ) &IEC61131_FUNCTION_BLOCK[ uIndex ];
                if( ( pDeclaration->Name == NULL ) ||
                        ( pDeclaration->Size == 0 ) ||
                        ( pDeclaration->Parameter == NULL ) ||
                        ( pDeclaration->Pointer == NULL ) )
                {
                    continue;
                }
                if( strcasecmp( pDeclaration->Name, pVariable->Type->Name ) != 0 )
                {
                    continue;
                }

                pSymbol->Size = pDeclaration->Size;
                pSymbol->Index = uIndex;

                break;
            }
        }
    }
    else
    {
        /* pSymbol->Data.Length = iec61131_symbolTypeSize( pSymbol->Data.Type ); */
        pSymbol->Size = iec61131_symbolTypeSize( pSymbol->Data.Type );
    }

    switch( pSymbol->Direct )
    {
        case OPERAND_DIRECT_INPUT:
        case OPERAND_DIRECT_MEMORY:
        case OPERAND_DIRECT_OUTPUT:

            pLocation = ( IEC61131_TOKEN_RAW * ) pVariable->Location;
            pSymbol->Location = pLocation->Data;
            pSymbol->Offset = _stage_getDirectLocation( pLocation->Data );
            break;

        case OPERAND_DIRECT_NONE:
        default:

            break;
    }

    if( pVariable->Type->Assigned )
    {
        switch( pSymbol->Data.Type )
        {  
            case TYPE_BOOL:
            case TYPE_SINT:
            case TYPE_USINT:
            case TYPE_BYTE:

                pSymbol->Data.Value.B8 = pVariable->Type->Value.Value.B8;
                break;

            case TYPE_INT:
            case TYPE_UINT:
            case TYPE_WORD:

                pSymbol->Data.Value.B16 = pVariable->Type->Value.Value.B16;
                break;

            case TYPE_DINT:
            case TYPE_UDINT:
            case TYPE_DWORD:
            case TYPE_REAL:

                pSymbol->Data.Value.B32 = pVariable->Type->Value.Value.B32;
                break;

            case TYPE_LINT:
            case TYPE_ULINT:
            case TYPE_LWORD:
            case TYPE_LREAL:

                pSymbol->Data.Value.B32 = pVariable->Type->Value.Value.B64;
                break;

            case TYPE_TIME:
            case TYPE_DATE:
            case TYPE_TOD:
            case TYPE_DT:
            case TYPE_STRING:
            case TYPE_WSTRING:
            default:

                break;
        }
    }

    for( nIndex1 = 1; nIndex1 < ( int ) pContext->SymbolTable.Count; ++nIndex1 )
    {
        ( void ) memcpy( &stSymbol, &pContext->SymbolTable.Entries[ nIndex1 ], sizeof( IEC61131_SYMBOL ) );

        nIndex2 = nIndex1 - 1;
        while( ( nIndex2 >= 0 ) &&
                ( _stage_compareSymbols( &pContext->SymbolTable.Entries[ nIndex2 ], &stSymbol ) > 0 ) )
        {
            ( void ) memcpy( &pContext->SymbolTable.Entries[ nIndex2 + 1 ], 
                    &pContext->SymbolTable.Entries[ nIndex2 ],
                    sizeof( IEC61131_SYMBOL ) );

            --nIndex2;
        }
        ( void ) memcpy( &pContext->SymbolTable.Entries[ nIndex2 + 1 ],
                &stSymbol,
                sizeof( IEC61131_SYMBOL ) );
    }

    return 0 /* pSymbol->Size */;
}


/*!
    \fn int stage_tables( IEC61131_CC *pContext )

    This stage of the IEC 61131-3 compiler operations involves the construction 
    of a series of data tables to accommodate subsequent look up for operations.
    These data tables include information such as symbol information, branching
    and jumping instructions, data labels and program organisation unit (POU) 
    call information.
    
    It should be noted that this stage was originally specific to symbol table 
    population, but has been expanded to include these other elements in order 
    to expedite linking operations after the program byte code has been 
    generated.
*/

int
stage_tables( IEC61131_CC *pContext )
{
    IEC61131_LIST_ELEMENT *pElement1, *pElement2;
    IEC61131_SYMBOL *pSymbol;
    IEC61131_TOKEN_INSTRUCTION *pInstr;
    IEC61131_TOKEN_LITERAL /* *pLiteral, */ stValue;
    IEC61131_TOKEN_POU *pPOU;
    IEC61131_TOKEN_VARIABLE *pVariable, stVariable;
    unsigned int uId, /* uSize, */ uSymbol, uType;
    unsigned char *pData;
    int nResult;


    /*
        Step through each program organisation unit (POU) defined within the 
        library and generate data table entries for each POU and each symbol and 
        branch or jump label encountered.
    */

    uId = 0;

    for( pElement1 = IEC61131_LIST_HEAD( &pContext->Library );
            pElement1;
            pElement1 = IEC61131_LIST_NEXT( pElement1 ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement1->Data;
        pPOU->Id = uId++;

        uType = ( ( IEC61131_TOKEN * ) pPOU )->Id;
        if( ( uType != PROGRAM ) &&
                ( uType != FUNCTION ) )
        {
//            continue;
        }

        /* if( ( ( IEC61131_TOKEN * ) pPOU )->Id == FUNCTION ) */
        if( pPOU->Type != NULL )
        {
            /*
                If the POU has an explicit type defined, which is indicative of a 
                function POU type, a pseudo-entry is generated in the symbol table, with a
                scope identical to the POU itself, to allow for the assignment of return
                values.
            */

            /* assert( ( ( IEC61131_TOKEN * ) pPOU )->Id == FUNCTION ); */
            /* assert( pPOU->Type != NULL ); */
            ( void ) memset( &stValue, 0, sizeof( stValue ) );
            stValue.Assigned = BOOL_FALSE;
            ( ( IEC61131_TOKEN * ) &stValue )->Id = pPOU->Type->Id;

            ( void ) memset( &stVariable, 0, sizeof( stVariable ) );
            stVariable.Name = pPOU->Name;
            stVariable.Type = &stValue;
            ( ( IEC61131_TOKEN * ) &stVariable )->Scope = pPOU;

            if( ( nResult = _stage_insertSymbolTable( pContext, &stVariable ) ) < 0 )
            {
                return nResult;
            }
        }

        for( pElement2 = IEC61131_LIST_HEAD( pPOU->Variables );
                pElement2;
                pElement2 = IEC61131_LIST_NEXT( pElement2 ) )
        {
            pVariable = ( IEC61131_TOKEN_VARIABLE * ) pElement2->Data;
            if( ( nResult = _stage_insertSymbolTable( pContext, pVariable ) ) < 0 )
            {
                return nResult;
            }
        }

        for( pElement2 = IEC61131_LIST_HEAD( pPOU->Body );
                pElement2;
                pElement2 = IEC61131_LIST_NEXT( pElement2 ) )
        {
            pInstr = ( IEC61131_TOKEN_INSTRUCTION * ) pElement2->Data;

            if( pInstr->Label != NULL )
            {
                if( ( nResult = _stage_insertLabelTable( pContext, pInstr ) ) != 0 )
                {
                    return nResult;
                }
            }
        }
    }


    /*
        Following the insertion sort of all symbols defined within a library, the
        symbol table is iterated through and each symbol is assigned an ascending
        unique identifier (matching the array index of the symbol).  In addition to
        this, the aligned block of memory for storage of symbol values is allocated,
        offset pointer assigned to the corresponding symbol data structure and
        initial values copied from the symbol definitions into this aligned memory
        block.

        The intent of this operation is such that following compilation and linking
        there is no requirement to iterate through or make reference to the symbol
        table definition.  Instead all operand operations can be performed directly,
        in an aligned fashion, on the allocated symbol table memory block.

        It should noted that this operation includes an exception for:
        
        - Directly represented symbols (which do not require explicit memory 
            location within the symbol table as their representation is manifest
            within the underlying I/O interface); or

        - Function block variables (as their memory allocation within the symbol
            table is tied to the instantiation of the function block).
    */

    uId = 0;

    for( uSymbol = 0, pContext->SymbolTable.Offset = 0;
            uSymbol < pContext->SymbolTable.Count;
            ++uSymbol )
    {
        pSymbol = ( IEC61131_SYMBOL * ) &pContext->SymbolTable.Entries[ uSymbol ];
        pPOU = ( IEC61131_TOKEN_POU * ) pSymbol->Scope;
        uType = ( ( IEC61131_TOKEN * ) pPOU )->Id;
        if( uType == FUNCTION_BLOCK )
        {
            continue;
        }

        switch( pSymbol->Direct )
        {
            case OPERAND_DIRECT_INPUT:
            case OPERAND_DIRECT_MEMORY:
            case OPERAND_DIRECT_OUTPUT:

                continue;
                /* break; */

            case OPERAND_DIRECT_NONE:
            default:

                pSymbol->Offset = pContext->SymbolTable.Offset;
                break;
        }

        pSymbol->Id = uId++;

        pContext->SymbolTable.Offset += ( ( pSymbol->Size / pContext->Align ) * pContext->Align ) +
                ( ( ( pSymbol->Size % pContext->Align ) > 0 ) ?
                        pContext->Align :
                        0 );

        if( ( pContext->SymbolTable.Data = ( unsigned char * ) realloc( pContext->SymbolTable.Data, pContext->SymbolTable.Offset ) ) == NULL )
        {
            return -errno;
        }

        pData = NULL;

        switch( pSymbol->Data.Type )
        {
            case TYPE_BOOL:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.B1 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.B1;
                break;

            case TYPE_SINT:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.S8 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.S8;
                break;

            case TYPE_USINT:
            case TYPE_BYTE:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.U8 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.U8;
                break;

            case TYPE_INT:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.S16 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.S16;
                break;

            case TYPE_UINT:
            case TYPE_WORD:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.U16 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.U16;
                break;

            case TYPE_DINT:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.S32 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.S32;
                break;

            case TYPE_DWORD:
            case TYPE_UDINT:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.U32 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.U32;
                break;

            case TYPE_LINT:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.S64 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.S64;
                break;

            case TYPE_LWORD:
            case TYPE_ULINT:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.U64 ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.U64;
                break;

            case TYPE_REAL:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.Float ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.Float;
                break;

            case TYPE_LREAL:

                /* assert( pSymbol->Size == sizeof( pSymbol->Data.Value.Double ) ); */
                pData = ( unsigned char * ) &pSymbol->Data.Value.Double;
                break;

            case TYPE_DATE:
            case TYPE_TOD:
            case TYPE_DT:
            case TYPE_STRING:
            case TYPE_WSTRING:
            case TYPE_TIME:
            default:

                break;
        }

        if( pData != NULL )
        {
            ( void ) memcpy( &pContext->SymbolTable.Data[ pSymbol->Offset ],
                    pData,
                    pSymbol->Size );
        }
    }

    return 0;
}
