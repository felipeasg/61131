#include <stdio.h>
#include <errno.h>
#include <arpa/inet.h>

#include "iec61131_bytecode.h"
#include "iec61131_external.h"
#include "iec61131_symbol.h"
#include "iec61131_token.h"
#include "stage.h"
#include "symbol.h"


static int _stage_linkExternalLibrary( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static int _stage_linkFunctionBlock( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static int _stage_linkLabel( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );

static int _stage_linkPOU( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr );


/*!
    \fn static int _stage_linkExternalLibrary( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
    \brief Perform linking for IEC 61131-3 functions implemented in standard C language library
    \param pContext Pointer to IEC 61131-3 compilation contextual data structure
    \param pInstr Pointer to IEC 61131-3 instruction token data structure
    \returns Returns zero on success, non-zero on error

    This function is intended to perform linking for IEC 61131-3 functions 
    and function blocks implemented in the external C language library.  All 
    functions and function blocks implemented in this manner must be defined in 
    the lib/function.c and lib/functionblock.c files of the project source 
    which is in turn shared between both the IEC 61131-3 compiler (to allow
    specification of the appropriate index to the implementation function in
    byte code) and the virtual machine run-time (for subsequent function 
    execution).
*/

static int 
_stage_linkExternalLibrary( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    EXTERNAL_FUNCTION_DECLARATION *pDeclaration;
    IEC61131_SYMBOL *pSymbol;
    IEC61131_TOKEN_POU *pPOU;
    unsigned int uAddress, uIndex, uTarget;
    char *pName, sByte;


    /*
        The following code iterates through the list of C-based functions defined 
        within the external library.
    */

    pName = ( ( IEC61131_TOKEN_RAW * ) pInstr->Branch )->Data;
    pPOU = ( ( IEC61131_TOKEN * ) pInstr )->Scope;

    for( uIndex = 0; uIndex < IEC61131_FUNCTION_COUNT; ++uIndex )
    {
        pDeclaration = ( EXTERNAL_FUNCTION_DECLARATION * ) &IEC61131_FUNCTION[ uIndex ];
        if( ( pDeclaration->Name == NULL ) ||
                ( pDeclaration->Pointer == NULL ) )
        {
            continue;
        }

        if( strcasecmp( pName, pDeclaration->Name ) == 0 )
        {
            uAddress = pInstr->PC + 4;

            uTarget = htonl( IEC61131_BYTECODE_FUNCTION | uIndex );
            sByte = ( ( uTarget & 0xff000000 ) >> 24 );
            memblock_write( &pContext->ByteCode, uAddress + 0, &sByte, 1 );
            sByte = ( ( uTarget & 0x00ff0000 ) >> 16 );
            memblock_write( &pContext->ByteCode, uAddress + 1, &sByte, 1 );
            sByte = ( ( uTarget & 0x0000ff00 ) >> 8 );
            memblock_write( &pContext->ByteCode, uAddress + 2, &sByte, 1 );
            sByte = ( uTarget & 0x000000ff );
            memblock_write( &pContext->ByteCode, uAddress + 3, &sByte, 1 );

            return 0;
        }
    }


    /*
        The following code iterates through the list of C-based function blocks 
        defined within the external library.  It should be noted that this code 
        differs from above due to the additional requirement of resolving the 
        function block symbol instance to the corresponding C-based function block 
        name.
    */

    if( ( pSymbol = symbol_getSymbol( pContext, pName, pPOU, false ) ) != NULL )
    {
        /* assert( pSymbol->Index < IEC61131_FUNCTION_BLOCK_COUNT ); */
        uAddress = pInstr->PC + 4;

        uTarget = htonl( IEC61131_BYTECODE_FUNCTION_BLOCK | pSymbol->Index );
        sByte = ( ( uTarget & 0xff000000 ) >> 24 );
        memblock_write( &pContext->ByteCode, uAddress + 0, &sByte, 1 );
        sByte = ( ( uTarget & 0x00ff0000 ) >> 16 );
        memblock_write( &pContext->ByteCode, uAddress + 1, &sByte, 1 );
        sByte = ( ( uTarget & 0x0000ff00 ) >> 8 );
        memblock_write( &pContext->ByteCode, uAddress + 2, &sByte, 1 );
        sByte = ( uTarget & 0x000000ff );
        memblock_write( &pContext->ByteCode, uAddress + 3, &sByte, 1 );

        return 0;
    }

    return -ENOENT;
}


static int 
_stage_linkFunctionBlock( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_SYMBOL *pSymbol;
    IEC61131_TOKEN_LITERAL *pLiteral;
    IEC61131_TOKEN_POU *pPOU;
    unsigned int uAddress, uTarget;
    char *pName, sByte;


    pName = ( ( IEC61131_TOKEN_RAW * ) pInstr->Branch )->Data;
    pPOU = ( ( IEC61131_TOKEN * ) pInstr )->Scope;

    if( ( pSymbol = symbol_getSymbol( pContext, pName, pPOU, false ) ) == NULL )
    {
        return -ENOENT;
    }

    pLiteral = ( IEC61131_TOKEN_LITERAL * ) pSymbol->Type;

    for( pElement = IEC61131_LIST_HEAD( &pContext->Library );
            pElement;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement->Data;
        if( strcmp( pPOU->Name, pLiteral->Name ) == 0 )
        {
            uAddress = pInstr->PC + 4;

            uTarget = htonl( pPOU->PC );
            sByte = ( ( uTarget & 0xff000000 ) >> 24 );
            memblock_write( &pContext->ByteCode, uAddress + 0, &sByte, 1 );
            sByte = ( ( uTarget & 0x00ff0000 ) >> 16 );
            memblock_write( &pContext->ByteCode, uAddress + 1, &sByte, 1 );
            sByte = ( ( uTarget & 0x0000ff00 ) >> 8 );
            memblock_write( &pContext->ByteCode, uAddress + 2, &sByte, 1 );
            sByte = ( uTarget & 0x000000ff );
            memblock_write( &pContext->ByteCode, uAddress + 3, &sByte, 1 );

            return 0;
        }
    }

    return -ENOENT;
}


static int
_stage_linkLabel( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    IEC61131_TOKEN_INSTRUCTION *pLabel;
    unsigned int uAddress, uIndex, uTarget;
    char sByte;


    for( uIndex = 0; uIndex < pContext->LabelTable.Count; ++uIndex )
    {
        pLabel = pContext->LabelTable.Entries[ uIndex ];

        if( ( strcmp( ( ( IEC61131_TOKEN_RAW * ) pInstr->Branch )->Data, pLabel->Label ) == 0 ) &&
                ( ( ( IEC61131_TOKEN * ) pInstr )->Scope == ( ( IEC61131_TOKEN * ) pLabel )->Scope ) )
        {
            uAddress = pInstr->PC + 4;

            uTarget = htonl( /* pLabel->PC */ pLabel->Target );
            sByte = ( ( uTarget & 0xff000000 ) >> 24 );
            memblock_write( &pContext->ByteCode, uAddress + 0, &sByte, 1 );
            sByte = ( ( uTarget & 0x00ff0000 ) >> 16 );
            memblock_write( &pContext->ByteCode, uAddress + 1, &sByte, 1 );
            sByte = ( ( uTarget & 0x0000ff00 ) >> 8 );
            memblock_write( &pContext->ByteCode, uAddress + 2, &sByte, 1 );
            sByte = ( uTarget & 0x000000ff );
            memblock_write( &pContext->ByteCode, uAddress + 3, &sByte, 1 );

            return 0;
        }
    }

    return -ENOENT;
}


static int
_stage_linkPOU( IEC61131_CC *pContext, IEC61131_TOKEN_INSTRUCTION *pInstr )
{
    IEC61131_LIST_ELEMENT *pElement;
    IEC61131_TOKEN *pToken;
    IEC61131_TOKEN_POU *pPOU;
    unsigned int uAddress, uTarget;
    char sByte;


    for( pElement = IEC61131_LIST_HEAD( &pContext->Library );
            pElement;
            pElement = IEC61131_LIST_NEXT( pElement ) )
    {
        pPOU = ( IEC61131_TOKEN_POU * ) pElement->Data;

        if( strcmp( ( ( IEC61131_TOKEN_RAW * ) pInstr->Branch )->Data, pPOU->Name ) == 0 )
        {
            if( ( ( IEC61131_TOKEN * ) pInstr )->Scope == pPOU )
            {
                pToken = ( IEC61131_TOKEN * ) pInstr;
                printf( "%s:%u:%u: warning: Potential execution call loop\n",
                        pContext->Source,
                        pToken->Line,
                        pToken->Column );
            }

            uAddress = pInstr->PC + 4;

            uTarget = htonl( pPOU->PC );
            sByte = ( ( uTarget & 0xff000000 ) >> 24 );
            memblock_write( &pContext->ByteCode, uAddress + 0, &sByte, 1 );
            sByte = ( ( uTarget & 0x00ff0000 ) >> 16 );
            memblock_write( &pContext->ByteCode, uAddress + 1, &sByte, 1 );
            sByte = ( ( uTarget & 0x0000ff00 ) >> 8 );
            memblock_write( &pContext->ByteCode, uAddress + 2, &sByte, 1 );
            sByte = ( uTarget & 0x000000ff );
            memblock_write( &pContext->ByteCode, uAddress + 3, &sByte, 1 );

            return 0;
        }
    }

    return -ENOENT;
}


/*!
    \fn int stage_link( IEC61131_CC *pContext )
    \brief Performs linking and resolution of branch and jump operations within byte code
    \param pContext Pointer to IEC 61131-3 compilation contextual data structure
    \returns Returns zero on success, non-zero on error

    This stage of the IEC 61131-3 compilation process involves the resolution 
    of branch, jump and call instructions within byte code.  
*/

int
stage_link( IEC61131_CC *pContext )
{
    IEC61131_TOKEN *pToken;
    IEC61131_TOKEN_INSTRUCTION *pInstr;
    unsigned int uIndex;


    /*
        For each of the unresolved branch and/or jump operations identified when 
        creating IEC 61131-3 byte code, iterate through the list of POU components 
        and table of parsed labels and match based on label name and execution 
        scope.
    */

    for( uIndex = 0; uIndex < pContext->InstructionTable.Count; ++uIndex )
    {
        pInstr = pContext->InstructionTable.Entries[ uIndex ];

        if( ( _stage_linkLabel( pContext, pInstr ) == 0 ) ||
                ( _stage_linkPOU( pContext, pInstr ) == 0 ) ||
                ( _stage_linkFunctionBlock( pContext, pInstr ) == 0 ) ||
                ( _stage_linkExternalLibrary( pContext, pInstr ) == 0 ) )
        {
            continue;
        }

        pToken = ( IEC61131_TOKEN * ) pInstr;
        printf( "%s:%u:%u: error: Cannot resolve jump/call instruction to '%s'\n",
                pContext->Source,
                pToken->Line,
                pToken->Column,
                ( ( IEC61131_TOKEN_RAW * ) pInstr->Branch )->Data );

        return -ENOENT;
    }

	return 0;
}
