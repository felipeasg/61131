IEC 61131-3 Bytecode Compiler and Virtual Machine
=================================================

Last updated: 2015/03/10


Overview
--------

This project is intended to implement a compiler for IEC 61131-3 textual 
languages, the output of which is a byte code intended to be run in a small 
virtual machine also under development as part of this project.  While this 
project is intended to eventually support the Instruction List (IL) and 
Structured Text (ST) IEC 61131-3 textual languages, support has initially
been implemented for IL only.

This project is arranged in the following structure:

    cc          IEC 61131-3 compiler source
    doc         Doxygen generated project documentation (incomplete)
    examples    Work-in-progress example source files
    include     Common project header files
    lib         Common project source files
    vm          IEC 61131-3 virtual machine
    

IEC 61131-3 Compiler
--------------------

This IEC 61131-3 compiler operates in a number of stages:

    Stage 1     Lexical analyser/syntax parsing
    Stage 2     Function block sizing
    Stage 3     Table population
    Stage 4     Symbol type checking
    Stage 5     Output file generation
    Stage 6     Symbol table output
    Stage 7     Byte code generation
    Stage 8     Linking

These distinct stages of operation of the IEC 61131-3 compiler is additionally 
reflected in the division and categorisation of source files.


Stage 1 - Lexical analyser/syntax parsing

This stage of operations is dependent upon the Flex and Bison for lexical 
analysis and syntax parsing accordingly.  This grammar has been developed with
reference to the IEC 61131-3 standard and the existing open-source IEC 61131-3
compiler project, MATIEC (https://bitbucket.org/mjsousa/matiec/).


Stage 2 - Function block sizing

This stage of operations is intended to determine how much space should be 
allocated within the target symbol table for each instance of a function block.
This is determined by iterating through each of the input, local and output
variables associated with a function block and accumulating the amount of 
memory-aligned space should be allocated for each.


Stage 3 - Table population

This stage of operations involves the construction of a series of data tables 
with symbol information and jump label locations parsed from the IEC 61131-3 
source file.  These tables are intended to accommodate subsequent compiler 
operations dependent upon the resolution of these program elements.

It should be noted that this stage was originally specific to symbol table 
population, but has been expanded to include branch and jump label locations 
and function block locations to expedite subsequent linking operations 
performed after program byte code has been generated.


Stage 4 - Symbol type checking

This stage of operations involves the stepping through parsed instruction list 
tokens and verifies consistency in the types of operators and operands as 
appropriate.  In addition to this, this stage will also perform cast typing of
literal value representations to the appropriate data type for operator actions
where possible, although the scope of such actions are however limited to 
integer, bit-string and floating point values only at this point.


Stage 5 - Output file generation

This very basic stage of operations involves the population of the data 
structure which comprises the header structure of the bytecode output file 
format.


Stage 6 - Symbol table output

This stage of operations involves the writing of initial values for symbol table
entries, in a memory-aligned format, ready for direct copy into RAM any employ 
by the IEC 61131-3 virtual machine run-time.


Stage 7 - Byte code generation

This stage of operations involves the generation of byte code for the parsed
instruction list source file.  If the byte code generated includes reference to
either a POU or POU label, the current byte code position is recorded for later
linking operations and a placeholder for the subsequent jump or call operation 
inserted into the byte code stream.  This placeholder is populated during the
linking stage of operations.

This action is performed in the event that the target location has not yet 
been written to byte code and therefore not known at the time of byte code
generation.


Stage 8 - Linking

This stage of operations involves the iteration through unresolved jump labels
and call operations identified during the preceding stage of compiler operations 
and inserts appropriate byte code target locations and external link references.



IEC 61131-3 Virtual Machine
---------------------------

The IEC 61131-3 virtual machine loads and executes byte code produced by the
IEC 61131-3 compiler in a cyclic fashion as described in the IEC 61131-3 
standard.  This execution behaviour consists of:

    - Read input channels
    - Execute program organisation units
    - Write output channels

The majority of these operations are encapsulated in a single source file 
(op.c).
