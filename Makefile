.PHONY: cc doc vm

all: clean cc vm doc

cc:
	make -C cc

vm:
	make -C vm

doc:
	doxygen Doxyfile > /dev/null 2>&1

clean:
	rm -rf doc/*
	make -C cc clean
	make -C vm clean

